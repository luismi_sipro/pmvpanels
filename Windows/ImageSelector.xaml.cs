﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ImageSelector : Window
    {
        public ImageSelector()
        {
            InitializeComponent();
            Images = new ObservableCollection<ImageInfo>();
            TipoIconos = new ObservableCollection<TipoIcono>();            
        }

        public ObservableCollection<ImageInfo> Images { get; set; }
        public ObservableCollection<TipoIcono> TipoIconos { get; set; }


        SQLServer _db = null;
        SQLOperations SQL = null;

        public bool OfflineMode = true;


     

        public SQLServer db
        {
            get
            {
                return _db;
            }
            set
            {
                _db = value;
                SQL = new SQLOperations(db);
            }
        }

        int _FiltroTipoIcono = -1;
        internal int FiltroTipoIcono { get { return _FiltroTipoIcono; } set { _FiltroTipoIcono = value; LoadImages(); } }

        int _SelectedImageID = -1;
        public int SelectedImageID { get { return _SelectedImageID; } set { _SelectedImageID = value; } }


        private bool LoadComboBox()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;
            try
            {
                //db = new SQLServer("Server = (local);Database = Panels;Trusted_Connection = True;");
                if (db == null) return false;
                if (SQL == null) return false;

                TipoIconos.Clear();

                DataTable TipoIconosDT = SQL.SELECT_ALL_Table("TipoIcono");

                TipoIconos.Add(new TipoIcono() { ID = -1, Nombre = "Todos los tipos" });

                if (!OfflineMode) foreach (DataRow irow in TipoIconosDT.Rows) TipoIconos.Add(new TipoIcono() { ID = (int)irow["ID"], Nombre = (string)irow["Nombre"] });

                comboBox.ItemsSource = TipoIconos;

                comboBox.SelectedIndex = 0;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadPreview()
        {
            if (OfflineMode) return LoadPreviewOffline();

            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                if (db == null) return false;
                if (SQL == null) return false;
                if (SelectedImageID > 0)
                {
                    DataTable Iconos = SQL.SELECT_Icono_byID(SelectedImageID);
                    foreach (DataRow Icono in Iconos.Rows)
                    {
                        image.Source = GlobalStatic.GetImageFromStream((byte[])Icono["ImagenIcono"]);
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadImages()
        {
            if (OfflineMode) return LoadImagesOffline();
           
            if (DesignerProperties.GetIsInDesignMode(this)) return false;
            this.comboBox.IsEnabled = true;
            try
            {
                //db = new SQLServer("Server = (local);Database = Panels;Trusted_Connection = True;");
                if (db == null) return false;
                if (SQL == null) return false;

                Images.Clear();

                DataTable Iconos;

                if (_FiltroTipoIcono == -1)
                    Iconos = SQL.SELECT_ALL_Table("Icono");
                else
                    Iconos = SQL.SELECT_Icono_byTipoIcono(_FiltroTipoIcono);

                foreach (DataRow Icono in Iconos.Rows)
                {
                    ImageInfo II = new ImageInfo() { ID = (int)Icono["ID"], TipoIcono = (int)Icono["TipoIcono"], Image = GlobalStatic.GetImageFromStream((byte[])Icono["ImagenIcono"]) };
                    Images.Add(II);
                }

                ListOfImages.ItemsSource = Images;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        System.Resources.ResourceManager rm = Properties.Resources.ResourceManager;
        private bool LoadImagesOffline()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            this.comboBox.IsEnabled = false;
            try
            {
                Images.Clear();

               

                for (int i = 128; i <= 252; i++)
                {
                    try
                    {
                        object img = rm.GetObject("pmv" + i);
                        Stream img2 = GlobalStatic.ToStream((System.Drawing.Image)img, System.Drawing.Imaging.ImageFormat.Png);
                        ImageInfo II = new ImageInfo() { ID = i, TipoIcono = 1, Image = GlobalStatic.GetImageFromStream(img2) };
                        Images.Add(II);
                    }
                    catch (Exception ex)
                    {
                        string s = ex.Message;
                    }
                 
                }

                ListOfImages.ItemsSource = Images;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadPreviewOffline()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {              
                if (SelectedImageID > 0)
                {
                    try
                    {
                        object img = rm.GetObject("pmv" + SelectedImageID);
                        Stream img2 = GlobalStatic.ToStream((System.Drawing.Image)img, System.Drawing.Imaging.ImageFormat.Png);
                        image.Source = GlobalStatic.GetImageFromStream(img2);                      
                    }
                    catch (Exception ex)
                    {
                        string s = ex.Message;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ListOfImages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectedImageID = ((ImageInfo)ListOfImages.SelectedItem).ID;
                image.Source = ((ImageInfo)ListOfImages.SelectedItem).Image;
            }
            catch (Exception)
            {
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FiltroTipoIcono = ((TipoIcono)comboBox.SelectedItem).ID;
        }

        private void bFiltrar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void bCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadComboBox();
            LoadImages();
            LoadPreview();
        }

        private void bCancelar_Click_1(object sender, RoutedEventArgs e)
        {
            SelectedImageID = 0;
            this.DialogResult = true;
            this.Close();
        }
    }

    
}
