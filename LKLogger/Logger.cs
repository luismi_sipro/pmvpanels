﻿using Microsoft.Win32;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Permissions;

namespace LKLog
{
    public static class wwLogger
    {
        public static int ErrorCount
        {
            get
            {
                if (!wwLogger.InitLoggerDll())
                {
                    return -1;
                }
                int result = 0;
                int num = 0;
                long num2 = 0L;
                long num3 = 0L;
                int loggerStats = NativeMethods.GetLoggerStats(string.Empty, ref result, ref num2, ref num, ref num3);
                if (loggerStats <= 0)
                {
                    return -1;
                }
                return result;
            }
        }

        public static bool IsLoaded
        {
            get;
            private set;
        }

        public static int WarningCount
        {
            get
            {
                if (!wwLogger.InitLoggerDll())
                {
                    return -1;
                }
                int num = 0;
                int result = 0;
                long num2 = 0L;
                long num3 = 0L;
                int loggerStats = NativeMethods.GetLoggerStats(string.Empty, ref num, ref num2, ref result, ref num3);
                if (loggerStats <= 0)
                {
                    return -1;
                }
                return result;
            }
        }

        private static bool CheckRegistry
        {
            get;
            set;
        }

        private static bool DomainUnloaded
        {
            get;
            set;
        }

        private static int MhIdentity
        {
            get;
            set;
        }

        static wwLogger()
        {
            wwLogger.MhIdentity = 0;
            wwLogger.CheckRegistry = true;

            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(FatalError);

           
        }

        private static void FatalError(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            LogError("UnhandledException: " + e.Message + Environment.NewLine + e.StackTrace);
            LogError("Runtime terminating: " + args.IsTerminating);

            //do
            //{
            //    (new System.Threading.Thread(delegate ()
            //    {
            //        throw new ArgumentException("ha-ha");
            //    })).Start();

            //} while (Console.ReadLine().Trim().ToLowerInvariant() == "x");
        }

        public static void LogConnection(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogConnection(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogCtorDtor(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogCtorDtor(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogCustom(int cookie, string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogCustom(wwLogger.MhIdentity, cookie, errorMessage);
            }
        }

        public static void LogEntryExit(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogEntryExit(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogError(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogError(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogInfo(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogInfo(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogRefCount(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogRefCount(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static int LogRegisterCustomFlag(string flagName)
        {
            if (!wwLogger.Initialize())
            {
                return 0;
            }
            return NativeMethods.RegisterLogFlag(wwLogger.MhIdentity, 11, flagName);
        }

        public static int LogRegisterCustomFlagEx(string flagName, int defaultValue)
        {
            if (!wwLogger.Initialize())
            {
                return 0;
            }
            return NativeMethods.RegisterLogFlagEx(wwLogger.MhIdentity, 11, flagName, defaultValue);
        }

        public static void LogSQL(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogSQL(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogSetIdentityName(string identityName)
        {
            if (wwLogger.Initialize())
            {
                int num = NativeMethods.SetIdentityName(wwLogger.MhIdentity, identityName);
            }
        }

        public static void LogStartStop(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogStartStop(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogThreadStartStop(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogThreadStartStop(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogTrace(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogTrace(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void LogWarning(string errorMessage)
        {
            if (wwLogger.Initialize())
            {
                NativeMethods.InternalLogWarning(wwLogger.MhIdentity, errorMessage);
            }
        }

        public static void ResetLoggerCheck()
        {
            wwLogger.CheckRegistry = true;
        }

        private static bool InitLoggerDll()
        {
            bool flag = wwLogger.IsLoaded;
            if (flag)
            {
                return true;
            }
            if (wwLogger.CheckRegistry)
            {
                IntPtr value = IntPtr.Zero;
                wwLogger.CheckRegistry = false;
                new RegistryPermission(RegistryPermissionAccess.Read, "HKEY_LOCAL_MACHINE\\Software\\ArchestrA\\Framework\\Logger").Assert();
                try
                {
                    RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("Software\\ArchestrA\\Framework\\Logger", false);
                    if (registryKey != null)
                    {
                        string text = Convert.ToString(registryKey.GetValue("InstallPath", string.Empty), CultureInfo.InvariantCulture);
                        if (text.Length > 0)
                        {
                            value = NativeMethods.LoadLibraryW(Path.Combine(text, "LoggerDll.dll"));
                        }
                    }
                }
                finally
                {
                    CodeAccessPermission.RevertAssert();
                }
                flag = (value != IntPtr.Zero);
            }
            wwLogger.IsLoaded = flag;
            return flag;
        }

        private static bool Initialize()
        {
            if (wwLogger.DomainUnloaded)
            {
                return false;
            }
            if (wwLogger.MhIdentity != 0)
            {
                return true;
            }
            if (wwLogger.InitLoggerDll())
            {
                int mhIdentity = 0;
                int num = NativeMethods.RegisterLoggerClient(ref mhIdentity);
                wwLogger.MhIdentity = mhIdentity;
                if (num != 0 && wwLogger.MhIdentity != 0)
                {
                    new FileIOPermission(PermissionState.Unrestricted).Assert();
                    try
                    {
                        num = NativeMethods.SetIdentityName(wwLogger.MhIdentity, Assembly.GetExecutingAssembly().GetName().Name);
                    }
                    finally
                    {
                        CodeAccessPermission.RevertAssert();
                    }
                    AppDomain.CurrentDomain.DomainUnload += new EventHandler(wwLogger.OnCurrentDomainUnload);
                    return true;
                }
            }
            return false;
        }

        private static void OnCurrentDomainUnload(object sender, EventArgs e)
        {
            wwLogger.UnInitialize();
            wwLogger.DomainUnloaded = true;


            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException -= new UnhandledExceptionEventHandler(FatalError);

        }

        private static void UnInitialize()
        {
            if (wwLogger.MhIdentity == 0)
            {
                return;
            }
            int num = NativeMethods.UnregisterLoggerClient(wwLogger.MhIdentity);
            wwLogger.MhIdentity = 0;
        }
    }
}

