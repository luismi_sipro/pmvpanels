﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sipro.Database;

namespace PanelsControls
{
    public class CLangCode
    {
        //Default language.
        public const String DEFAULT_LANGUAGE = "spanish";

        //BibliotecaPanelesv2: 00001 -> 00050
        public const String BP2_NAME_LABEL_LIBRARY = "CODE00001";

        //EdicionPanelesv2: 00051 -> 00100
        public const String PE2_NAME_LABEL_TEMPLATE = "CODE00051";
        public const String PE2_NAME_LABEL_ASPECTS = "CODE00052";
        public const String PE2_NAME_BTN_APPLY = "CODE00053";
        public const String PE2_NAME_BTN_SAVE = "CODE00054";
        public const String PE2_NAME_BTN_CANCEL = "CODE00055";

        //PanelSelectorv2: 00101 -> 00150
        public const String PS2_NAME_LABEL_TUNNEL = "CODE00101";
        public const String PS2_NAME_LABEL_PANEL = "CODE00102";

        //HistoricoPanelesv2: 00151 -> 00200
        public const String HP2_NAME_BTN_PANEL = "CODE00151";
        public const String HP2_NAME_BTN_TYPE = "CODE00152";
        public const String HP2_NAME_BTN_COPY = "CODE00153";
        public const String HP2_NAME_LABEL_SEL = "CODE00154";

        //ImageSelectorv2: 00201 -> 00250
        public const String IS2_NAME_LABEL_SEL = "CODE00201";
        public const String IS2_NAME_LABEL_TYPESIG = "CODE00202";
        public const String IS2_NAME_BTN_CANCEL = "CODE00203";
        public const String IS2_NAME_BTN_ACCEPT = "CODE00204";
        public const String IS2_NAME_BTN_WITHOUT = "CODE00205";
        public const String IS2_NAME_LABEL_PREV = "CODE00206";
        public const String IS2_NAME_LABEL_ALL = "CODE00207";


        //List of translations labels.
        private static List<CLangDef> m_listOfTexts;


        #region METHODS

        /// <summary>
        /// Gets language texts for app labels.
        /// </summary>
        /// <param name="mConnection"></param>
        /// <param name="mLanguage"></param>
        /// <returns></returns>
        public static void GetLanguageTexts(CDBSqlServer mConnection, String mLanguage)
        {
            //Check if object connection exist and connection it's ok.
            if (mConnection == null || mConnection.State != System.Data.ConnectionState.Open)
                return;

            if (String.IsNullOrEmpty(mLanguage) == true)
                mLanguage = DEFAULT_LANGUAGE;

            //Create query.
            String _query = String.Format("SELECT * FROM Idiomas WHERE Idioma = '{0}'", mLanguage.ToLower());
            CDataBaseResult _dbResult = mConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return;

            //Get results.
            m_listOfTexts = new List<CLangDef>();
            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CLangDef _langText = new CLangDef();
                _langText.Id = Convert.ToInt32(_row.GetValue("Id"));
                _langText.Code = _row.GetValue("Codigo").ToString();
                _langText.Language = _row.GetValue("Idioma").ToString();
                _langText.Text = _row.GetValue("Texto").ToString();

                m_listOfTexts.Add(_langText);
            }
                   
        }

        /// <summary>
        /// Return text from selected language.
        /// </summary>
        /// <param name="mCode">Text code to search.</param>
        public static String GetText(String mCode)
        {
            if (m_listOfTexts == null)
                return mCode;

            if (m_listOfTexts.Any(x => x.Code == mCode) == false)
                return mCode;

            return m_listOfTexts.FirstOrDefault(x => x.Code == mCode).Text;
        }

        /// <summary>
        /// Return error text from selected language.
        /// </summary>
        /// <param name="mCode">Text error to search.</param>
        public static String GetErrorText(Int32 mCode)
        {
            String _code = String.Format("CODERR{0:00000}", mCode);

            if (m_listOfTexts == null)
                return _code;

            if (m_listOfTexts.Any(x => x.Code == _code) == false)
                return _code;

            return m_listOfTexts.FirstOrDefault(x => x.Code == _code).Text;
        }
             
        #endregion


    }

    public class CLangDef
    {
        public Int32 Id
        {
            get;
            set;
        }

        public String Code
        {
            get;
            set;
        }

        public String Language
        {
            get;
            set;
        }

        public String Text
        {
            get;
            set;
        }
    }
}
