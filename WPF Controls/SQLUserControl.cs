﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace PanelsControls
{
    public class SQLUserControl: UserControl
    {
        protected string DataBaseName = "Panels";
        protected bool isInit = false;
        public SQLServer db = null;

        System.Windows.Threading.DispatcherTimer tmrInitialize = new System.Windows.Threading.DispatcherTimer();

        public static readonly DependencyProperty SQLUserProperty = DependencyProperty.Register("SQLUser", typeof(string), typeof(SQLUserControl), new UIPropertyMetadata("", new PropertyChangedCallback(OnSQLUserChanged)));
        public static readonly DependencyProperty SQLPasswordProperty = DependencyProperty.Register("SQLPassword", typeof(string), typeof(SQLUserControl), new UIPropertyMetadata("", new PropertyChangedCallback(OnSQLPasswordChanged)));
        public static readonly DependencyProperty SQLServerProperty = DependencyProperty.Register("SQLServer", typeof(string), typeof(SQLUserControl), new UIPropertyMetadata("", new PropertyChangedCallback(OnSQLServerChanged)));
        public static readonly DependencyProperty SQLTrustedConnectionProperty = DependencyProperty.Register("SQLTrustedConnection", typeof(bool), typeof(SQLUserControl), new UIPropertyMetadata(false, new PropertyChangedCallback(OnSQLTrustedConnectionChanged)));
     

        protected string _ConnectionString = "";

        [Browsable(false)]
        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }
            set
            {
                if (null != value && value.Length > 0 && value != _ConnectionString)
                {
                    _ConnectionString = value;
                }
            }
        }

        internal protected string _SQLUser = "";
        [Category("SQL Connection")]
        [Browsable(true)]
        [Description("Predefined user database connection.")]
        public string SQLUser
        {
            get
            {
                if (_SQLUser != (string)base.GetValue(SQLUserProperty)) base.SetValue(SQLUserProperty, _SQLUser);
                return _SQLUser;
            }
            set
            {
                if (null != value && value.Length > 0 && value != _SQLUser)
                {
                    _SQLUser = value;
                    base.SetValue(SQLUserProperty, value);
                    _ConnectionString = MountConnectionString();
                    if (!isInit) PreInitialize();
                }
            }
        }

        internal protected string _SQLPassword = "";
        [Category("SQL Connection")]
        [Browsable(true)]
        [PasswordPropertyText(true)]
        [Description("Predefined password database connection.")]
        public string SQLPassword
        {
            get
            {
                if (_SQLPassword != (string)base.GetValue(SQLPasswordProperty)) base.SetValue(SQLPasswordProperty, _SQLPassword);
                return _SQLPassword;
            }
            set
            {
                if (null != value && value.Length > 0 && value != _SQLPassword)
                {
                    _SQLPassword = value;
                    base.SetValue(SQLPasswordProperty, value);
                    _ConnectionString = MountConnectionString();
                    if (!isInit) PreInitialize();
                }
            }
        }

        internal protected string _SQLServer = "";
        [Category("SQL Connection")]
        [Browsable(true)]
        [Description("SQL Server database name.")]
        public string SQLServer
        {
            get
            {
                if (_SQLServer != (string)base.GetValue(SQLServerProperty)) base.SetValue(SQLServerProperty, _SQLServer);
                return _SQLServer;
            }
            set
            {
                if (null != value && value.Length > 0 && value != _SQLServer)
                {
                    _SQLServer = value;
                    base.SetValue(SQLServerProperty, value);
                    _ConnectionString = MountConnectionString();
                    if (!isInit) PreInitialize();
                }
            }
        }

        internal protected bool _SQLTrustedConnection = false;
        [Category("SQL Connection")]
        [Browsable(true)]
        [Description("Indicates whether to connect to SQL database using an OS user or not.")]
        public bool SQLTrustedConnection
        {
            get
            {
                if (_SQLTrustedConnection != (bool)base.GetValue(SQLTrustedConnectionProperty)) base.SetValue(SQLTrustedConnectionProperty, _SQLTrustedConnection);
                return _SQLTrustedConnection;
            }
            set
            {
                if (value != _SQLTrustedConnection)
                {
                    _SQLTrustedConnection = value;
                    base.SetValue(SQLTrustedConnectionProperty, value);
                    _ConnectionString = MountConnectionString();
                    if (!isInit) PreInitialize();
                }
            }
        }

        private static void OnSQLUserChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (!string.IsNullOrEmpty(str1))
                if (((SQLUserControl)d).SQLUser != str1)
                    ((SQLUserControl)d).SQLUser = str1;
        }

        private static void OnSQLPasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (!string.IsNullOrEmpty(str1))
                if (((SQLUserControl)d).SQLPassword != str1)
                    ((SQLUserControl)d).SQLPassword = str1;
        }

        private static void OnSQLServerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (!string.IsNullOrEmpty(str1))
                if (((SQLUserControl)d).SQLServer != str1)
                    ((SQLUserControl)d).SQLServer = str1;
        }

        private static void OnSQLTrustedConnectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            try
            {
                if (((SQLUserControl)d).SQLTrustedConnection != (bool)e.NewValue)
                    ((SQLUserControl)d).SQLTrustedConnection = (bool)e.NewValue;
            }
            catch (Exception)
            {
            }
        }

        protected void PreInitialize()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            tmrInitialize.Stop();
            tmrInitialize.Interval = new TimeSpan(0, 0, 0, 0, 300);
            tmrInitialize.Tick += new EventHandler(tmrInitialize_Tick);
            tmrInitialize.Start();
        }

        void tmrInitialize_Tick(object sender, EventArgs e)
        {
            tmrInitialize.Stop();
            Initialize();
        }

        private void Initialize()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            if (!isInit)
            {
                if (VerifyConnectionProperties() == false) return;

                db = new SQLServer(ConnectionString);

                InitControl();

                isInit = true;
            }
        }

        private bool VerifyConnectionProperties()
        {
            try
            {
                if (SQLUser.Trim().Length == 0 && SQLTrustedConnection == false) return false;
                if (SQLPassword.Trim().Length == 0 && SQLTrustedConnection == false) return false;
                if (SQLServer.Trim().Length == 0) return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private string MountConnectionString()
        {
            try
            {
                string s = "";

                s = "Server = " + SQLServer + ";";
                s += "Database = " + DataBaseName + ";";
                if (SQLTrustedConnection)
                { s += "Trusted_Connection = True;"; }
                else
                {
                    s += "User = " + SQLUser + ";";
                    s += "Password = " + SQLPassword + ";";
                }
                s += "max pool size = 3000000";

                return s;
            }
            catch (System.Exception)
            {
                return "";
            }
        }

        protected virtual void InitControl()
        {

        }
    }
}
