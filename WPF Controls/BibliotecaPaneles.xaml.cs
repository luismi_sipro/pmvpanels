﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Windows.Threading;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for BibliotecaPaneles.xaml
    /// </summary>
    public partial class BibliotecaPaneles : SQLUserControl
    {

        public int MaxTreeLevel = 2;

        //List<CategoryTreeNode> TreeNodes;
        public ObservableCollection<CategoryTreeNode> TreeNodes
        {
            get;
            set;
        }
        CuteTreeNode SelectedNode = null;
        SQLOperations SQL = null;

        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(string), typeof(BibliotecaPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        public static readonly DependencyProperty IDTemplateProperty = DependencyProperty.Register("IDTemplate", typeof(string), typeof(BibliotecaPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnIDTemplateChanged)));
        public static readonly DependencyProperty EditingProperty = DependencyProperty.Register("Editing", typeof(bool), typeof(BibliotecaPaneles), new UIPropertyMetadata(false, new PropertyChangedCallback(OnEditingChanged)));

        System.Windows.Threading.DispatcherTimer tmrUpdate = new System.Windows.Threading.DispatcherTimer();

        internal protected string _Panel = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Instance Panel")]
        public string Panel
        {
            get
            {
                if (_Panel != (string)base.GetValue(PanelProperty)) base.SetValue(PanelProperty, _Panel);
                return _Panel;
            }
            set
            {
                if (null != value && value != _Panel)
                {
                    _Panel = value;
                    base.SetValue(PanelProperty, value);
                    if (!isInit) PreInitialize(); else InitControl();
                }
            }
        }

        internal protected string _IDTemplate = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Panel Template selected")]
        public string IDTemplate
        {
            get
            {
                if (_IDTemplate != (string)base.GetValue(IDTemplateProperty)) base.SetValue(IDTemplateProperty, _IDTemplate);
                return _IDTemplate;
            }
            set
            {
                if (null != value && value != _IDTemplate)
                {
                    _IDTemplate = value;
                    base.SetValue(IDTemplateProperty, value);
                }
            }
        }

        internal protected bool _Editing = false;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Indicates if current Panel Template is in edition")]
        public bool Editing
        {
            get
            {
                if (_Editing != (bool)base.GetValue(EditingProperty)) base.SetValue(EditingProperty, _Editing);
                return _Editing;
            }
            set
            {
                if (value != _Editing)
                {
                    _Editing = value;
                    base.SetValue(EditingProperty, value);
                    this.treeView.IsEnabled = !value;
                }
            }
        }

        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((BibliotecaPaneles)d).Panel != str1)
                ((BibliotecaPaneles)d).Panel = str1;
        }

        private static void OnIDTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((BibliotecaPaneles)d).IDTemplate != str1)
                ((BibliotecaPaneles)d).IDTemplate = str1;
        }

        private static void OnEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            bool bit = Convert.ToBoolean(e.NewValue);
            if (((BibliotecaPaneles)d).Editing != bit)
                ((BibliotecaPaneles)d).Editing = bit;
        }

        public BibliotecaPaneles()
        {
            InitializeComponent();
            //InitControl();
            tmrUpdate.Interval = new TimeSpan(0, 0, 0, 0, 300);
            tmrUpdate.Tick += new EventHandler(tmrUpdate_Tick);
        }


        public void Refresh()
        {
            if (isInit) ReloadTree();
        }

        protected override void InitControl()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            SQL = new SQLOperations(db);

            tmrCheckDB.Interval = new TimeSpan(0, 0, 0, 10, 0);
            tmrCheckDB.Tick += new EventHandler(tmrCheckDB_Tick);
            tmrCheckDB.Start();
            CheckDBFunction();

            if (!OfflineMode)
            {
                SQL.INSERT_PlantillasPorDefecto();
                SQL.UPDATE_2();
            }

            TreeNodes = new ObservableCollection<CategoryTreeNode>();
            treeView.ItemsSource = TreeNodes;
            ReloadTree();
        }


        private CategoryTreeNode AddCategoryNode(CategoryTreeNode parent, string Name, Guid ID)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return null;

            CategoryTreeNode CTN = null;
            try
            {
                if (parent.Level + 1 > MaxTreeLevel)
                {
                    //if (ID == ExpandID && ExpandID != Guid.Empty)
                    //{
                    //    ExpandID = Guid.Empty;
                    //    CTN = new CategoryTreeNode() { Name = Name, Level = MaxTreeLevel, parent = parent.parent, ID = ID, IsExpanded = true };
                    //}
                    //else
                    {
                        CTN = new CategoryTreeNode() { Name = Name, Level = MaxTreeLevel, parent = parent.parent, ID = ID };
                    }
                    parent.parent.Nodes.Add(CTN);
                }
                else
                {
                    CTN = new CategoryTreeNode() { Name = Name, Level = parent.Level + 1, parent = parent, ID = ID };
                    parent.Nodes.Add(CTN);
                }
            }
            catch (Exception)
            {
            }

            return CTN;
        }

        private TemplateTreeNode AddTemplateNode(CategoryTreeNode parent, string Name, Guid ID, int DefaultNumber)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return null;

            TemplateTreeNode TTN = new TemplateTreeNode() { Name = Name, ID = ID, parent = parent, Level = parent.Level + 1, DefaultNumber = DefaultNumber };
            parent.Nodes.Add(TTN);
            //IDTemplate = ID.ToString();
            //Editing = true;
            return TTN;
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            //REFRESH
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            Refresh();
        }

        private void newFolder_Click(object sender, RoutedEventArgs e)
        {
            //NEW FOLDER UNDER SELECTED NODE
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (SelectedNode == null) return;
            if (SelectedNode.ID == Guid.Parse(DefaultCat)) return;
            SelectedNode.IsExpanded = true;
            AddFolder(SelectedNode);

        }

        private void newTemplate_Click(object sender, RoutedEventArgs e)
        {
            //NEW TEMPLATE UNDER SELECTED NODE
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (SelectedNode == null) return;
            if (SelectedNode.ID == Guid.Parse(DefaultCat)) return;
            AddTemplate(SelectedNode);

        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            //EDIT SELECTED ITEM
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (SelectedNode == null) return;
            if (SelectedNode.GetType() == typeof(CategoryTreeNode))
            {
                if (SelectedNode.ID == Guid.Parse(DefaultCat)) return;
                EditFolder(SelectedNode);
            }
            else
            {
                IDTemplate = SelectedNode.ID.ToString();
                Editing = true;
            }
        }

        string DefaultCat = "00000000-0000-0000-0000-000000000001";
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            //DELETE SELECTED ITEM
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            if (SelectedNode == null) return;
            if (SelectedNode.GetType() == typeof(CategoryTreeNode))
            {
                if (SelectedNode.ID == Guid.Parse(DefaultCat)) return;
                if (System.Windows.Forms.MessageBox.Show("¿Desea eliminar la carpeta '" + SelectedNode.Name + "' y todo su contenido?", "Eliminar Carpeta", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No) return;
                DeleteFolder((CategoryTreeNode)SelectedNode);
            }
            else
            {
                if (((TemplateTreeNode)SelectedNode).DefaultNumber >= 0) return;
                if (System.Windows.Forms.MessageBox.Show("¿Desea eliminar la plantilla de panel '" + SelectedNode.Name + "'?", "Eliminar Plantilla Panel", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No) return;
                DeleteTemplate((TemplateTreeNode)SelectedNode);
                IDTemplate = "";
            }
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            try
            {
                if (!isInit) return;
                Editing = false;
                SelectedNode = (CuteTreeNode)treeView.SelectedItem;
                if (SelectedNode.GetType() == typeof(TemplateTreeNode))
                    IDTemplate = SelectedNode.ID.ToString();
            }
            catch (Exception)
            {
                IDTemplate = "";
            }

        }

        int Tipologia = 0;
        private int GetTipology(string Panel)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return 0;

            if (OfflineMode) return 0;

            try
            {
                DataTable DataConfig = SQL.SELECT("SELECT [ID],[NumLineas],[NumIconos] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Instancia] WHERE Panel = '" + Panel + "')");

                foreach (DataRow crow in DataConfig.Rows)
                {
                    Tipologia = (int)crow["ID"];
                }
                return Tipologia;
            }
            catch (Exception)
            {
                return 0;
            }
        }


        private bool ReloadTree()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                TreeNodes.Clear();

                CategoryTreeNode Root = new CategoryTreeNode() { Name = "Biblioteca", Level = 0, IsExpanded = true };

                GetTipology(this.Panel);

                if (!OfflineMode)
                {
                    DataTable Categoria = SQL.SELECT_ALL_Table("Categoria");
                    DataTable Plantilla = SQL.SELECT_Plantilla_byTipologia(this.Tipologia);// SQL.SELECT_ALL_Table("Plantilla");

                    LoadCategoryNode(Categoria, Plantilla, ref Root);
                }

                TreeNodes.Add(Root);
                treeView.ItemsSource = TreeNodes;

                IDTemplate = "";

                return true;
            }
            catch (Exception)
            {
                ReloadTree();
                return false;
            }
        }

        private Action EmptyDelegate = delegate () { };


        public void Refresh(UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        private bool LoadCategoryNode(DataTable Categoria, DataTable Plantilla, ref CategoryTreeNode parentnode)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                foreach (DataRow C in Categoria.Select("ParentID = '" + parentnode.ID + "'", "Nombre"))
                {
                    CategoryTreeNode Cnode = AddCategoryNode(parentnode, (string)C["Nombre"], (Guid)C["ID"]);

                    LoadCategoryNode(Categoria, Plantilla, ref Cnode);
                    LoadTemplateNode(Categoria, Plantilla, ref Cnode);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadTemplateNode(DataTable Categoria, DataTable Plantilla, ref CategoryTreeNode parentnode)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                foreach (DataRow C in Plantilla.Select("Categoria = '" + parentnode.ID + "'", "Nombre"))
                {
                    int DefaultNumber = -1;
                    if (C["NumeroPorDefecto"] != DBNull.Value) DefaultNumber = (int)C["NumeroPorDefecto"];
                    TemplateTreeNode Cnode = AddTemplateNode(parentnode, (string)C["Nombre"], (Guid)C["ID"], DefaultNumber);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool AddFolder(CuteTreeNode parent)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                CategoryTreeNode MyParent = null;
                if (parent.GetType() == typeof(TemplateTreeNode)) MyParent = parent.parent; else MyParent = (CategoryTreeNode)parent;
                if (MyParent.Level == MaxTreeLevel) MyParent = MyParent.parent;

                NewFolder NF = new NewFolder();
                NF.db = this.db;
                NF.ParentID = MyParent.ID;
                if (NF.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;

                AddCategoryNode(MyParent, NF.FolderName, NF.ID);

                NF.Dispose();
                NF = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        Guid FindID = Guid.Empty;
        CuteTreeNode AddNode = null;
        private bool EditFolder(CuteTreeNode Folder)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                EditFolder EF = new EditFolder();
                EF.db = this.db;
                EF.ID = Folder.ID;
                EF.FolderName = Folder.Name;
                if (EF.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
                Guid parentID = Folder.parent.ID;

                Folder.parent.Nodes.Remove(Folder);

                Folder.Name = EF.FolderName;
                //ExpandID = Folder.ID;
                //ReloadTree();
                Refresh(treeView);
                FindID = parentID;
                AddNode = Folder;

                tmrUpdate.Start();

                //((CategoryTreeNode)FindNodeByID(parentID)).Nodes.Add(Folder);
                //CategoryTreeNode temp = new CategoryTreeNode() { Name = " ", Level = 0, IsExpanded = true, ID = new Guid() };
                //TreeNodes.Add(temp);
                //TreeNodes.Remove(temp);
                EF.Dispose();
                EF = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        void tmrUpdate_Tick(object sender, EventArgs e)
        {
            tmrUpdate.Stop();
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            CategoryTreeNode ParentFolder = (CategoryTreeNode)FindNodeByID(FindID);
            if (ParentFolder.ID != AddNode.ID) ParentFolder.Nodes.Add(AddNode);

            FindID = Guid.Empty;
            AddNode = null;
        }

        private CuteTreeNode FindNodeByID(Guid ID)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return null;

            try
            {
                //Busqueda recursiva
                foreach (CuteTreeNode Node in TreeNodes)
                {
                    if (Node.ID == ID) return Node;
                    if (Node.GetType() == typeof(CategoryTreeNode))
                    {
                        CuteTreeNode node = FindNodeByID((CategoryTreeNode)Node, ID);
                        if (node != null) return node;
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private CuteTreeNode FindNodeByID(CategoryTreeNode ParentNode, Guid ID)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return null;

            try
            {
                //Busqueda recursiva
                foreach (CuteTreeNode Node in ParentNode.Nodes)
                {
                    if (Node.ID == ID) return Node;
                    if (Node.GetType() == typeof(CategoryTreeNode))
                    {
                        CuteTreeNode node = FindNodeByID((CategoryTreeNode)Node, ID);
                        if (node != null) return node;
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        private bool AddTemplate(CuteTreeNode parent)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                CategoryTreeNode MyParent = null;
                if (parent.GetType() == typeof(TemplateTreeNode)) return false; else MyParent = (CategoryTreeNode)parent;

                NewTemplate NT = new NewTemplate();
                NT.db = this.db;
                NT.Category = MyParent.ID;
                NT.Tipologia = this.Tipologia;
                //if (this.IDTemplate == "") NT.IDEditingTemplate = Guid.Empty; else NT.IDEditingTemplate = Guid.Parse(this.IDTemplate);
                NT.IDEditingTemplate = Guid.Empty;
                if (NT.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;

                AddTemplateNode(MyParent, NT.TemplateName, NT.ID, -1);
                Guid ntid = NT.ID;
                this.IDTemplate = ntid.ToString();
                NT.Dispose();
                NT = null;

                Editing = true;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool DeleteTemplate(TemplateTreeNode Template)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            if (OfflineMode) return false;
            try
            {
                using (SQLOperations SQL = new SQLOperations(db))
                {
                    if (SQL.DELETE_Plantilla(Template.ID))
                    {
                        Template.parent.Nodes.Remove(Template);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //private bool DeleteFolder(CategoryTreeNode ParentNode)
        //{
        //    try
        //    {
        //        //Busqueda recursiva
        //        for (int i = ParentNode.Nodes.Count - 1; i >= 0; i--)               
        //        {                   
        //            if (ParentNode.Nodes[i].GetType() == typeof(CategoryTreeNode))
        //            {
        //                if (!DeleteFolder((CategoryTreeNode)ParentNode.Nodes[i])) return false;
        //            }
        //            else
        //            {
        //                if (!DeleteTemplate((TemplateTreeNode)ParentNode.Nodes[i])) return false;
        //            }
        //        }
        //        return DeletFolder(ParentNode);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        private bool DeleteFolder(CategoryTreeNode Folder)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            if (OfflineMode) return false;
            try
            {
                using (SQLOperations SQL = new SQLOperations(db))
                {
                    if (SQL.DELETE_Categoria(Folder.ID, true, true))
                    {
                        Folder.parent.Nodes.Remove(Folder);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        System.Windows.Threading.DispatcherTimer tmrCheckDB = new System.Windows.Threading.DispatcherTimer();
        bool CheckingDB = false;

        void tmrCheckDB_Tick(object sender, EventArgs e)
        {
            tmrCheckDB.Stop();

            CheckDBFunction();

            tmrCheckDB.Start();
        }

        private void CheckDBFunction()
        {
            if (!CheckingDB)
            {
                System.Threading.Thread t = new System.Threading.Thread(CheckDBConection);
                t.Start();
            }
        }

        private void CheckDBConection()
        {
            CheckingDB = true;
            try
            {
                OfflineMode = !SQL.CheckConnection();
            }
            catch (Exception)
            {
            }
            CheckingDB = false;
        }

        public bool _OfflineMode = false;

        public bool OfflineMode
        {
            get
            {
                return _OfflineMode;
            }
            set
            {
                if (_OfflineMode != value)
                {
                    _OfflineMode = value;
                    if (_OfflineMode == false)
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Hidden; });
                    else
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Visible; });
                }
            }
        }
    }

    public abstract class CuteTreeNode
    {
        public int Level = 0;
        public bool m_IsExpanded = false;
        public Guid ID { get; set; }
        public bool IsExpanded
        {
            get
            {
                return m_IsExpanded;
            }
            set
            {
                if (Equals(m_IsExpanded, value)) return;
                m_IsExpanded = value;
            }
        }

        public CategoryTreeNode parent = null;
        public string Name { get; set; }
    }

    public class CategoryTreeNode : CuteTreeNode
    {
        public ObservableCollection<CuteTreeNode> Nodes { get; set; }
        public CategoryTreeNode()
        {
            this.Nodes = new ObservableCollection<CuteTreeNode>();
        }
    }

    public class TemplateTreeNode : CuteTreeNode
    {
        public int DefaultNumber { get; set; }
    }
}
