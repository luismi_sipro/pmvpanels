﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for EdicionPaneles.xaml
    /// </summary>
    public partial class HistoricoPaneles : SQLUserControl
    {
        public HistoricoPaneles()
        {
            InitializeComponent();
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            tmrStore.Interval = new TimeSpan(0, 0, 0, 0, 250);
            tmrStore.Tick += new EventHandler(tmrStore_Tick);
            tmrFullData.Interval = new TimeSpan(0, 0, 0, 0, 500);
            tmrFullData.Tick += new EventHandler(tmrFullData_Tick);
            tmrCheckDB.Interval = new TimeSpan(0, 0, 0, 10, 0);
            tmrCheckDB.Tick += new EventHandler(tmrCheckDB_Tick);          
            Button1.IsChecked = true;
        }

        string DefaultText12 = "123456789ABC";
        string DefaultText16 = "123456789ABCDEF0";
        int DefaultIcon = 146;

        int FilterMode = 0;
        int HistoricalPosition = 0;
        int NumeroAspectos = 0;
        char sepchar = '€';
        System.Windows.Threading.DispatcherTimer tmrStore = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer tmrFullData = new System.Windows.Threading.DispatcherTimer();
        SQLOperations SQL = null;

        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(string), typeof(HistoricoPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        public static readonly DependencyProperty AplicarPlantillaProperty = DependencyProperty.Register("AplicarPlantilla", typeof(bool), typeof(HistoricoPaneles), new UIPropertyMetadata(false, new PropertyChangedCallback(OnAplicarPlantillaChanged)));
        public static readonly DependencyProperty PanelDataProperty = DependencyProperty.Register("PanelData", typeof(string), typeof(HistoricoPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelDataChanged)));
     
        //System.Windows.Threading.DispatcherTimer tmrUpdate = new System.Windows.Threading.DispatcherTimer();      

        internal protected string _PanelData = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Datos del Panel Historico")]
        public string PanelData
        {
            get
            {
                if (_PanelData != (string)base.GetValue(PanelDataProperty)) base.SetValue(PanelDataProperty, _PanelData);
                return _PanelData;
            }
            set
            {
                if (null != value && value != _PanelData)
                {
                    _PanelData = value;
                    base.SetValue(PanelDataProperty, _PanelData);
                }
            }
        }

        internal protected string _Panel = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Instance Panel")]
        public string Panel
        {
            get
            {
                if (_Panel != (string)base.GetValue(PanelProperty)) base.SetValue(PanelProperty, _Panel);
                return _Panel;
            }
            set
            {
                if (null != value && value != _Panel)
                {
                    _Panel = value;
                    base.SetValue(PanelProperty, value);
                    if (!isInit) PreInitialize(); else InitControl();
                }
            }
        }


        internal protected bool _AplicarPlantilla = false;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Trigger to refresh historical data.")]
        public bool AplicarPlantilla
        {
            get
            {
                if (_AplicarPlantilla != (bool)base.GetValue(AplicarPlantillaProperty)) base.SetValue(AplicarPlantillaProperty, _AplicarPlantilla);
                return _AplicarPlantilla;
            }
            set
            {
                if (value != _AplicarPlantilla)
                {
                    _AplicarPlantilla = value;
                    base.SetValue(AplicarPlantillaProperty, value);
                    if (isInit) if (value) tmrStore.Start();
                }
            }
        }

        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((HistoricoPaneles)d).Panel != str1)
                ((HistoricoPaneles)d).Panel = str1;
        }


        private static void OnAplicarPlantillaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            bool bit = Convert.ToBoolean(e.NewValue);
            if (((HistoricoPaneles)d).AplicarPlantilla != bit)
                ((HistoricoPaneles)d).AplicarPlantilla = bit;
        }


        private static void OnPanelDataChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (!string.IsNullOrEmpty(str1))
                if (((HistoricoPaneles)d).PanelData != str1)
                    ((HistoricoPaneles)d).PanelData = str1;
        }

        protected override void InitControl()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            SQL = new SQLOperations(db);

            CheckDBFunction();
            tmrCheckDB.Start();

            Aspecto1.db = this.db;
            Aspecto2.db = this.db;
            Aspecto3.db = this.db;
            Aspecto4.db = this.db;

            comboBox.SelectedIndex = 0;
            LoadData();
        }

        bool LoadingInfo = false;

        int Tipologia = 0;

        private bool ClearData()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                Aspecto1.ID = Guid.Empty;
                Aspecto2.ID = Guid.Empty;
                Aspecto3.ID = Guid.Empty;
                Aspecto4.ID = Guid.Empty;

                this.Aspecto1.Icono1 = DefaultIcon;
                this.Aspecto1.Icono2 = DefaultIcon;
                this.Aspecto2.Icono1 = DefaultIcon;
                this.Aspecto2.Icono2 = DefaultIcon;
                this.Aspecto3.Icono1 = DefaultIcon;
                this.Aspecto3.Icono2 = DefaultIcon;
                this.Aspecto4.Icono1 = DefaultIcon;
                this.Aspecto4.Icono2 = DefaultIcon;

                this.Aspecto1.Linea1 = DefaultText16;
                this.Aspecto1.Linea2 = DefaultText16;
                this.Aspecto1.Linea3 = DefaultText16;
                this.Aspecto2.Linea1 = DefaultText16;
                this.Aspecto2.Linea2 = DefaultText16;
                this.Aspecto2.Linea3 = DefaultText16;
                this.Aspecto3.Linea1 = DefaultText16;
                this.Aspecto3.Linea2 = DefaultText16;
                this.Aspecto3.Linea3 = DefaultText16;
                this.Aspecto4.Linea1 = DefaultText16;
                this.Aspecto4.Linea2 = DefaultText16;
                this.Aspecto4.Linea3 = DefaultText16;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private bool SetFullData1()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                this.PanelData = "_";
                tmrFullData.Start();
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }

        private bool SetFullData2()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            //PanelData = "Tipologia€NumeroDeAspectos€A1Icono1€A1Icono2€A1Texto€A2Icono1€A12cono2€A2Texto€A3Icono1€A3Icono2€A3Texto€A4Icono1€A4Icono2€A4Texto"
            try
            {        
                string aux = this.Tipologia.ToString() + sepchar + this.NumeroAspectos.ToString();
                aux = aux + sepchar + this.Aspecto1.Icono1 + sepchar + this.Aspecto1.Icono2 + sepchar + this.Aspecto1.Linea1 + this.Aspecto1.Linea2 + this.Aspecto1.Linea3;
                aux = aux + sepchar + this.Aspecto2.Icono1 + sepchar + this.Aspecto2.Icono2 + sepchar + this.Aspecto2.Linea1 + this.Aspecto2.Linea2 + this.Aspecto2.Linea3;
                aux = aux + sepchar + this.Aspecto3.Icono1 + sepchar + this.Aspecto3.Icono2 + sepchar + this.Aspecto3.Linea1 + this.Aspecto3.Linea2 + this.Aspecto3.Linea3;
                aux = aux + sepchar + this.Aspecto4.Icono1 + sepchar + this.Aspecto4.Icono2 + sepchar + this.Aspecto4.Linea1 + this.Aspecto4.Linea2 + this.Aspecto4.Linea3;

                this.PanelData = aux;

                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }


        int NumChars = 12;

        private int GetTipology(string Panel)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return 0;

            if (OfflineMode) return 0;

            try
            {
                DataTable DataConfig = SQL.SELECT("SELECT [ID],[NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Instancia] WHERE Panel = '" + Panel + "')");

                foreach (DataRow crow in DataConfig.Rows)
                {
                    Tipologia = (int)crow["ID"];
                    int NumLines = (int)crow["NumLineas"];
                    int NumIcons = (int)crow["NumIconos"];
                    NumChars = (int)crow["NumChar"];
                    this.Aspecto1.NumLines = NumLines;
                    this.Aspecto1.NumIcons = NumIcons;
                    this.Aspecto1.NumChars = NumChars;
                    this.Aspecto2.NumLines = NumLines;
                    this.Aspecto2.NumIcons = NumIcons;
                    this.Aspecto2.NumChars = NumChars;
                    this.Aspecto3.NumLines = NumLines;
                    this.Aspecto3.NumIcons = NumIcons;
                    this.Aspecto3.NumChars = NumChars;
                    this.Aspecto4.NumLines = NumLines;
                    this.Aspecto4.NumIcons = NumIcons;
                    this.Aspecto4.NumChars = NumChars;
                }
                return Tipologia;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private DateTime GetDateTime(int FilterMode, int HistoricalPosition)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return DateTime.MinValue;
            if (OfflineMode) return DateTime.MinValue;
            if (HistoricalPosition == 0) HistoricalPosition = 1;
            try
            {
                string dataquery = "";

                switch (FilterMode)
                {
                    case 0:
                        dataquery = "SELECT DISTINCT TOP(11) [FechaHora] FROM [Historico] WHERE Panel = '" + this.Panel + "' ORDER BY 1 DESC";
                        break;
                    case 1:
                        dataquery = "SELECT DISTINCT TOP(11) [FechaHora] FROM [Historico] WHERE Tipologia = '" + this.Tipologia + "' ORDER BY 1 DESC";
                        break;
                }

                DataTable Fechas = SQL.SELECT(dataquery);

                return (DateTime)Fechas.Select("FechaHora = FechaHora", "FechaHora DESC")[HistoricalPosition-1]["FechaHora"];
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        private bool LoadData()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            if (OfflineMode) return false;
            try
            {
                LoadingInfo = true;

                NumeroAspectos = 0;

                ClearData();

                GetTipology(this.Panel);

                DateTime Fecha = GetDateTime(this.FilterMode, this.HistoricalPosition);


                string query = "";
                switch (this.FilterMode)
                {
                    case 0:
                        query = "SELECT * FROM [Historico] WHERE Panel = '" + this.Panel + "' AND FechaHora = '" + GlobalStatic.DateToSQLString(Fecha) + "'";
                        break;
                    case 1:
                        query = "SELECT * FROM [Historico] WHERE Tipologia =  '" + this.Tipologia + "' AND FechaHora = '" + GlobalStatic.DateToSQLString(Fecha) + "'";
                        break;
                }

                DataTable HistoricalData = SQL.SELECT(query);

                NumeroAspectos = HistoricalData.Rows.Count;
                UpdateNumAspectos();

                foreach (DataRow arow in HistoricalData.Select("ID = ID", "NumeroAspecto ASC"))
                {
                    int NumAspecto = (int)arow["NumeroAspecto"];

                    switch (NumAspecto)
                    {
                        case 1:
                            Aspecto1.IsEnabled = false;
                            if (arow["Icono1"] == DBNull.Value) Aspecto1.Icono1 = 0; else Aspecto1.Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto1.Icono2 = 0; else Aspecto1.Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto1.Linea1 = Convert.ToString(arow["Linea1"]).PadRight(NumChars);
                            Aspecto1.Linea2 = Convert.ToString(arow["Linea2"]).PadRight(NumChars);
                            Aspecto1.Linea3 = Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            Aspecto1.UpdateData();
                            break;
                        case 2:
                            Aspecto2.IsEnabled = false;
                            if (arow["Icono1"] == DBNull.Value) Aspecto2.Icono1 = 0; else Aspecto2.Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto2.Icono2 = 0; else Aspecto2.Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto2.Linea1 = Convert.ToString(arow["Linea1"]).PadRight(NumChars);
                            Aspecto2.Linea2 = Convert.ToString(arow["Linea2"]).PadRight(NumChars);
                            Aspecto2.Linea3 = Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            Aspecto2.UpdateData();
                            break;
                        case 3:
                            Aspecto3.IsEnabled = false;
                            if (arow["Icono1"] == DBNull.Value) Aspecto3.Icono1 = 0; else Aspecto3.Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto3.Icono2 = 0; else Aspecto3.Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto3.Linea1 = Convert.ToString(arow["Linea1"]).PadRight(NumChars);
                            Aspecto3.Linea2 = Convert.ToString(arow["Linea2"]).PadRight(NumChars);
                            Aspecto3.Linea3 = Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            Aspecto3.UpdateData();
                            break;
                        case 4:
                            Aspecto4.IsEnabled = false;
                            if (arow["Icono1"] == DBNull.Value) Aspecto4.Icono1 = 0; else Aspecto4.Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto4.Icono2 = 0; else Aspecto4.Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto4.Linea1 = Convert.ToString(arow["Linea1"]).PadRight(NumChars);
                            Aspecto4.Linea2 = Convert.ToString(arow["Linea2"]).PadRight(NumChars);
                            Aspecto4.Linea3 = Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            Aspecto4.UpdateData();
                            break;
                        default:
                            break;
                    }

                }

                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }

        void tmrStore_Tick(object sender, EventArgs e)
        {
            tmrStore.Stop();

            if (DesignerProperties.GetIsInDesignMode(this)) return;

            //this.AplicarPlantilla = false;

            LoadData();
        }

        void tmrFullData_Tick(object sender, EventArgs e)
        {
            tmrFullData.Stop();

            if (DesignerProperties.GetIsInDesignMode(this)) return;

            SetFullData2();

        }

        private bool UpdateNumAspectos()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                LoadingInfo = true;

                switch (NumeroAspectos)
                {
                    case 0:
                        Aspecto1.Visibility = Visibility.Hidden;
                        Aspecto2.Visibility = Visibility.Hidden;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Hidden;
                        n2.Visibility = Visibility.Hidden;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 1:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Hidden;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Hidden;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 3:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Visible;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Visible;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 4:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Visible;
                        Aspecto4.Visibility = Visibility.Visible;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Visible;
                        n4.Visibility = Visibility.Visible;
                        break;
                }

                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }


        private void x0_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x0.IsChecked == true)
            {
                HistoricalPosition = 0;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                LoadData();
            }


        }

        private void x1_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x1.IsChecked == true)
            {
                x0.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 1;
                LoadData();
            }
        }

        private void x2_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x2.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 2;
                LoadData();
            }
        }

        private void x3_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x3.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 3;
                LoadData();
            }
        }

        private void x4_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x4.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 4;
                LoadData();
            }
        }

        private void x5_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x5.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 5;
                LoadData();
            }
        }

        private void x6_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x6.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 6;
                LoadData();
            }
        }

        private void x7_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x7.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x8.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 7;
                LoadData();
            }
        }

        private void x8_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x8.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x9.IsChecked = false;
                HistoricalPosition = 8;
                LoadData();
            }
        }

        private void x9_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x9.IsChecked == true)
            {
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
                x5.IsChecked = false;
                x6.IsChecked = false;
                x7.IsChecked = false;
                x8.IsChecked = false;
                HistoricalPosition = 9;
                LoadData();
            }
        }


        private void bSeleccionar_Click(object sender, RoutedEventArgs e)
        {

            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            SetFullData1();
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (Button1.IsChecked == true)
            {
                Button2.IsChecked = false;
                FilterMode = 0;
                LoadData();
            }
        }

        private void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            {
                Button1.IsChecked = false;
                FilterMode = 1;
                LoadData();
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (OfflineMode) return;
            if (!isInit) return;
            if (LoadingInfo) return;

            HistoricalPosition = Convert.ToInt32(((System.Windows.Controls.Label)comboBox.SelectedItem).Content);
            LoadData();

        }

        System.Windows.Threading.DispatcherTimer tmrCheckDB = new System.Windows.Threading.DispatcherTimer();
        bool CheckingDB = false;

        void tmrCheckDB_Tick(object sender, EventArgs e)
        {
            tmrCheckDB.Stop();

            CheckDBFunction();

            tmrCheckDB.Start();
        }

        private void CheckDBFunction()
        {
            if (!CheckingDB)
            {
                System.Threading.Thread t = new System.Threading.Thread(CheckDBConection);
                t.Start();
            }
        }

        private void CheckDBConection()
        {
            CheckingDB = true;
            try
            {
                OfflineMode = !SQL.CheckConnection();
            }
            catch (Exception)
            {
            }
            CheckingDB = false;
        }

        public bool _OfflineMode = false;

        public bool OfflineMode
        {
            get
            {
                return _OfflineMode;
            }
            set
            {
                if (_OfflineMode != value)
                {
                    _OfflineMode = value;
                    if (_OfflineMode == false)
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Hidden; });
                    else
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Visible; });
                }
            }
        }
    }
}
