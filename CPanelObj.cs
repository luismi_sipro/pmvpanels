﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelsControls
{
    public class CPanelObj
    {
        #region PROPERTIES

        public String Id
        {
            get;
            set;
        }

        public String Tunnel
        {
            get;
            set;
        }

        public String Panel
        {
            get;
            set;
        }

        public String IPAddress
        {
            get;
            set;
        }

        public String Type
        {
            get;
            set;
        }
        #endregion

    }
}
