﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

//c2 namespaces
using Sipro.Utilities.Error;

namespace Sipro.Database
{
    /// <summarysssssssss
    /// Base class for inheriting connection with different database technologies.
    /// </summary>
    public abstract class CDataBase
    {
        #region EVENTS
        #endregion
        
        #region MEMBERS
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Get error.
        /// </summary>
        public CErrorDataBase Error
        {
            get;
            private set;
        }

        /// <summary>
        /// Get connection string.
        /// </summary>
        public String ConnectionString
        {
            get;
            private set;
        }

        /// <summary>
        /// Get data base connection.
        /// </summary>
        public abstract String DataBase
        {
            get;
        }

        /// <summary>
        /// Gets the user connection.
        /// </summary>
        public abstract String User
        {
            get;
        }

        /// <summary>
        /// Gets the user password connection.
        /// </summary>
        public abstract String Password
        {
            get;
        }

        /// <summary>
        /// Gets database server name.
        /// </summary>
        public abstract String Server
        {
            get;
        }


        /// <summary>
        /// Return the state of the connection.
        /// </summary>
        public abstract ConnectionState State
        {
            get;
        }
        

        #endregion
        
        #region CONSTRUCTORS

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="mConnectionStr"></param>
        public CDataBase(String mConnectionStr)
        {
            this.ConnectionString = mConnectionStr;
        }
        #endregion

        #region METHODS: Abstract

        /// <summary>
        /// Connect to defined database.
        /// </summary>
        /// <returns>True, if it's succesfull.</returns>
        public abstract Boolean Connect();

        /// <summary>
        /// Execute query and get the result.
        /// </summary>
        /// <param name="mQuery">Complete query to ask to database.</param>
        /// <returns>Result of query.</returns>
        public abstract CDataBaseResult ExecuteQuery(String mQuery);

        /// <summary>
        /// Close database connection.
        /// </summary>
        public abstract void Close();
        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion
    }

    /// <summary>
    /// Store result of query.
    /// </summary>
    public class CDataBaseResult
    {
        #region EVENTS
        #endregion

        #region MEMBERS

        private List<CDataBaseRow> m_rows;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Gets rows of query result.
        /// </summary>
        public CDataBaseRow[] Rows
        {
            get { return m_rows.ToArray(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean HasRows
        {
            get;
            private set;
        }
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Default constructor
        /// </summary>
        internal CDataBaseResult(Boolean mHasRows)
        {
            m_rows = new List<CDataBaseRow>();
            this.HasRows = mHasRows;
        }
        #endregion

        #region METHODS

        /// <summary>
        /// Add new row to data base result.
        /// </summary>
        /// <param name="mRow"></param>
        public void AddRow(CDataBaseRow mRow)
        {
            this.m_rows.Add(mRow);
        }
        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion
    }

    /// <summary>
    /// Store complete row of result.
    /// </summary>
    public class CDataBaseRow
    {
        #region EVENTS
        #endregion

        #region MEMBERS

        private List<CDataBaseCell> m_cells;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Number of columns.
        /// </summary>
        public Int32 NumberOfColumns
        {
            get { return m_cells.Count; }
        }

        /// <summary>
        /// Gets cells array.
        /// </summary>
        public CDataBaseCell[] Cells
        {
            get { return this.m_cells.ToArray(); }
        }
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Class constructor.
        /// </summary>
        public CDataBaseRow()
        {
            m_cells = new List<CDataBaseCell>();
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Add cell to row.
        /// </summary>
        /// <param name="mCell"></param>
        public void Add(CDataBaseCell mCell)
        {
            this.m_cells.Add(mCell);
        }

        /// <summary>
        /// Gets cell value from column id.
        /// </summary>
        /// <param name="mColumnId">Index of column of query results.</param>
        /// <returns>Value of cell.</returns>
        public Object GetValue(UInt16 mColumnId)
        {
            CDataBaseCell _cell = m_cells.Find(x => x.ColumnIndex == mColumnId);
            if (_cell == null)
                return null;
            return _cell.Value;
        }

        /// <summary>
        /// Gets cell value from column name.
        /// </summary>
        /// <param name="mColumnName">Name of column of query results.</param>
        /// <returns>Value of cell.</returns>
        public Object GetValue(String mColumnName)
        {
            CDataBaseCell _cell = m_cells.Find(x => x.ColumnName == mColumnName);
            if (_cell == null)
                return null;
            return _cell.Value;
        }

        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion

    }

    /// <summary>
    /// Struct of result column information.
    /// </summary>
    public class CDataBaseColumn
    {
        #region EVENTS
        #endregion

        #region MEMBERS
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Numeric index of column.
        /// </summary>
        public UInt16 Index
        {
            get;
            private set;
        }

        /// <summary>
        /// Name of column.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="mIndex">Number of column</param>
        /// <param name="mName">Name of column</param>
        public CDataBaseColumn(UInt16 mIndex, String mName)
        {
            this.Index = mIndex;
            this.Name = mName;
        }

        #endregion

        #region METHODS
        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion
    }

    /// <summary>
    /// Struct of specific result cell.
    /// </summary>
    public class CDataBaseCell
    {
        #region EVENTS
        #endregion


        #region MEMBERS

        private CDataBaseColumn m_colData;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Numeric index of column.
        /// </summary>
        public UInt16 ColumnIndex
        {
            get { return this.m_colData.Index; }
        }

        /// <summary>
        /// Name of column.
        /// </summary>
        public String ColumnName
        {
            get { return this.m_colData.Name; }
        }
        #endregion

        /// <summary>
        /// Cell value.
        /// </summary>
        public Object Value
        {
            get;
            private set;
        }

        #region CONSTRUCTORS

        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="mIndex">Number of column</param>
        /// <param name="mName">Name of column</param>
        public CDataBaseCell(UInt16 mIndex, String mName, Object mValue)
        {
            this.m_colData = new CDataBaseColumn(mIndex, mName);
            this.Value = mValue;
        }

        #endregion

        #region METHODS
        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion
    }

    /// <summary>
    /// Error class for managing database errors.
    /// </summary>
    public class CErrorDataBase : CErrorInfo
    {
        #region EVENTS
        #endregion

        #region MEMBERS
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Name of database.
        /// </summary>
        public String DataBaseName
        {
            get;
            private set;
        }
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="mError">Number of error.</param>
        /// <param name="mInfo">Specific information.</param>
        /// <param name="mDBName">Name of database</param>
        public CErrorDataBase(Int16 mError, String mInfo, String mDBName)
            :base(mError, mInfo)
        {
            this.DataBaseName = mDBName;
        }
        #endregion

        #region METHODS

        /// <summary>
        /// Print the database error summary.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + String.Format("\n[DB Name]: {0}", this.DataBaseName);
        }
        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLERS
        #endregion
    }


    /// <summary>
    /// Specific exception for database classes.
    /// </summary>
    public class EDataBaseException : Exception
    {
        #region CONSTRUCTORS

        public EDataBaseException()
        {

        }

        public EDataBaseException (String mMessage)
            :base(mMessage)
        {

        }

        public EDataBaseException (String mMessage, Exception mInner)
            :base(mMessage, mInner)
        {

        }

        #endregion
    }

}
