﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;
using System.Data;

namespace PanelsControls
{
    public class SQLOperations : IDisposable
    {

        SQLServer db = null;
        public SQLOperations(SQLServer db)
        {
            this.db = db;
        }

        // Flag: Has Dispose already been called?
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }

        public bool INSERT_Categoria(Guid ID, string Nombre, Guid ParentID)
        {
            try
            {
                string executequery = "INSERT INTO [Categoria] VALUES('" + ID + "','" + Nombre + "','" + ParentID + "')";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UPDATE_Categoria(Guid ID, string Nombre)
        {
            try
            {
                string executequery = "UPDATE [Categoria] SET [Nombre] = '" + Nombre + "' WHERE ID = '" + ID + "'";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UPDATE_Plantilla(Guid ID, string Nombre, int NumeroAspectos)
        {
            try
            {
                string executequery = "UPDATE [Plantilla] SET [Nombre] = '" + Nombre + "', [NumeroAspectos] = " + NumeroAspectos + " WHERE ID = '" + ID + "'";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool INSERT_Plantilla(Guid ID, string Nombre, int NumeroAspectos, Guid Categoria, int Tipologia)
        {
            try
            {
                string executequery = "INSERT INTO [Plantilla] VALUES('" + ID + "','" + Nombre + "'," + NumeroAspectos + ",'" + Categoria + "'," + Tipologia + ",NULL)";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool INSERT_Plantilla_Defecto(Guid ID, string Nombre, int NumeroAspectos, Guid Categoria, int Tipologia, int NumeroPorDefecto)
        {
            try
            {
                string executequery = "INSERT INTO [Plantilla] VALUES('" + ID + "','" + Nombre + "'," + NumeroAspectos + ",'" + Categoria + "'," + Tipologia + "," + NumeroPorDefecto + ")";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool INSERT_Historico(string Panel, int Tipologia, int NumeroAspecto, DateTime Fecha, int Icono1, int Icono2, string Linea1, string Linea2, string Linea3, int NumChar = 16)
        {
            try
            {
                if (string.IsNullOrEmpty(Linea1)) Linea1 = "";
                if (string.IsNullOrEmpty(Linea2)) Linea2 = "";
                if (string.IsNullOrEmpty(Linea3)) Linea3 = "";
                Linea1 = Linea1.PadRight(NumChar).Substring(0, NumChar);
                Linea2 = Linea2.PadRight(NumChar).Substring(0, NumChar);
                Linea3 = Linea3.PadRight(NumChar).Substring(0, NumChar);
                string i1 = Icono1.ToString(); if (i1 == "0") i1 = "NULL";
                string i2 = Icono2.ToString(); if (i2 == "0") i2 = "NULL";

                string executequery = "INSERT INTO [Historico] VALUES('" + Guid.NewGuid() + "','" + Panel + "'," + Tipologia + "," + NumeroAspecto + ",'" + GlobalStatic.DateToSQLString(Fecha) + "'," + i1 + "," + i2 + ",'" + Linea1 + "','" + Linea2 + "','" + Linea3 + "')";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        public bool DELETE_Plantilla(Guid ID)
        {
            try
            {
                string query = "SELECT * FROM [Plantilla] WHERE ID = '" + ID + "' AND [NumeroPorDefecto] IS NULL";
                if (db.GetDataTable(query).Rows.Count == 0) return false; //No se puede borrar una plantilla por defecto
                string executequery = "DELETE [Aspecto] WHERE Plantilla = '" + ID + "'";
                int ret = db.Execute(executequery);
                executequery = "DELETE [Plantilla] WHERE ID = '" + ID + "'";
                ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckConnection()
        {
            try
            {
                return db.CheckConnection();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DELETE_Aspecto_byPlantilla(Guid ID)
        {
            try
            {
                string executequery = "DELETE [Aspecto] WHERE Plantilla = '" + ID + "'";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Create_Tunel_Column()
        {
            try
            {
                string Query = "SELECT * FROM sys.tables WHERE Name = N'Tunel' AND Object_ID = Object_ID(N'Instancia')";
                DataTable table = db.GetDataTable(Query);
                if (table.Rows.Count == 0)
                {
                    Query = "ALTER TABLE [Instancia] ADD [Tunel] nvarchar(255) NULL";
                    int ret = db.Execute(Query);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool INSERT_PlantillasPorDefecto()
        {
            try
            {
                string DefaultCat = "00000000-0000-0000-0000-000000000001";
                string query = "SELECT * FROM [Categoria] WHERE ID = '" + DefaultCat + "'";
                if (db.GetDataTable(query).Rows.Count == 0) INSERT_Categoria(Guid.Parse(DefaultCat), " Planes Automáticos", Guid.Empty);

                query = "SELECT * FROM [Tipologia]";
                DataTable Tipologías = db.GetDataTable(query);

                foreach (DataRow Tipologia in Tipologías.Rows)
                {
                    for (int NumeroPorDefecto = 1; NumeroPorDefecto <= 5; NumeroPorDefecto++)
                    {
                        query = "SELECT * FROM [Plantilla] WHERE [Tipologia] = " + Tipologia["ID"] + " AND [NumeroPorDefecto] = " + NumeroPorDefecto;
                        if (db.GetDataTable(query).Rows.Count == 0) INSERT_Plantilla_Defecto(Guid.NewGuid(), "PERSONALIZADO" + NumeroPorDefecto, 0, Guid.Parse(DefaultCat), Convert.ToInt32(Tipologia["ID"]), NumeroPorDefecto);
                    }
                }

                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }


        public bool UPDATE_2()
        {
            try
            {
                string query = "SELECT * FROM [Tipologia] WHERE ID = 1116";
                DataTable Tipologías = db.GetDataTable(query);
                if (db.LastError != "") return false;
                if (Tipologías.Rows.Count == 0)
                {
                    string executequery = "ALTER TABLE Tipologia ADD NumChar int NOT NULL CONSTRAINT DF_Tipologia_NumChar DEFAULT 12";
                    int ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                    executequery = "INSERT INTO [Tipologia] VALUES(1116,'16L1I','1 Linea de 16 caracteres y 1 Icono',1,1,16)";
                    ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                    executequery = "UPDATE Plantilla SET Tipologia = 1116 WHERE Tipologia = 1016";
                    ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                    executequery = "UPDATE Instancia SET Tipologia = 1116 WHERE Tipologia = 1016";
                    ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                    executequery = "UPDATE Historico SET Tipologia = 1116 WHERE Tipologia = 1016";
                    ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                    executequery = "DELETE Tipologia WHERE ID = 1016";
                    ret = db.Execute(executequery);
                    if (db.LastError != "") return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool INSERT_Aspecto(Guid ID, Guid Plantilla, int NumeroAspecto, int Icono1, int Icono2, string Linea1, string Linea2, string Linea3, int NumChar = 16)
        {
            try
            {
                if (string.IsNullOrEmpty(Linea1)) Linea1 = "".PadRight(NumChar); else Linea1 = Linea1.PadRight(NumChar);
                if (string.IsNullOrEmpty(Linea2)) Linea2 = "".PadRight(NumChar); else Linea2 = Linea2.PadRight(NumChar);
                if (string.IsNullOrEmpty(Linea3)) Linea3 = "".PadRight(NumChar); else Linea3 = Linea3.PadRight(NumChar);
                string i1 = Icono1.ToString(); if (i1 == "0") i1 = "NULL";
                string i2 = Icono2.ToString(); if (i2 == "0") i2 = "NULL";
                string executequery = "INSERT INTO [Aspecto] VALUES('" + ID + "','" + Plantilla + "'," + NumeroAspecto + "," + i1 + "," + i2 + ",'" + Linea1 + "','" + Linea2 + "','" + Linea3 + "')";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DELETE_Categoria(Guid ID, bool includeChildren, bool includeTemplates)
        {
            try
            {
                if (includeChildren)
                {
                    using (DataTable Categorias = SELECT_Categoria_byParentID(ID))
                    {
                        foreach (DataRow row in Categorias.Rows)
                        {
                            if (!DELETE_Categoria(Guid.Parse(row["ID"].ToString()), includeChildren, includeTemplates)) return false;
                        }
                    }
                }

                if (includeTemplates)
                {
                    using (DataTable Plantillas = SELECT_Plantilla_byCategoria(ID))
                    {
                        foreach (DataRow row in Plantillas.Rows)
                        {
                            if (!DELETE_Plantilla(Guid.Parse(row["ID"].ToString()))) return false;
                        }
                    }
                }

                string executequery = "DELETE [Categoria] WHERE ID = '" + ID + "'";
                int ret = db.Execute(executequery);
                if (ret != 1) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable SELECT_Plantilla_byID(Guid ID)
        {
            try
            {
                string query = "SELECT * FROM [Plantilla] WHERE ID = '" + ID + "'";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT_Icono_byTipoIcono(int ID)
        {
            try
            {
                string query = "SELECT * FROM [Icono] WHERE TipoIcono = " + ID;
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT_Icono_byID(int ID)
        {
            try
            {
                string query = "SELECT * FROM [Icono] WHERE ID = " + ID;
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DataTable SELECT_Plantilla_byCategoria(Guid Categoria)
        {
            try
            {
                string query = "SELECT * FROM [Plantilla] WHERE Categoria = '" + Categoria + "'";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT_Plantilla_byTipologia(int Tipologia)
        {
            try
            {
                string query = "SELECT * FROM [Plantilla] WHERE Tipologia = " + Tipologia;
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DataTable SELECT_Categoria_byParentID(Guid ParentID)
        {
            try
            {
                string query = "SELECT * FROM [Categoria] WHERE ParentID = '" + ParentID + "'";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT_Aspecto_byID(Guid ID)
        {
            try
            {
                string query = "SELECT * FROM [Aspecto] WHERE [ID] = '" + ID + "'";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }

        }
        public DataTable SELECT_Aspecto_byPlantilla(Guid ID)
        {
            try
            {
                string query = "SELECT * FROM [Aspecto] WHERE [Plantilla] = '" + ID + "'";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT(string query)
        {
            try
            {
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable SELECT_ALL_Table(string TableName)
        {
            try
            {
                string query = "SELECT * FROM [" + TableName + "]";
                return db.GetDataTable(query);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
