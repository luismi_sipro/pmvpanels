﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelControl.xaml
    /// </summary>
    public partial class PanelControl : UserControl
    {
        string DefaultText12 = "123456789ABC";
        string DefaultText16 = "123456789ABCDEF0";
        int DefaultIcon = 146;

        public PanelControl()
        {
            InitializeComponent();
            tmrResize.Interval = new TimeSpan(0, 0, 0, 0, 100);
            tmrResize.Tick += new EventHandler(tmrResize_Tick);
            RedrawControl();
        }

        public static readonly DependencyProperty IDProperty = DependencyProperty.Register("ID", typeof(Guid), typeof(PanelControl), new UIPropertyMetadata(Guid.Empty, new PropertyChangedCallback(OnIDChanged)));

        private static void OnIDChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (e.NewValue == null) return;
                Guid str1 = Guid.Parse(e.NewValue.ToString());
                if (((PanelControl)d).ID != str1)
                    ((PanelControl)d).ID = str1;
            }
            catch (Exception)
            {
            }
        }

        System.Windows.Threading.DispatcherTimer tmrResize = new System.Windows.Threading.DispatcherTimer();

        void tmrResize_Tick(object sender, EventArgs e)
        {
            tmrResize.Stop();

            if (DinamicW > 0) PanelGrid.Width = PanelGrid.ActualHeight * DinamicW;
            if (DinamicH > 0) PanelGrid.Height = PanelGrid.ActualWidth * DinamicH;

            if (PanelGrid.Height != Double.NaN)
            {
                try
                {
                    if (Col1.ActualWidth > 0) Col1.Width = new GridLength(PanelGrid.Height, GridUnitType.Pixel);
                    //if (Col2.ActualWidth > 0) Col2.Width = new GridLength(PanelGrid.Height * 3, GridUnitType.Pixel);
                    if (Col3.ActualWidth > 0) Col3.Width = new GridLength(PanelGrid.Height, GridUnitType.Pixel);
                }
                catch (Exception)
                {

                }
            }

            DinamicW = 0;
            DinamicH = 0;


            //if (PanelGrid.ActualHeight > this.ActualHeight)
            //{
            //    double prop = PanelGrid.ActualHeight / PanelGrid.ActualWidth;
            //    PanelGrid.Height = this.ActualHeight;
            //    PanelGrid.Width = PanelGrid.Width / prop;
            //}

            LoadingGrid.Visibility = Visibility.Hidden;
        }

        double DinamicW = 0;
        double DinamicH = 0;

        int _numlines = 3;
        int _numicons = 2;
        int _numchars = 12;
        public int NumLines
        {
            get
            {
                return _numlines;
            }
            set
            {
                _numlines = value;
                RedrawControl();
            }
        }

        public int NumIcons
        {
            get
            {
                return _numicons;
            }
            set
            {
                _numicons = value;
                RedrawControl();
            }
        }

        public int NumChars
        {
            get
            {
                return _numchars;
            }
            set
            {
                _numchars = value;

                if (_numchars == 12)
                {
                    this.PanelLinea16.Visibility = Visibility.Hidden;
                    this.PanelLinea26.Visibility = Visibility.Hidden;
                    this.PanelLinea36.Visibility = Visibility.Hidden;
                    this.PanelLinea1.Visibility = Visibility.Visible;
                    this.PanelLinea2.Visibility = Visibility.Visible;
                    this.PanelLinea3.Visibility = Visibility.Visible;
                }

                if (_numchars == 16)
                {
                    this.PanelLinea1.Visibility = Visibility.Hidden;
                    this.PanelLinea2.Visibility = Visibility.Hidden;
                    this.PanelLinea3.Visibility = Visibility.Hidden;
                    this.PanelLinea16.Visibility = Visibility.Visible;
                    this.PanelLinea26.Visibility = Visibility.Visible;
                    this.PanelLinea36.Visibility = Visibility.Visible;
                }

                RedrawControl();
            }
        }


        SQLServer _db = null;
        SQLOperations SQL = null;

        public SQLServer db
        {
            get
            {
                return _db;
            }
            set
            {
                _db = value;
                SQL = new SQLOperations(db);
            }
        }


        internal protected Guid _ID = Guid.Empty;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Template Aspect Unique ID")]
        public Guid ID
        {
            get
            {
                if (_ID != (Guid)base.GetValue(IDProperty)) base.SetValue(IDProperty, _ID);
                return _ID;
            }
            set
            {
                if (null != value)
                {
                    _ID = value;
                    base.SetValue(IDProperty, value);
                    LoadData();
                }
            }
        }

        int _Icono1 = -1;
        int _Icono2 = -1;
        string _Linea1 = "";
        string _Linea2 = "";
        string _Linea3 = "";
        public int Icono1 { get { return _Icono1; } set { /*if (value <= 0) _Icono1 = 146; else*/ _Icono1 = value; } }
        public int Icono2 { get { return _Icono2; } set { /*if (value <= 0) _Icono2 = 146; else*/ _Icono2 = value; } }
        public string Linea1 { get { return _Linea1; } set { _Linea1 = value; } }
        public string Linea2 { get { return _Linea2; } set { _Linea2 = value; } }
        public string Linea3 { get { return _Linea3; } set { _Linea3 = value; } }

        private bool ClearData()
        {
            try
            {
                LoadIcon(DefaultIcon, this.Icon1);
                LoadIcon(DefaultIcon, this.Icon2);

                LoadLine12(DefaultText12, this.PanelLinea1);
                LoadLine12(DefaultText12, this.PanelLinea2);
                LoadLine12(DefaultText12, this.PanelLinea3);

                LoadLine16(DefaultText16, this.PanelLinea16);
                LoadLine16(DefaultText16, this.PanelLinea26);
                LoadLine16(DefaultText16, this.PanelLinea36);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool UpdateData()
        {
            try
            {
                /*if (NumIcons > 0)*/ LoadIcon(this.Icono1, this.Icon1);
                /*if (NumIcons > 1)*/ LoadIcon(this.Icono2, this.Icon2);

                if (NumChars == 12)
                {
                    /*if (NumLines > 0)*/ LoadLine12(Linea1, this.PanelLinea1);
                    /*if (NumLines > 1)*/ LoadLine12(Linea2, this.PanelLinea2);
                   /* if (NumLines > 2)*/ LoadLine12(Linea3, this.PanelLinea3);
                }

                if (NumChars == 16)
                {
                   /* if (NumLines > 0)*/ LoadLine16(Linea1, this.PanelLinea16);
                   /* if (NumLines > 1)*/ LoadLine16(Linea2, this.PanelLinea26);
                   /* if (NumLines > 2)*/ LoadLine16(Linea3, this.PanelLinea36);
                }
                return true;
                           
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadData()
        {
            ClearData();
            try
            {
                if (OfflineMode) return false;
                if (db == null) return false;
                if (SQL == null) return false;

                DataTable Aspecto = SQL.SELECT_Aspecto_byID(this.ID);

                foreach (DataRow arow in Aspecto.Rows)
                {
                    DataTable DataConfig = SQL.SELECT("SELECT [NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Plantilla] WHERE ID = '" + arow["ID"] + "')");
                    foreach (DataRow crow in DataConfig.Rows)
                    {
                        NumLines = (int)crow["NumLineas"];
                        NumIcons = (int)crow["NumIconos"];
                        NumChars = (int)crow["NumChar"];
                    }

                    if (arow["Icono1"] == DBNull.Value) Icono1 = 0; else Icono1 = (int)arow["Icono1"];
                    if (arow["Icono2"] == DBNull.Value) Icono1 = 0; else Icono2 = (int)arow["Icono2"];

                    Linea1 = (string)arow["Linea1"];
                    Linea2 = (string)arow["Linea2"];
                    Linea3 = (string)arow["Linea3"];

                    if (NumIcons > 0) LoadIcon(Icono1, this.Icon1);
                    if (NumIcons > 1) LoadIcon(Icono2, this.Icon2);

                    Linea1 = Linea1.PadRight(NumChars).Substring(0, NumChars);
                    Linea2 = Linea2.PadRight(NumChars).Substring(0, NumChars);
                    Linea3 = Linea3.PadRight(NumChars).Substring(0, NumChars);

                    if (NumChars == 12)
                    {
                        if (NumLines > 0) LoadLine12(Linea1, this.PanelLinea1);
                        if (NumLines > 1) LoadLine12(Linea2, this.PanelLinea2);
                        if (NumLines > 2) LoadLine12(Linea3, this.PanelLinea3);
                    }

                    if (NumChars == 16)
                    {
                        if (NumLines > 0) LoadLine16(Linea1, this.PanelLinea16);
                        if (NumLines > 1) LoadLine16(Linea2, this.PanelLinea26);
                        if (NumLines > 2) LoadLine16(Linea3, this.PanelLinea36);
                    }
                }


                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadLine12(string Text, PanelText12 PanelText)
        {
            try
            {
                PanelText.Text12 = Text;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadLine16(string Text, PanelText16 PanelText)
        {
            try
            {
                PanelText.Text16 = Text;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool _OfflineMode = false;

        public bool OfflineMode
        {
            get
            {
                return _OfflineMode;
            }
            set
            {
                if (_OfflineMode != value)
                {
                    _OfflineMode = value;
                    //if (_OfflineMode == false)
                    //    this.Dispatcher.Invoke((Action)delegate { this.BottomFill.Fill = new SolidColorBrush(Colors.Black); });
                    //else
                    //    this.Dispatcher.Invoke((Action)delegate { this.BottomFill.Fill = new SolidColorBrush(Colors.DarkGoldenrod); });
                }
            }
        }

        private bool LoadIcon(int IconNum, Button Button)
        {
            return LoadIconOffline(IconNum, Button);

            //if (DesignerProperties.GetIsInDesignMode(this)) return false;

            //if (OfflineMode) return LoadIconOffline(IconNum, Button);

            //try
            //{
            //    Button.Content = new Image
            //    {                    
            //        Source = GlobalStatic.GetImageFromStream(GetIconData(IconNum)),
            //        VerticalAlignment = VerticalAlignment.Stretch,
            //        HorizontalAlignment = HorizontalAlignment.Stretch
            //    };
            //    return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}

        }

        System.Resources.ResourceManager rm = Properties.Resources.ResourceManager;

        private bool LoadIconOffline(int IconNum, Button Button)
        {
            //if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                object img = rm.GetObject("pmv" + IconNum);
                Stream img2 = GlobalStatic.ToStream((System.Drawing.Image)img, System.Drawing.Imaging.ImageFormat.Png);

                Button.Content = new Image
                {
                    Source = GlobalStatic.GetImageFromStream(img2),
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalAlignment = HorizontalAlignment.Stretch
                };
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private byte[] GetIconData(int ID)
        {
            try
            {
                if (db == null) return null;
                if (SQL == null) return null;

                DataTable ImageData = SQL.SELECT("SELECT [ImagenIcono] FROM [Icono] WHERE ID = " + ID);
                foreach (System.Data.DataRow row in ImageData.Rows)
                {
                    return (byte[])row["ImagenIcono"];
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void RedrawControl()
        {
            LoadingGrid.Visibility = Visibility.Visible;
            switch (_numlines)
            {
                case 0:
                    PanelGrid.Height = double.NaN;
                    DinamicW = 1;

                    Row1.Height = new GridLength(30, GridUnitType.Star);
                    Row2.Height = new GridLength(0, GridUnitType.Star);
                    Row3.Height = new GridLength(0, GridUnitType.Star);

                    Col1.Width = new GridLength(30, GridUnitType.Star);
                    Col2.Width = new GridLength(0, GridUnitType.Star);
                    Col3.Width = new GridLength(0, GridUnitType.Star);

                    break;
                //case 1:
                //    //if (this.Width > this.Height * 5)
                //    //{
                //    //    PanelGrid.Height = double.NaN;
                //    //    DinamicW = 300f / 20f; //PanelGrid.Height = 20;
                //    //}
                //    //else
                //    //{
                //    PanelGrid.Width = double.NaN;
                //    DinamicH = 20f / 300f; //PanelGrid.Height = 20;
                //    //}
                //    Row1.Height = new GridLength(30, GridUnitType.Star);
                //    Row2.Height = new GridLength(0, GridUnitType.Star);
                //    Row3.Height = new GridLength(0, GridUnitType.Star);

                //    Col1.Width = new GridLength(60, GridUnitType.Star);
                //    Col2.Width = new GridLength(180, GridUnitType.Star);
                //    Col3.Width = new GridLength(60, GridUnitType.Star);

                //    switch (_numicons)
                //    {
                //        case 0:
                //            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180; 
                //            Col1.Width = new GridLength(0, GridUnitType.Star);
                //            Col2.Width = new GridLength(100, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 1:
                //            DinamicH = DinamicH * (300f / 200f);//PanelGrid.Width = 200;
                //            Col1.Width = new GridLength(20, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 2:
                //            DinamicH = DinamicH * (300f / 220f);//PanelGrid.Width = 220;
                //            Col1.Width = new GridLength(20, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(20, GridUnitType.Star);
                //            break;
                //        default:
                //            break;
                //    }
                //    break;
                //case 2:
                //    //if (this.Width > this.Height * 5)
                //    //{
                //    //    PanelGrid.Height = double.NaN;
                //    //    DinamicW = 300f / 40f; //PanelGrid.Height = 20;
                //    //}
                //    //else
                //    //{
                //    PanelGrid.Width = double.NaN;
                //    DinamicH = 40f / 300f; //PanelGrid.Height = 40;
                //    //}
                //    Row1.Height = new GridLength(30, GridUnitType.Star);
                //    Row2.Height = new GridLength(30, GridUnitType.Star);
                //    Row3.Height = new GridLength(0, GridUnitType.Star);

                //    Col1.Width = new GridLength(60, GridUnitType.Star);
                //    Col2.Width = new GridLength(180, GridUnitType.Star);
                //    Col3.Width = new GridLength(60, GridUnitType.Star);

                //    switch (_numicons)
                //    {
                //        case 0:
                //            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180;
                //            Col1.Width = new GridLength(0, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 1:
                //            DinamicH = DinamicH * (300f / 220f);//PanelGrid.Width = 220;
                //            Col1.Width = new GridLength(40, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 2:
                //            DinamicH = DinamicH * (300f / 260f);//PanelGrid.Width = 260;
                //            Col1.Width = new GridLength(40, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(40, GridUnitType.Star);
                //            break;
                //        default:
                //            break;
                //    }
                //    break;
                default:
                    //if (this.Width > this.Height * 5)
                    //{
                    //    PanelGrid.Height = double.NaN;
                    //    DinamicW = 300f / 60f; //PanelGrid.Height = 20;
                    //}
                    //else
                    //{
                    PanelGrid.Width = double.NaN;
                    DinamicH = 60f / 300f; //PanelGrid.Height = 60;
                    //}
                    //Row1.Height = new GridLength(30, GridUnitType.Star);
                    //Row2.Height = new GridLength(30, GridUnitType.Star);
                    //Row3.Height = new GridLength(30, GridUnitType.Star);
                    switch (NumLines)
                    {
                        case 1:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(0, GridUnitType.Star);
                            Row3.Height = new GridLength(0, GridUnitType.Star);
                            break;
                        case 2:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(30, GridUnitType.Star);
                            Row3.Height = new GridLength(0, GridUnitType.Star);
                            break;
                        case 3:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(30, GridUnitType.Star);
                            Row3.Height = new GridLength(30, GridUnitType.Star);
                            break;
                    }  
                        


                    Col1.Width = new GridLength(60, GridUnitType.Star);
                    Col2.Width = new GridLength(180, GridUnitType.Star);
                    Col3.Width = new GridLength(60, GridUnitType.Star);

                    switch (_numicons)
                    {
                        case 0:
                            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180;
                            Col1.Width = new GridLength(0, GridUnitType.Star);
                            Col2.Width = new GridLength(150, GridUnitType.Star);
                            Col3.Width = new GridLength(0, GridUnitType.Star);
                            break;
                        case 1:
                            DinamicH = DinamicH * (300f / 240f);//PanelGrid.Width = 240;
                            Col1.Width = new GridLength(60, GridUnitType.Star);
                            Col2.Width = new GridLength(180, GridUnitType.Star);
                            Col3.Width = new GridLength(0, GridUnitType.Star);
                            break;
                        case 2:
                            DinamicH = DinamicH * (300f / 300f);//PanelGrid.Width = 300;
                            Col1.Width = new GridLength(60, GridUnitType.Star);
                            Col2.Width = new GridLength(180, GridUnitType.Star);
                            Col3.Width = new GridLength(60, GridUnitType.Star);
                            break;
                        default:
                            break;
                    }
                    break;
              
            }
            tmrResize.Start();
        }

        private void Icon1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                int auxIcon = Icono1;
                EditIcon(ref Icon1, ref auxIcon);
                Icono1 = auxIcon;
            }
        }

        private void Icon2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                int auxIcon = Icono2;
                EditIcon(ref Icon2, ref auxIcon);
                Icono2 = auxIcon;
            }
        }

        private bool EditIcon(ref Button Icon, ref int Icono)
        {
            try
            {
                PanelsControls.ImageSelector IS = new PanelsControls.ImageSelector();
                IS.OfflineMode = _OfflineMode;
                IS.db = this.db;
                IS.SelectedImageID = Icono;
                if (IS.ShowDialog() == true) { Icono = IS.SelectedImageID; LoadIcon(Icono, Icon); }
                IS = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void PanelLinea1_OnTextChanged(string Text)
        {
            Linea1 = Text;
        }

        private void PanelLinea2_OnTextChanged(string Text)
        {
            Linea2 = Text;
        }

        private void PanelLinea3_OnTextChanged(string Text)
        {
            Linea3 = Text;
        }

        private void PanelLinea16_OnTextChanged(string Text)
        {
            Linea1 = Text;
        }

        private void PanelLinea26_OnTextChanged(string Text)
        {
            Linea2 = Text;
        }

        private void PanelLinea36_OnTextChanged(string Text)
        {
            Linea3 = Text;
        }
    }


}
