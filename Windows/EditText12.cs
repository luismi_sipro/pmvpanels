﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PanelsControls
{
    public partial class EditText12 : Form
    {


        public string Text12 = "";
        public int MaxNumChar = 12;

        public EditText12()
        {
            InitializeComponent();
        }

        private void NewFolder_Load(object sender, EventArgs e)
        {
            this.textBox1.MaxLength = MaxNumChar;
            this.textBox1.Text = Text12;
            this.textBox1.Focus();
            this.textBox1.Select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Text12 = textBox1.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
