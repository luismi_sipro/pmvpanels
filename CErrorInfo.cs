﻿using System;

namespace Sipro.Utilities.Error
{
    /// <summary>
    /// Represent the method that control error events. 
    /// </summary>
    /// <param name="sender">Object causing the error.</param>
    /// <param name="e">Provide error information.</param>
    public delegate void ErrorManagerEventHandler(Object sender, CErrorInfo e);

    /// <summary>
    /// Container class for manage the errors of application.
    /// </summary>
    public class CErrorInfo
    {
        /// <summary>
        /// Get or Set number of error.
        /// </summary>
        public Int32 ErrorCode
        {
            get;
            set;
        }

        /// <summary>
        /// Get or Set specific information for error ocurred.
        /// </summary>
        public String AdditionalInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Get or Set string that describe the generic error.
        /// </summary>
        public String Description
        {
            get;
            set;
        }
        
        /// <summary>
        /// Class constructor.
        /// </summary>
        /// <param name="mError">Number of error.</param>
        /// <param name="mInfo">Specific information.</param>
        public CErrorInfo(Int32 mError, String mInfo)
        {
            this.ErrorCode = mError;
            this.AdditionalInfo = mInfo;
        }

        /// <summary>
        /// Print the error summary.
        /// </summary>
        public override string ToString()
        {
            if(String.IsNullOrEmpty(this.AdditionalInfo) == true)
                return this.Description;
            else
                return String.Format("{0}\n\n[Info]: {1}", this.Description, this.AdditionalInfo);
        }
    }

    /// <summary>
    /// Container class for manage the errors of xml files.
    /// </summary>
    public class CErrorInfoXml : CErrorInfo
    {
        private String m_file;
        private Int32 m_line;

        /// <summary>
        /// Xml file error.
        /// </summary>
        public String File
        {
            get { return m_file; }
            set { m_file = value; }
        }

        /// <summary>
        /// Number of line.
        /// </summary>
        public Int32 LineNumber
        {
            get { return m_line; }
            set { m_line = value; }
        }

        /// <summary>
        /// Constructor class.
        /// </summary>
        /// <param name="mError">Number of error.</param>
        /// <param name="mInfo">Specific information.</param>
        /// <param name="mDescr">Generic description.</param>
        /// <param name="mFile">Xml file error.</param>
        /// <param name="mLine">Line number error.</param>
        public CErrorInfoXml(Int16 mError, String mInfo, String mFile, Int32 mLine) 
            :base(mError, mInfo)
        {
            this.m_file = mFile;
            this.m_line = mLine;
        }

        /// <summary>
        /// Print the error summary.
        /// </summary>
        public override string ToString()
        {
            return base.ToString() + String.Format("\n[File]: {0}\n[Line]: {1}", this.m_file, this.m_line);
        }
    }
}
