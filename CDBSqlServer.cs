﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Sipro.Database
{
    public class CDBSqlServer : CDataBase
    {
        #region EVENTS
        #endregion

        #region MEMBERS
        private String m_user;
        private String m_pass;
        private String m_server;
        private String m_database;

        private SqlConnection m_connection;
        #endregion

        #region PROPERTIES

        /// <summary>
        /// Get the user name.
        /// </summary>
        public override String User
        {
            get { return m_user; }
        }

        /// <summary>
        /// Get the password of user autentication.
        /// </summary>
        public override String Password
        {
            get { return m_pass; }
        }

        /// <summary>
        /// Get the server of database.
        /// </summary>
        public override String Server
        {
            get { return m_server; }
        }

        /// <summary>
        /// Get database name.
        /// </summary>
        public override String DataBase
        {
            get { return m_database; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override ConnectionState State
        {
            get
            {
                return m_connection.State;
            }
        }
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mConnectionStr"></param>
        public CDBSqlServer(String mConnectionStr)
            :base(mConnectionStr)
        {
            m_connection = new SqlConnection(mConnectionStr);

            //Parse connection parameters.
            fParseConnectionString();

        }
        #endregion


        #region METHODS

        public override Boolean Connect()
        {
            try
            {
                if(m_connection.State != ConnectionState.Open)
                    m_connection.Open();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        /// <summary>
        /// Specific version for SQL Server database.
        /// </summary>
        /// <param name="mQuery">Complete query to ask to database.</param>
        /// <returns>Result of query.</returns>
        public override CDataBaseResult ExecuteQuery(String mQuery)
        {
            try
            {
                //Create a command.
                SqlCommand _command = new SqlCommand(mQuery, m_connection);
                _command.CommandType = CommandType.Text;

                //Execute command.
                SqlDataReader _result = _command.ExecuteReader();

                //Get result and store.
                CDataBaseResult _dbResult = new CDataBaseResult(_result.HasRows);

                while (_result.Read())
                {
                    //Create and add row.
                    CDataBaseRow _row = new CDataBaseRow();
                    _dbResult.AddRow(_row);

                    for (UInt16 i = 0; i < _result.FieldCount; i++)
                    {
                        CDataBaseCell _cell = new CDataBaseCell(i, _result.GetName(i), _result.GetValue(i));
                        _row.Add(_cell);
                    }
                }

                //Close reader result.
                _result.Close();
                return _dbResult;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Specific method to close sql server connection.
        /// </summary>
        public override void Close()
        {
            m_connection.Close();
        }
        #endregion

        #region FUNCTIONS

        private void fParseConnectionString()
        {
            //Server={0};Database={1};User Id={2};Password={3}"
            String _connectionString = m_connection.ConnectionString;

            //Parsing.
            String[] _split = _connectionString.Split(';');

            foreach(String _piece in _split)
            {
                String[] _pair = _piece.Split('=');

                
                switch (_pair[0])
                {
                    case "Server":
                        m_server = _pair[1];
                        break;
                    case "Database":
                        m_database = _pair[1];
                        break;
                    case "User Id":
                        m_user = _pair[1];
                        break;
                    case "Password":
                        m_pass = _pair[1];
                        break;
                }
            }

        }
        #endregion

        #region EVENTS HANDLERS
        #endregion







    }
}
