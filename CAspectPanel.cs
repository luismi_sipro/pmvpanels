﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelsControls
{
    public class CAspectPanel
    {
        #region PROPERTIES

        public String ID
        {
            get;
            set;
        }

        public String Panel
        {
            get;
            set;
        }

        public String Line1
        {
            get;
            set;
        }

        public String Line2
        {
            get;
            set;
        }

        public String Line3
        {
            get;
            set;
        }

        public Int32 Icon1
        {
            get;
            set;
        }

        public Int32 Icon2
        {
            get;
            set;
        }

        public DateTime Timestamp
        {
            get;
            set;
        }

        public Int32 NumAspect
        {
            get;
            set;
        }

        public Int32 Tipology
        {
            get;
            set;
        }

        #endregion
    }
}
