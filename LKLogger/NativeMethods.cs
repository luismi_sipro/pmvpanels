﻿using System;
using System.Runtime.InteropServices;

namespace LKLog
{
    internal static class NativeMethods
    {
        [DllImport("kernel32")]
        internal static extern IntPtr LoadLibraryW([MarshalAs(UnmanagedType.LPWStr)] string lpMdoule);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "REGISTERLOGGERCLIENT", ExactSpelling = true)]
        internal static extern int RegisterLoggerClient(ref int hIdentity);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "UNREGISTERLOGGERCLIENT", ExactSpelling = true)]
        internal static extern int UnregisterLoggerClient(int hIdentity);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "SETIDENTITYNAME", ExactSpelling = true)]
        internal static extern int SetIdentityName(int hIdentity, string strIdentity);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGERROR", ExactSpelling = true)]
        internal static extern void InternalLogError(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGWARNING", ExactSpelling = true)]
        internal static extern void InternalLogWarning(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGINFO", ExactSpelling = true)]
        internal static extern void InternalLogInfo(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGTRACE", ExactSpelling = true)]
        internal static extern void InternalLogTrace(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGSTARTSTOP", ExactSpelling = true)]
        internal static extern void InternalLogStartStop(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGENTRYEXIT", ExactSpelling = true)]
        internal static extern void InternalLogEntryExit(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGTHREADSTARTSTOP", ExactSpelling = true)]
        internal static extern void InternalLogThreadStartStop(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGSQL", ExactSpelling = true)]
        internal static extern void InternalLogSQL(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGCONNECTION", ExactSpelling = true)]
        internal static extern void InternalLogConnection(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGCTORDTOR", ExactSpelling = true)]
        internal static extern void InternalLogCtorDtor(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGREFCOUNT", ExactSpelling = true)]
        internal static extern void InternalLogRefCount(int hIdentity, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "REGISTERLOGFLAG", ExactSpelling = true)]
        internal static extern int RegisterLogFlag(int hIdentity, int nCustomFlag, [MarshalAs(UnmanagedType.LPWStr)] string strFlag);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "REGISTERLOGFLAGEX", ExactSpelling = true)]
        internal static extern int RegisterLogFlagEx(int hIdentity, int nCustomFlag, [MarshalAs(UnmanagedType.LPWStr)] string strFlag, int nDefaultVal);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "LOGCUSTOM2", ExactSpelling = true)]
        internal static extern void InternalLogCustom(int hIdentity, int nCustomFlag, [MarshalAs(UnmanagedType.LPWStr)] string errorMessage);

        [DllImport("LoggerDLL.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode, EntryPoint = "GETLOGGERSTATS", ExactSpelling = true)]
        internal static extern int GetLoggerStats([MarshalAs(UnmanagedType.LPWStr)] string hostName, ref int errorCount, ref long ftLastError, ref int warningCount, ref long ftLastWarning);
    }
}
