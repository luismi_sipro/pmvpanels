﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//My namespaces
using System.ComponentModel;
using Sipro.Database;
using Sipro.Utilities.Error;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelControlInfov2.xaml
    /// </summary>
    public partial class PanelControlInfov2 : UserControl
    {
        #region MEMBERS

        //SQL Server manager.
        private CDBSqlServer m_sqlConnection;

        CTypologyDescr m_typeDescr;
        #endregion


        #region PROPERTIES

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Server name")]
        public String Server
        {
            get { return (string)GetValue(ServerProperty); }
            set
            {
                if (value == (string)GetValue(ServerProperty))
                    return;
                SetValue(ServerProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User name")]
        public String User
        {
            get { return (string)GetValue(UserProperty); }
            set
            {
                if (value == (string)GetValue(UserProperty))
                    return;
                SetValue(UserProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User password")]
        public String Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                if (value == (string)GetValue(PasswordProperty))
                    return;
                SetValue(PasswordProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(false)]
        public String ConnectionStr
        {
            get { return String.Format("Server={0};Database={1};User Id={2};Password={3}", Server, "Panels", User, Password); }
        }


        [Category("Panel Properties")]
        [Browsable(true)]
        public String Panel
        {
            get { return (string)GetValue(PanelProperty); }
            set
            {
                if (value == (string)GetValue(PanelProperty))
                    return;
                SetValue(PanelProperty, value);
                //fTypologyConfigPanels(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String Texto
        {
            get { return (string)GetValue(TextoProperty); }
            set
            {
                if (value == (string)GetValue(TextoProperty))
                    return;
                SetValue(TextoProperty, value);
                //fProcessText(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public Int32 Icono1
        {
            get { return (Int32)GetValue(Icono1Property); }
            set
            {
                if (value == (Int32)GetValue(Icono1Property))
                    return;
                SetValue(Icono1Property, value);
                this.Aspecto.Icono1 = value;
                this.Aspecto.UpdateData();
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public Int32 Icono2
        {
            get { return (Int32)GetValue(Icono2Property); }
            set
            {
                if (value == (Int32)GetValue(Icono2Property))
                    return;
                SetValue(Icono2Property, value);
                //this.Aspecto.Icono2 = value;
                //this.Aspecto.UpdateData();
            }
        }
        #endregion


        #region DEPENDENCY PROPERTIES

        // SQL connection information.
        public static readonly DependencyProperty ServerProperty = DependencyProperty.Register("Server", typeof(String), typeof(PanelControlInfov2));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register("User", typeof(String), typeof(PanelControlInfov2));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(PanelControlInfov2));

        //Specific I/O public control properties.
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(PanelControlInfov2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PanelControlInfov2 _infoPanels = d as PanelControlInfov2;
            if (_infoPanels == null)
                return;

            _infoPanels.fTypologyConfigPanels(e.NewValue.ToString());
        }

        public static readonly DependencyProperty Icono1Property = DependencyProperty.Register("Icono1", typeof(Int32), typeof(PanelControlInfov2), new PropertyMetadata(0, new PropertyChangedCallback(OnIcon1Changed)));
        private static void OnIcon1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PanelControlInfov2 _infoPanels = d as PanelControlInfov2;
            if (_infoPanels == null)
                return;

            _infoPanels.Aspecto.Icono1 = Convert.ToInt32(e.NewValue);
            _infoPanels.Aspecto.UpdateData();
        }
        public static readonly DependencyProperty Icono2Property = DependencyProperty.Register("Icono2", typeof(Int32), typeof(PanelControlInfov2), new PropertyMetadata(0, new PropertyChangedCallback(OnIcon2Changed)));
        private static void OnIcon2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PanelControlInfov2 _infoPanels = d as PanelControlInfov2;
            if (_infoPanels == null)
                return;

            _infoPanels.Aspecto.Icono2 = Convert.ToInt32(e.NewValue);
            _infoPanels.Aspecto.UpdateData();
        }
        public static readonly DependencyProperty TextoProperty = DependencyProperty.Register("Texto", typeof(String), typeof(PanelControlInfov2), new PropertyMetadata("", new PropertyChangedCallback(OnTextChanged)));
        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PanelControlInfov2 _infoPanels = d as PanelControlInfov2;
            if (_infoPanels == null)
                return;

            //Object control.
            String _text = "";
            if (e != null && e.NewValue != null)
                _text = e.NewValue.ToString();

            _infoPanels.fProcessText(_text);
        }

        #endregion


        #region CONSTRUCTORS

        public PanelControlInfov2()
        {
            InitializeComponent();
        }
        #endregion


        #region FUNCTIONS

        //Config graphical panel interface according to the typology.
        private CErrorInfo fTypologyConfigPanels(String mPanelName)
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //Execute query.
            String _query = String.Format("SELECT DISTINCT * FROM Tipologia WHERE ID = (SELECT Tipologia FROM Instancia WHERE Panel='{0}')", mPanelName);
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return new CErrorInfo(CErrorCode.ERR_PNL_TYPE, CLangCode.GetErrorText(CErrorCode.ERR_PNL_TYPE));

            //Get results.
            m_typeDescr = new CTypologyDescr();
            m_typeDescr.ID = Convert.ToInt32(_dbResult.Rows[0].GetValue("ID"));
            m_typeDescr.Name = _dbResult.Rows[0].GetValue("Nombre").ToString();
            m_typeDescr.Description = _dbResult.Rows[0].GetValue("Descripcion").ToString();
            m_typeDescr.NumLines = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumLineas"));
            m_typeDescr.NumIcons = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumIconos"));
            m_typeDescr.NumChars = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumChar"));

            //Set panel typology description.
            this.Aspecto.TypeDescr = m_typeDescr;

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        private void fProcessText(String mFullText)
        {
            //Check if exist typology.
            if (m_typeDescr == null)
                return;

            Int32 _numLines = m_typeDescr.NumLines;
            Int32 _numChars = m_typeDescr.NumChars;
            Int32 _lenText = mFullText.Length;

            //Adjust max length expected.
            if (_lenText > _numChars * _numLines)
                _lenText = _numChars * _numLines;

            //Create complete fulltext. Prevent crash app if chars are less than expected.
            StringBuilder _validFullText = new StringBuilder(_numChars * _numLines);
            _validFullText.Append(' ', _numChars * _numLines);
            for (Int32 i = 0; i < _lenText; i++)
                _validFullText[i] = mFullText[i];

            //Prepare default texts.
            if (_numLines >= 1)
                this.Aspecto.Linea1 = _validFullText.ToString().Substring(0, _numChars);

            if (_numLines >= 2)
                this.Aspecto.Linea2 = _validFullText.ToString().Substring(_numChars, _numChars);

            if (_numLines >= 3)
                this.Aspecto.Linea3 = _validFullText.ToString().Substring(_numChars * 2, _numChars);

        }

        #endregion


        #region EVENT HANDLER

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection = new CDBSqlServer(ConnectionStr);
            Boolean _r = m_sqlConnection.Connect();
            if (_r == false)
            {
                //OnShowError(new CErrorInfo(CErrorCode.ERR_DB_CONNECT, "No se puede conectar con la base de datos."));
                return;
            }

            this.Aspecto.DBConnection = m_sqlConnection;
            fTypologyConfigPanels(this.Panel);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection.Close();
        }
        #endregion
    }
}
