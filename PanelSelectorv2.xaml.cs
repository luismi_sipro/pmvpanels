﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

//my namespaces
using Sipro.Database;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelSelectorv2.xaml
    /// </summary>
    public partial class PanelSelectorv2 : UserControl
    {
        #region MEMBERS

        //SQL Server manager.
        private CDBSqlServer m_sqlConnection;

        private List<CPanelObj> m_panelObjList;
        #endregion


        #region PROPERTIES
        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Server name")]
        public String Server
        {
            get { return (string)GetValue(ServerProperty); }
            set
            {
                if (value == (string)GetValue(ServerProperty))
                    return;
                SetValue(ServerProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Database name")]
        public String DataBase
        {
            get { return (string)GetValue(DataBaseProperty); }
            set
            {
                if (value == (string)GetValue(DataBaseProperty))
                    return;
                SetValue(DataBaseProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User name")]
        public String User
        {
            get { return (string)GetValue(UserProperty); }
            set
            {
                if (value == (string)GetValue(UserProperty))
                    return;
                SetValue(UserProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User password")]
        public String Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                if (value == (string)GetValue(PasswordProperty))
                    return;
                SetValue(PasswordProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(false)]
        public String ConnectionStr
        {
            get { return String.Format("Server={0};Database={1};User Id={2};Password={3}", Server, DataBase, User, Password); }
        }

        [Category("Configuration")]
        [Browsable(true)]
        public String AppLang
        {
            get { return (string)GetValue(AppLangProperty); }
            set
            {
                if (value == (string)GetValue(AppLangProperty))
                    return;
                SetValue(AppLangProperty, value);
            }
        }


        [Category("Panel Properties")]
        [Browsable(true)]
        public String Panel
        {
            get { return (string)GetValue(PanelProperty); }
            set
            {
                if (value == (string)GetValue(PanelProperty))
                    return;
                SetValue(PanelProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String Tunel
        {
            get { return (string)GetValue(TunelProperty); }
            set
            {
                if (value == (string)GetValue(TunelProperty))
                    return;
                SetValue(TunelProperty, value);
            }
        }

        #endregion


        #region DEPENDENCY PROPERTIES

        public static readonly DependencyProperty ServerProperty = DependencyProperty.Register("Server", typeof(String), typeof(PanelSelectorv2));
        public static readonly DependencyProperty DataBaseProperty = DependencyProperty.Register("DataBase", typeof(String), typeof(PanelSelectorv2));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register("User", typeof(String), typeof(PanelSelectorv2));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(PanelSelectorv2));

        //Configuration
        public static readonly DependencyProperty AppLangProperty = DependencyProperty.Register("AppLang", typeof(String), typeof(PanelSelectorv2));

        //Specific I/O public control properties.
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(PanelSelectorv2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PanelSelectorv2 _selPanels = d as PanelSelectorv2;
            if (_selPanels == null || e.NewValue == null)
                return;

            //Get panel name or null.
            CPanelObj _panel = _selPanels.m_panelObjList.FirstOrDefault(x => x.Panel == e.NewValue.ToString());
            if (_panel == null)
                return;

            _selPanels.cboPanel.SelectedItem = _panel;
        }

        public static readonly DependencyProperty TunelProperty = DependencyProperty.Register("Tunel", typeof(String), typeof(PanelSelectorv2));
        #endregion


        #region CONSTRUCTORS

        public PanelSelectorv2()
        {
            InitializeComponent();
            m_panelObjList = new List<CPanelObj>();
        }

        #endregion


        #region FUNCTIONS

        //Translate applications texts.
        private void fTranslateLabels(String mLang)
        {
            //Load texts
            CLangCode.GetLanguageTexts(m_sqlConnection, mLang);

            this.lbPanel.Content = CLangCode.GetText(CLangCode.PS2_NAME_LABEL_PANEL);
            this.lbTunel.Content = CLangCode.GetText(CLangCode.PS2_NAME_LABEL_TUNNEL);
        }

        //Get information from sql server.
        private Int32 fGetPanelInformation()
        {
            String _query = String.Format("SELECT * FROM Instancia");
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            m_panelObjList.Clear();

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return -1;

            //if Yes, retreive it.
            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CPanelObj _pObj = new CPanelObj();
                //_pObj.Id = _row.GetValue("id").ToString();
                _pObj.Tunnel = _row.GetValue("Tunel").ToString();
                _pObj.Panel = _row.GetValue("Panel").ToString();
                //_pObj.IPAddress = _row.GetValue("ip").ToString();
                _pObj.Type = _row.GetValue("Tipologia").ToString();

                m_panelObjList.Add(_pObj);
            }

            return 0;
        }

        #endregion


        #region EVENT HANDLERS

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Create sql server connection.
            m_sqlConnection = new CDBSqlServer(ConnectionStr);
            Boolean _r = m_sqlConnection.Connect();
            if (_r == false)
                return;

            //Fill tunnel and panel combos list.
            if (fGetPanelInformation() < 0)
                return;

            //Set list of tunnels and select first tunnel.(default).
            this.cboTunel.ItemsSource = m_panelObjList.Select(x => x.Tunnel).Distinct();
            this.cboTunel.SelectedIndex = 0;

            //Select panel.
            CPanelObj _objPanel = this.m_panelObjList.FirstOrDefault(x => x.Panel == this.Panel);
            if (_objPanel != null)
            {
                this.cboPanel.SelectedItem = _objPanel;
            }

            //Translations
            fTranslateLabels(AppLang);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection.Close();
        }

        private void cboTunel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Check and select the tunnel.
            ComboBox _cbo = e.Source as ComboBox;
            if (_cbo == null)
                return;
            String _tunnelSelected = _cbo.SelectedValue.ToString();

            //Set list of panels and select default.
            this.Tunel = _tunnelSelected;
            this.cboPanel.DisplayMemberPath = "Panel";
            this.cboPanel.ItemsSource = this.m_panelObjList.Where(x => x.Tunnel == _tunnelSelected);
            //this.cboPanel.SelectedIndex = 0;
            
        }

        private void cboPanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox _cbo = e.Source as ComboBox;
            if (_cbo == null)
                return;

            //Set panel property.
            CPanelObj _panelSelected = _cbo.SelectedValue as CPanelObj;
            if (_panelSelected == null)
                return;

            this.Panel = _panelSelected.Panel;
        }
        #endregion
    }
}
