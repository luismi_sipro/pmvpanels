﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PanelsControls
{
    public partial class NewFolder : Form
    {

        public Guid ID = Guid.Empty;
        public Guid ParentID = Guid.Empty;
        public string FolderName = "";
        public SQLServer db;

        public NewFolder()
        {
            InitializeComponent();
        }

        private void NewFolder_Load(object sender, EventArgs e)
        {
            this.textBox1.Focus();
            this.textBox1.Select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (AddNewFolder(this.textBox1.Text))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("No se ha podido añadir la nueva carpeta.");
            }
        }

        private bool AddNewFolder(string Name)
        {
            try
            {
                ID = Guid.NewGuid();
                FolderName = Name;
                using (SQLOperations SQL = new SQLOperations(db))
                {
                    return SQL.INSERT_Categoria(ID, FolderName, ParentID);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
