﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelText12.xaml
    /// </summary>
    public partial class PanelText12 : UserControl
    {
        public PanelText12()
        {
            InitializeComponent();
        }

        public delegate void TextChangedHandler(string Text);

        [Browsable(true)]
        [Category("Custom Events")]
        public event TextChangedHandler OnTextChanged;

        public static readonly DependencyProperty Text12Property = DependencyProperty.Register("Text12", typeof(string), typeof(PanelControl), new UIPropertyMetadata("", new PropertyChangedCallback(OnText12Changed)));

        private static void OnText12Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (e.NewValue == null) return;
                string str1 = e.NewValue.ToString();
                if (((PanelText12)d).Text12 != str1)
                    ((PanelText12)d).Text12 = str1;
            }
            catch (Exception)
            {
            }
        }

        internal protected string _Text12 = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Text to visualize (12 chars)")]
        public string Text12
        {
            get
            {
                if (_Text12 != (string)base.GetValue(Text12Property)) base.SetValue(Text12Property, _Text12);
                return _Text12;
            }
            set
            {
                if (null != value)
                {
                    _Text12 = VerifyEncoding(value);
                    base.SetValue(Text12Property, value);
                    SetText12();
                }
            }
        }

        private string VerifyEncoding(string text)
        {
            try
            {
                if (text.Length < 12) text = text.PadRight(12);
                string aux = "";
                for (int i = 0; i < 12; i++)
                {
                    int code = Encoding.GetEncoding(437).GetBytes(text)[i];
                    byte[] b = new byte[1];
                    b[0] = Convert.ToByte(code);
                    if (32 > code || code > 175)
                        aux = aux + " ";
                    else
                        aux = aux + Encoding.GetEncoding(437).GetChars(b)[0];
                }
                return aux;
            }
            catch (Exception)
            {
                return "";
            }
        }

        enum TextAlign
        {
            Left,
            Center,
            Right
        }

        TextAlign CurrentAlign = TextAlign.Left;

        private void Grid_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            switch (CurrentAlign)
            {
                case TextAlign.Left:
                    CurrentAlign = TextAlign.Center;
                    break;
                case TextAlign.Center:
                    CurrentAlign = TextAlign.Right;
                    break;
                case TextAlign.Right:
                    CurrentAlign = TextAlign.Left;
                    break;
            }
            AlignText();
        }

        private void AlignText()
        {
            string aux = Text12;
            switch (CurrentAlign)
            {
                case TextAlign.Left:
                    aux = aux.Trim().PadRight(12);
                    break;
                case TextAlign.Center:
                    aux = CenterString(aux.Trim(), 12);
                    break;
                case TextAlign.Right:
                    aux = aux.Trim().PadLeft(12);
                    break;
            }
            Text12 = aux;
        }

        private string CenterString(string stringToCenter, int totalLength)
        {
            return stringToCenter.PadLeft(((totalLength - stringToCenter.Length) / 2)
                                + stringToCenter.Length)
                       .PadRight(totalLength);
        }


        private bool SetText12()
        {
            try
            {
                textBlock1.Text = "";
                textBlock2.Text = "";
                textBlock3.Text = "";
                textBlock4.Text = "";
                textBlock5.Text = "";
                textBlock6.Text = "";
                textBlock7.Text = "";
                textBlock8.Text = "";
                textBlock9.Text = "";
                textBlocka.Text = "";
                textBlockb.Text = "";
                textBlockc.Text = "";

                textBlock1.Text = Text12[0].ToString();
                textBlock2.Text = Text12[1].ToString();
                textBlock3.Text = Text12[2].ToString();
                textBlock4.Text = Text12[3].ToString();
                textBlock5.Text = Text12[4].ToString();
                textBlock6.Text = Text12[5].ToString();
                textBlock7.Text = Text12[6].ToString();
                textBlock8.Text = Text12[7].ToString();
                textBlock9.Text = Text12[8].ToString();
                textBlocka.Text = Text12[9].ToString();
                textBlockb.Text = Text12[10].ToString();
                textBlockc.Text = Text12[11].ToString();

                if (null != OnTextChanged) OnTextChanged(Text12);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                EditText12();
            }
        }

        private bool EditText12()
        {
            try
            {
                EditText12 ET12 = new EditText12();
                ET12.MaxNumChar = 12;
                ET12.Text12 = this.Text12.TrimEnd();
                if (ET12.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;
                this.Text12 = ET12.Text12;
                ET12.Dispose();
                ET12 = null;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
