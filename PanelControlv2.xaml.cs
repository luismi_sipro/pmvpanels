﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

//My namespaces
using Sipro.Database;

namespace PanelsControls
{
    

    /// <summary>
    /// Interaction logic for PanelControlv2.xaml
    /// </summary>
    public partial class PanelControlv2 : UserControl
    {
        #region MEMBERS
        //private System.Resources.ResourceManager rm = Properties.Resources.ResourceManager;
        #endregion


        #region PROPERTIES

        /// <summary>
        /// Gets or sets current database connection.
        /// </summary>
        public CDBSqlServer DBConnection
        {
            get;
            set;
        }

        private CTypologyDescr m_TypeDescr = null;
        /// <summary>
        /// Gets or sets the typology panel description.
        /// </summary>
        public CTypologyDescr TypeDescr
        {
            get { return m_TypeDescr; }
            set
            {
                if (value == m_TypeDescr)
                    return;
                m_TypeDescr = value;
                fSetVisibilityByType();
            }
        }

        public Int32 Icono1
        {
            get;
            set;
        }

        public Int32 Icono2
        {
            get;
            set;
        }

        public String Linea1
        {
            get { return this.PanelLinea16a.Text16; }
            set
            {
                if (value == this.PanelLinea16a.Text16)
                    return;
                this.PanelLinea16a.Text16 = value;
            }
        }

        public String Linea2
        {
            get { return this.PanelLinea16b.Text16; }
            set
            {
                if (value == this.PanelLinea16b.Text16)
                    return;
                this.PanelLinea16b.Text16 = value;
            }
        }

        public String Linea3
        {
            get { return this.PanelLinea16c.Text16; }
            set
            {
                if (value == this.PanelLinea16c.Text16)
                    return;
                this.PanelLinea16c.Text16 = value;
            }
        }

        public String AppLang
        {
            get;
            set;
        }

        #endregion


        #region CONSTRUCTORS

        public PanelControlv2()
        {
            InitializeComponent();
        }
        #endregion


        #region METHODS

        public void UpdateData()
        {
            //Load icons from db.
            Boolean _rIcon1 = LoadIcon(this.Icono1, this.btnIcon1);
            Boolean _rIcon2 = LoadIcon(this.Icono2, this.btnIcon2);

            //Set text into controls.
            //this.PanelLinea16a.Text16 = this.Linea1;
            //this.PanelLinea16b.Text16 = this.Linea2;
            //this.PanelLinea16c.Text16 = this.Linea3;
        }

        public void ClearData()
        {
            this.Icono1 = 0;
            this.Icono2 = 0;
            this.Linea1 = "";
            this.Linea2 = "";
            this.Linea3 = "";

            UpdateData();
        }
        #endregion


        #region FUNCTIONS

        //Select image to show in button.
        private Boolean LoadIcon(Int32 mIconNum, Button mBtnIcon)
        {
            //Load icon from serialized image at database.
            Byte[] _imgStream = GetIconData(mIconNum);
            if (_imgStream == null)
            {
                //Set black rectangle like a 'black image'
                mBtnIcon.Content = new Rectangle
                {
                    Height = 96,
                    Width = 96,                    
                    Fill = new SolidColorBrush(Colors.Black)
                };
                return false;
            }

            mBtnIcon.Content = new Image
            {
                Source = GlobalStatic.GetImageFromStream(_imgStream),
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch
            };

            return true;
        }

        private Byte[] GetIconData(Int32 ID)
        {
            //Check database connection.
            if (DBConnection == null)
                return null;
            if (DBConnection.State != System.Data.ConnectionState.Open)
                return null;

            //Execute query.
            String _query = String.Format("SELECT [ImagenIcono] FROM [Icono] WHERE ID = {0}", ID);
            CDataBaseResult _r = DBConnection.ExecuteQuery(_query);
            if (_r.HasRows == false || _r.Rows.Length > 1)
                return null;

            //Get stream.
            return (Byte[])_r.Rows[0].GetValue("ImagenIcono");
        }

        private void fSetVisibilityByType()
        {
            //Get type description.
            Int32 _numIcons = this.m_TypeDescr.NumIcons;
            Int32 _numChars = this.m_TypeDescr.NumChars;
            Int32 _numLines = this.m_TypeDescr.NumLines;

            //Set number of chars.
            this.PanelLinea16a.NumChars = _numChars;
            this.PanelLinea16b.NumChars = _numChars;
            this.PanelLinea16c.NumChars = _numChars;

            //Column adjust defaults
            this.btnIcon1.Visibility = Visibility.Hidden;
            this.btnIcon2.Visibility = Visibility.Hidden;
            this.PanelLinea16a.SetValue(Grid.ColumnProperty, 2);
            this.PanelLinea16b.SetValue(Grid.ColumnProperty, 2);
            this.PanelLinea16c.SetValue(Grid.ColumnProperty, 2);
            if (_numIcons == 0)
            {
                this.PanelLinea16a.SetValue(Grid.ColumnProperty, 0);
                this.PanelLinea16b.SetValue(Grid.ColumnProperty, 0);
                this.PanelLinea16c.SetValue(Grid.ColumnProperty, 0);
                this.PanelLinea16a.SetValue(Grid.ColumnSpanProperty, 5);
                this.PanelLinea16b.SetValue(Grid.ColumnSpanProperty, 5);
                this.PanelLinea16c.SetValue(Grid.ColumnSpanProperty, 5);
            }

            if (_numIcons == 2)
            {
                this.btnIcon1.Visibility = Visibility.Visible;
                this.btnIcon2.Visibility = Visibility.Visible;
                this.PanelLinea16a.SetValue(Grid.ColumnSpanProperty, 1);
                this.PanelLinea16b.SetValue(Grid.ColumnSpanProperty, 1);
                this.PanelLinea16c.SetValue(Grid.ColumnSpanProperty, 1);

            }

            if (_numIcons == 1)
            {
                this.btnIcon1.Visibility = Visibility.Visible;
                this.PanelLinea16a.SetValue(Grid.ColumnSpanProperty, 3);
                this.PanelLinea16b.SetValue(Grid.ColumnSpanProperty, 3);
                this.PanelLinea16c.SetValue(Grid.ColumnSpanProperty, 3);
            }

            //Row adjust.
            this.PanelLinea16a.Visibility = Visibility.Hidden;
            this.PanelLinea16b.Visibility = Visibility.Hidden;
            this.PanelLinea16c.Visibility = Visibility.Hidden;
            if (_numLines == 3)
            {
                this.PanelLinea16a.SetValue(Grid.RowProperty, 0);
                this.PanelLinea16b.SetValue(Grid.RowProperty, 2);
                this.PanelLinea16c.SetValue(Grid.RowProperty, 4);
                this.PanelLinea16a.SetValue(Grid.RowSpanProperty, 2);
                this.PanelLinea16b.SetValue(Grid.RowSpanProperty, 2);
                this.PanelLinea16c.SetValue(Grid.RowSpanProperty, 2);
                this.PanelLinea16a.Visibility = Visibility.Visible;
                this.PanelLinea16b.Visibility = Visibility.Visible;
                this.PanelLinea16c.Visibility = Visibility.Visible;
            }

            if (_numLines == 2)
            {
                this.PanelLinea16a.SetValue(Grid.RowProperty, 0);
                this.PanelLinea16b.SetValue(Grid.RowProperty, 3);
                this.PanelLinea16a.SetValue(Grid.RowSpanProperty, 3);
                this.PanelLinea16b.SetValue(Grid.RowSpanProperty, 3);
                this.PanelLinea16a.Visibility = Visibility.Visible;
                this.PanelLinea16b.Visibility = Visibility.Visible;
            }

            if (_numLines == 1)
            {
                this.PanelLinea16a.SetValue(Grid.RowProperty, 0);
                this.PanelLinea16a.SetValue(Grid.RowSpanProperty, 6);
                this.PanelLinea16a.Visibility = Visibility.Visible;
            }
        }
        #endregion


        #region EVENT HANDLERS
        
        //Open 
        private void Buttons_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Only pass with LEFT button click
            if (e.ChangedButton == MouseButton.Right)
                return;

            //Get button control
            Button _btn = sender as Button;
            if (_btn == null)
                return;

            //Open 'Image selector' control.
            ImageSelectorv2 _dlgImgSel = new ImageSelectorv2();
            _dlgImgSel.DBConnection = this.DBConnection;
            _dlgImgSel.AppLang = this.AppLang;
            if (_dlgImgSel.ShowDialog() == false)
                return;

            //Update button image icon.
            LoadIcon(_dlgImgSel.IconSelected, _btn);

            //Update icon parameter.
            if (_btn.Name == "btnIcon1")
                this.Icono1 = _dlgImgSel.IconSelected;

            if (_btn.Name == "btnIcon2")
                this.Icono2 = _dlgImgSel.IconSelected;
        }
        #endregion
    }

}
