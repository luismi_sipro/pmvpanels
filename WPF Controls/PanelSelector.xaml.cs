﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Windows.Threading;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelSelector.xaml
    /// </summary>
    public partial class SelectorPanel : SQLUserControl, INotifyPropertyChanged
    {
        internal class SelectableObject<T>
        {
            //public bool IsSelected { get; set; }
            public T ObjectData { get; set; }

            public SelectableObject(T objectData)
            {
                ObjectData = objectData;
            }

            //public SelectableObject(T objectData, bool isSelected)
            //{
            //    IsSelected = isSelected;
            //    ObjectData = objectData;
            //}
        }

        public SelectorPanel()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        DataTable DTPaneles;
        DataTable DTTuneles;
        ObservableCollection<SelectableObject<string>> ListaPaneles = new ObservableCollection<SelectableObject<string>>();
        ObservableCollection<SelectableObject<string>> ListaTuneles = new ObservableCollection<SelectableObject<string>>();
        System.Collections.SortedList PanelTipologia = new System.Collections.SortedList();
        SQLOperations SQL = null;
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(string), typeof(SelectorPanel), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        public static readonly DependencyProperty TunelProperty = DependencyProperty.Register("Tunel", typeof(string), typeof(SelectorPanel), new UIPropertyMetadata("", new PropertyChangedCallback(OnTunelChanged)));
        public static readonly DependencyProperty TipologiaProperty = DependencyProperty.Register("Tipologia", typeof(int), typeof(SelectorPanel), new UIPropertyMetadata(1, new PropertyChangedCallback(OnTipologiaChanged)));

        private static void OnTipologiaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            int i = Convert.ToInt32(str1);
            if (((SelectorPanel)d).Tipologia != i)
                ((SelectorPanel)d).Tipologia = i;
            //((SelectorPanel)d).RaisePropertyChanged("GISize_IsReady");
        }


        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((SelectorPanel)d).Panel != str1)
                ((SelectorPanel)d).Panel = str1;
            //((SelectorPanel)d).RaisePropertyChanged("GISize_IsReady");
        }

        private static void OnTunelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((SelectorPanel)d).Tunel != str1)
                ((SelectorPanel)d).Tunel = str1;
            //((SelectorPanel)d).RaisePropertyChanged("GISize_IsReady");
        }

        private static void OnMiPropiedadChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        internal protected string _Panel = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Instance Panel")]
        public string Panel
        {
            get
            {
                if (_Panel != (string)base.GetValue(PanelProperty))
                    base.SetValue(PanelProperty, _Panel);
                return _Panel;
            }
            set
            {
                if (null != value && value != _Panel)
                {
                    _Panel = value;
                    base.SetValue(PanelProperty, value);

                    if (SelectingPanel == false)
                    {
                        ApplyPanelInCombo(Panel);
                    }
                    //if (!isInit) PreInitialize(); else InitControl();
                }
            }
        }

        internal protected int _Tipologia = 1;
        [Category("Custom Properties 2")]
        [Browsable(true)]
        [Description("Typology of Current Instance Panel")]
        public int Tipologia
        {
            get
            {
                if (_Tipologia != (int)base.GetValue(TipologiaProperty)) base.SetValue(TipologiaProperty, _Tipologia);
                return _Tipologia;
            }
            set
            {
                if (value != _Tipologia)
                {
                    _Tipologia = value;
                    base.SetValue(TipologiaProperty, value);
                }
            }
        }

        internal protected string _Tunel = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Panel")]
        public string Tunel
        {
            get
            {
                if (_Tunel != (string)base.GetValue(TunelProperty)) base.SetValue(TunelProperty, _Tunel);
                return _Tunel;
            }
            set
            {
                if (null != value && value != _Tunel)
                {
                    _Tunel = value;
                    base.SetValue(TunelProperty, value);


                    if (SelectingTunel == false)
                    {
                        ApplyTunelInCombo(_Tunel);
                    }

                }
            }
        }

        System.Windows.Threading.DispatcherTimer tmrUpdateCombo = new System.Windows.Threading.DispatcherTimer();

        protected override void InitControl()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;           
            SQL = new SQLOperations(db);
            CheckDBFunction();
            SQL.Create_Tunel_Column();
            LoadCombobox();

            tmrUpdateCombo.Interval = new TimeSpan(0, 0, 0, 0, 100);
            tmrUpdateCombo.Tick += new EventHandler(tmrUpdateCombo_Tick);
            tmrUpdateCombo.Start();

            tmrCheckDB.Interval = new TimeSpan(0, 0, 0, 10, 0);
            tmrCheckDB.Tick += new EventHandler(tmrCheckDB_Tick);
            tmrCheckDB.Start();
           
        }

        private bool LoadCombobox()
        {
            SelectingTunel = true;
            SelectingPanel = true;

            try
            {

                ListaTuneles.Clear();
                ListaPaneles.Clear();
                PanelTipologia.Clear();
                ListaTuneles.Add(new SelectableObject<string>("<Todos>"));

                if (!OfflineMode)
                {

                    DTTuneles = SQL.SELECT("SELECT DISTINCT [Tunel] FROM Instancia");
                    DTPaneles = SQL.SELECT("SELECT [Panel],[Tipologia] FROM Instancia");

                    foreach (DataRow Tunel in DTTuneles.Select("Tunel = Tunel", "Tunel")) ListaTuneles.Add(new SelectableObject<string>((string)Tunel["Tunel"]));

                    foreach (DataRow Panel in DTPaneles.Select("Panel = Panel", "Panel"))
                    {
                        ListaPaneles.Add(new SelectableObject<string>((string)Panel["Panel"]));
                        PanelTipologia.Add((string)Panel["Panel"], Convert.ToInt32(Panel["Tipologia"]));
                    }

                    cbTuneles.ItemsSource = ListaTuneles;
                    cbPaneles.ItemsSource = ListaPaneles;

                    cbTuneles.IsEnabled = true;
                    cbPaneles.IsEnabled = true;
                }
                else
                {
                    cbTuneles.IsEnabled = false;
                    cbPaneles.IsEnabled = false;
                }

                cbTuneles.SelectedIndex = 0;
                SelectingTunel = false;
                SelectingPanel = false;
                return true;
            }
            catch (Exception)
            {
                SelectingTunel = false;
                SelectingPanel = false;
                return false;
            }

        }

        private bool LoadComboPaneles(string Tunel)
        {
            try
            {
                ListaPaneles.Clear();
                if (!OfflineMode)
                {
                    string query = "SELECT [Panel] FROM Instancia";
                    if (Tunel != "<Todos>") query = query + " WHERE [Tunel] = '" + Tunel + "'";
                    DTPaneles = SQL.SELECT(query);
                    foreach (DataRow Panel in DTPaneles.Select("Panel = Panel", "Panel")) ListaPaneles.Add(new SelectableObject<string>((string)Panel["Panel"]));
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool ApplyPanelInCombo(string ExternalPanel)
        {
            try
            {
                if (SelectedPanel != ExternalPanel)
                {
                    LoadComboPaneles("<Todos>");
                    cbTuneles.Text = "<Todos>";
                    cbTextTunel.Text = "<Todos>";
                }

                foreach (SelectableObject<string> ComboItem in cbPaneles.Items)
                {
                    if (ComboItem.ObjectData == ExternalPanel)
                    {
                        cbPaneles.SelectedItem = ComboItem;
                        cbPaneles.Text = ComboItem.ObjectData;
                        cbText.Text = ComboItem.ObjectData;
                        break;
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool ApplyTunelInCombo(string Tunel)
        {
            try
            {
                cbTuneles.SelectedItem = Tunel;
                cbTuneles.Text = Tunel;
                LoadComboPaneles(Tunel);
                cbTextTunel.Text = Tunel;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        void tmrUpdateCombo_Tick(object sender, EventArgs e)
        {
            tmrUpdateCombo.Stop();

            if (cbPaneles.Text != Panel)
            {
                ApplyPanelInCombo(Panel);
                if (OfflineMode) cbPaneles.Text = Panel;
            }

            tmrUpdateCombo.Start();
        }


        string SelectedTunel = "";
        string SelectedPanel = "";

        bool SelectingTunel = false;
        bool SelectingPanel = false;

        private void Tunel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectingTunel = true;
            try
            {
                SelectedTunel = ((SelectableObject<string>)cbTuneles.SelectedItem).ObjectData;
                LoadComboPaneles(SelectedTunel);
                cbTextTunel.Text = SelectedTunel;
            }
            catch (Exception)
            {

            }
            SelectingTunel = false;
        }

        private void Panel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectingTunel) return;
            SelectingPanel = true;
            try
            {
                SelectedPanel = ((SelectableObject<string>)cbPaneles.SelectedItem).ObjectData;
                Panel = SelectedPanel;
                if (PanelTipologia.ContainsKey(Panel)) Tipologia = (int)PanelTipologia[Panel]; else Tipologia = 1;
            }
            catch (Exception)
            {

            }
            SelectingPanel = false;
        }

        System.Windows.Threading.DispatcherTimer tmrCheckDB = new System.Windows.Threading.DispatcherTimer();
        bool CheckingDB = false;

        void tmrCheckDB_Tick(object sender, EventArgs e)
        {
            tmrCheckDB.Stop();

            CheckDBFunction();

            tmrCheckDB.Start();
        }

        private void CheckDBFunction()
        {
            if (!CheckingDB)
            {
                System.Threading.Thread t = new System.Threading.Thread(CheckDBConection);
                t.Start();
            }
        }

        private void CheckDBConection()
        {
            CheckingDB = true;
            try
            {
                OfflineMode = !SQL.CheckConnection();
            }
            catch (Exception)
            {
            }
            CheckingDB = false;
        }

        public bool _OfflineMode = false;

        public bool OfflineMode
        {
            get
            {
                return _OfflineMode;
            }
            set
            {
                if (_OfflineMode != value)
                {
                    _OfflineMode = value;
                    if (_OfflineMode == false)
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Hidden; });
                    else
                        this.Dispatcher.Invoke((Action)delegate { this.ImageGrid.Visibility = Visibility.Visible; });
                }
            }
        }

        private void SQLUserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void SQLUserControl_Unloaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
