﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelTextv2.xaml
    /// </summary>
    public partial class PanelTextv2 : UserControl
    {
        #region MEMBERS

        public const Int32 MAX_TEXT_SIZE = 16;
        #endregion


        #region PROPERTIES

        private Int32 m_NumChars = 0;
        /// <summary>
        /// Get or set real number of chars.
        /// </summary>
        public Int32 NumChars
        {
            get { return m_NumChars; }
            set
            {
                if (m_NumChars == value)
                    return;
                m_NumChars = value;
                fCreateMaxSize(m_NumChars);
            }
        }     


        private String m_Text = "";
        /// <summary>
        /// Get or set text to show.
        /// </summary>
        public String Text16
        {
            get { return m_Text; }
            set
            {
                if (m_Text == value)
                    return;
                m_Text = value;
                fFillText(m_Text);
            }
        }

        /// <summary>
        /// Get and set edit mode.
        /// </summary>
        public Boolean EditMode
        {
            get;
            set;
        }
        #endregion


        #region CONSTRUCTORS
        
        public PanelTextv2()
        {
            InitializeComponent();
        }
        #endregion


        #region METHODS
        #endregion


        #region FUNCTIONS

        private void fFillText(String mText)
        {
            for(Int32 i = 0; i<mText.Length; i++)
            {
                TextBlock _txt = this.FindName(String.Format("t{0:00}", i)) as TextBlock;
                _txt.Text = mText[i].ToString();
            }
        }


        private void fCreateMaxSize(Int32 mNumChars)
        {
            for(Int32 i = 0; i<MAX_TEXT_SIZE; i++)
            {
                //Set auto column
                this.gridText.ColumnDefinitions[i].Width = new GridLength(1, GridUnitType.Star);

                //Pseudo hide column.
                if (i >= mNumChars)
                    this.gridText.ColumnDefinitions[i].Width = new GridLength(0, GridUnitType.Star);
            }
        }


        #endregion


        #region EVENT HANDLERS

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Only pass with LEFT button click
            if (e.ChangedButton == MouseButton.Right)
                return;

            //Open text editor.
            DlgInputText _dlg = new DlgInputText();
            _dlg.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _dlg.MaxCharacters = this.NumChars;
            _dlg.Text = this.Text16;
            if (_dlg.ShowDialog() == false)
                return;

            //Set new text.
            this.Text16 = _dlg.Text;
        }
        #endregion
    }
}
