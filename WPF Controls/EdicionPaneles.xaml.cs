﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for EdicionPaneles.xaml
    /// </summary>
    public partial class EdicionPaneles : SQLUserControl
    {
        public EdicionPaneles()
        {
            InitializeComponent();
            tmrCheckDB.Interval = new TimeSpan(0, 0, 0, 10, 0);
            tmrCheckDB.Tick += new EventHandler(tmrCheckDB_Tick);
            tmrCheckDB.Start();
            InitOffline();
        }

        const string DefaultText12 = "123456789ABC";
        const string DefaultText16 = "123456789ABCDEF0";
        int DefaultIcon = 146;
        char sepchar = '€';

        SQLOperations SQL = null;

        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        public static readonly DependencyProperty IDTemplateProperty = DependencyProperty.Register("IDTemplate", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnIDTemplateChanged)));
        public static readonly DependencyProperty EditingProperty = DependencyProperty.Register("Editing", typeof(bool), typeof(EdicionPaneles), new UIPropertyMetadata(false, new PropertyChangedCallback(OnEditingChanged)));

        public static readonly DependencyProperty NumAspectosProperty = DependencyProperty.Register("NumeroAspectos", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnNumAspectosChanged)));
        public static readonly DependencyProperty NumeroConsignaProperty = DependencyProperty.Register("NumeroConsigna", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(-1, new PropertyChangedCallback(OnNumConsignaChanged)));

        public static readonly DependencyProperty A1TextoProperty = DependencyProperty.Register("Aspecto1Texto", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnA1TextoChanged)));
        public static readonly DependencyProperty A2TextoProperty = DependencyProperty.Register("Aspecto2Texto", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnA2TextoChanged)));
        public static readonly DependencyProperty A3TextoProperty = DependencyProperty.Register("Aspecto3Texto", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnA3TextoChanged)));
        public static readonly DependencyProperty A4TextoProperty = DependencyProperty.Register("Aspecto4Texto", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnA4TextoChanged)));

        public static readonly DependencyProperty A1Icono1Property = DependencyProperty.Register("Aspecto1Icono1", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA1Icono1Changed)));
        public static readonly DependencyProperty A1Icono2Property = DependencyProperty.Register("Aspecto1Icono2", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA1Icono2Changed)));
        public static readonly DependencyProperty A2Icono1Property = DependencyProperty.Register("Aspecto2Icono1", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA2Icono1Changed)));
        public static readonly DependencyProperty A2Icono2Property = DependencyProperty.Register("Aspecto2Icono2", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA2Icono2Changed)));
        public static readonly DependencyProperty A3Icono1Property = DependencyProperty.Register("Aspecto3Icono1", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA3Icono1Changed)));
        public static readonly DependencyProperty A3Icono2Property = DependencyProperty.Register("Aspecto3Icono2", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA3Icono2Changed)));
        public static readonly DependencyProperty A4Icono1Property = DependencyProperty.Register("Aspecto4Icono1", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA4Icono1Changed)));
        public static readonly DependencyProperty A4Icono2Property = DependencyProperty.Register("Aspecto4Icono2", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(0, new PropertyChangedCallback(OnA4Icono2Changed)));

        public static readonly DependencyProperty PanelDataProperty = DependencyProperty.Register("PanelData", typeof(string), typeof(EdicionPaneles), new UIPropertyMetadata("", new PropertyChangedCallback(OnPanelDataChanged)));
        public static readonly DependencyProperty AplicarPlantillaProperty = DependencyProperty.Register("AplicarPlantilla", typeof(bool), typeof(EdicionPaneles), new UIPropertyMetadata(false, new PropertyChangedCallback(OnAplicarPlantillaChanged)));

        public static readonly DependencyProperty TipologiaProperty = DependencyProperty.Register("Tipologia", typeof(int), typeof(EdicionPaneles), new UIPropertyMetadata(1, new PropertyChangedCallback(OnTipologiaChanged)));
        public static readonly DependencyProperty OfflineModeProperty = DependencyProperty.Register("OfflineMode", typeof(bool), typeof(EdicionPaneles), new UIPropertyMetadata(false, new PropertyChangedCallback(OnOfflineModeChanged)));


        //System.Windows.Threading.DispatcherTimer tmrUpdate = new System.Windows.Threading.DispatcherTimer();

        internal protected int _NumeroAspectos = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Numero de Aspectos")]
        public int NumeroAspectos
        {
            get
            {
                if (_NumeroAspectos != (int)base.GetValue(NumAspectosProperty)) base.SetValue(NumAspectosProperty, _NumeroAspectos);
                return _NumeroAspectos;
            }
            set
            {
                if (value != _NumeroAspectos)
                {
                    _NumeroAspectos = value;
                    base.SetValue(NumAspectosProperty, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _NumeroConsigna = -1;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Numero de Consigna")]
        public int NumeroConsigna
        {
            get
            {
                if (_NumeroConsigna != (int)base.GetValue(NumeroConsignaProperty)) base.SetValue(NumeroConsignaProperty, _NumeroConsigna);
                return _NumeroConsigna;
            }
            set
            {
                if (value != _NumeroConsigna)
                {
                    _NumeroConsigna = value;
                    base.SetValue(NumeroConsignaProperty, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto1Icono1 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 1 del Aspecto 1")]
        public int Aspecto1Icono1
        {
            get
            {
                if (_Aspecto1Icono1 != (int)base.GetValue(A1Icono1Property)) base.SetValue(A1Icono1Property, _Aspecto1Icono1);
                return _Aspecto1Icono1;
            }
            set
            {
                if (value != _Aspecto1Icono1)
                {
                    _Aspecto1Icono1 = value;
                    base.SetValue(A1Icono1Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto1Icono2 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 2 del Aspecto 1")]
        public int Aspecto1Icono2
        {
            get
            {
                if (_Aspecto1Icono2 != (int)base.GetValue(A1Icono2Property)) base.SetValue(A1Icono2Property, _Aspecto1Icono2);
                return _Aspecto1Icono2;
            }
            set
            {
                if (value != _Aspecto1Icono2)
                {
                    _Aspecto1Icono2 = value;
                    base.SetValue(A1Icono2Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto2Icono1 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 1 del Aspecto 2")]
        public int Aspecto2Icono1
        {
            get
            {
                if (_Aspecto2Icono1 != (int)base.GetValue(A2Icono1Property)) base.SetValue(A2Icono1Property, _Aspecto2Icono1);
                return _Aspecto2Icono1;
            }
            set
            {
                if (value != _Aspecto2Icono1)
                {
                    _Aspecto2Icono1 = value;
                    base.SetValue(A2Icono1Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto2Icono2 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 2 del Aspecto 2")]
        public int Aspecto2Icono2
        {
            get
            {
                if (_Aspecto2Icono2 != (int)base.GetValue(A2Icono2Property)) base.SetValue(A2Icono2Property, _Aspecto2Icono2);
                return _Aspecto2Icono2;
            }
            set
            {
                if (value != _Aspecto2Icono2)
                {
                    _Aspecto2Icono2 = value;
                    base.SetValue(A2Icono2Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto3Icono1 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 1 del Aspecto 3")]
        public int Aspecto3Icono1
        {
            get
            {
                if (_Aspecto3Icono1 != (int)base.GetValue(A3Icono1Property)) base.SetValue(A3Icono1Property, _Aspecto3Icono1);
                return _Aspecto3Icono1;
            }
            set
            {
                if (value != _Aspecto3Icono1)
                {
                    _Aspecto3Icono1 = value;
                    base.SetValue(A3Icono1Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto3Icono2 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 2 del Aspecto 3")]
        public int Aspecto3Icono2
        {
            get
            {
                if (_Aspecto3Icono2 != (int)base.GetValue(A3Icono2Property)) base.SetValue(A3Icono2Property, _Aspecto3Icono2);
                return _Aspecto3Icono2;
            }
            set
            {
                if (value != _Aspecto3Icono2)
                {
                    _Aspecto3Icono2 = value;
                    base.SetValue(A3Icono2Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto4Icono1 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 1 del Aspecto 4")]
        public int Aspecto4Icono1
        {
            get
            {
                if (_Aspecto4Icono1 != (int)base.GetValue(A4Icono1Property)) base.SetValue(A4Icono1Property, _Aspecto4Icono1);
                return _Aspecto4Icono1;
            }
            set
            {
                if (value != _Aspecto4Icono1)
                {
                    _Aspecto4Icono1 = value;
                    base.SetValue(A4Icono1Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected int _Aspecto4Icono2 = 0;
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Icono 2 del Aspecto 4")]
        public int Aspecto4Icono2
        {
            get
            {
                if (_Aspecto4Icono2 != (int)base.GetValue(A4Icono2Property)) base.SetValue(A4Icono2Property, _Aspecto4Icono2);
                return _Aspecto4Icono2;
            }
            set
            {
                if (value != _Aspecto4Icono2)
                {
                    _Aspecto4Icono2 = value;
                    base.SetValue(A4Icono2Property, value);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected string _Aspecto1Texto = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Texto del Aspecto 1")]
        public string Aspecto1Texto
        {
            get
            {
                if (_Aspecto1Texto != (string)base.GetValue(A1TextoProperty)) base.SetValue(A1TextoProperty, _Aspecto1Texto);
                return _Aspecto1Texto;
            }
            set
            {
                if (null != value && value != _Aspecto1Texto)
                {
                    _Aspecto1Texto = value.PadRight(NumChars * 3).Substring(0, NumChars * 3);
                    base.SetValue(A1TextoProperty, _Aspecto1Texto);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected string _Aspecto2Texto = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Texto del Aspecto 2")]
        public string Aspecto2Texto
        {
            get
            {
                if (_Aspecto2Texto != (string)base.GetValue(A2TextoProperty)) base.SetValue(A2TextoProperty, _Aspecto2Texto);
                return _Aspecto2Texto;
            }
            set
            {
                if (null != value && value != _Aspecto2Texto)
                {
                    _Aspecto2Texto = value.PadRight(NumChars * 3).Substring(0, NumChars * 3);
                    base.SetValue(A2TextoProperty, _Aspecto2Texto);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected string _Aspecto3Texto = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Texto del Aspecto 3")]
        public string Aspecto3Texto
        {
            get
            {
                if (_Aspecto3Texto != (string)base.GetValue(A3TextoProperty)) base.SetValue(A3TextoProperty, _Aspecto3Texto);
                return _Aspecto3Texto;
            }
            set
            {
                if (null != value && value != _Aspecto3Texto)
                {
                    _Aspecto3Texto = value.PadRight(NumChars * 3).Substring(0, NumChars * 3);
                    base.SetValue(A3TextoProperty, _Aspecto3Texto);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected string _Aspecto4Texto = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Texto del Aspecto 4")]
        public string Aspecto4Texto
        {
            get
            {
                if (_Aspecto4Texto != (string)base.GetValue(A4TextoProperty)) base.SetValue(A4TextoProperty, _Aspecto4Texto);
                return _Aspecto4Texto;
            }
            set
            {
                if (null != value && value != _Aspecto4Texto)
                {
                    _Aspecto4Texto = value.PadRight(NumChars * 3).Substring(0, NumChars * 3);
                    base.SetValue(A4TextoProperty, _Aspecto4Texto);
                    if (isInit) UpdateData();
                }
            }
        }

        internal protected string _PanelData = "";
        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Datos del Panel Historico")]
        public string PanelData
        {
            get
            {
                if (_PanelData != (string)base.GetValue(PanelDataProperty)) base.SetValue(PanelDataProperty, _PanelData);
                return _PanelData;
            }
            set
            {
                if (null != value && value != _PanelData)
                {
                    _PanelData = value;
                    base.SetValue(PanelDataProperty, _PanelData);
                    if (isInit)
                    {
                        if (value != "")
                        {
                            SetFullData(_PanelData);
                            UpdateData();
                            Editing = true;
                            CheckAspectos();
                            bAceptar.Visibility = Visibility.Hidden;
                        }
                    }
                }
            }
        }

        int _Tipologia = 1;
        int _TipologiaOffline = 1;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Tipología")]
        public int Tipologia
        {
            get
            {
                if (_TipologiaOffline != (int)base.GetValue(TipologiaProperty)) base.SetValue(TipologiaProperty, _TipologiaOffline);
                return _TipologiaOffline;
            }
            set
            {
                if (_OfflineMode)
                {
                    if (value != _TipologiaOffline)
                    {
                        //LKLog.wwLogger.LogWarning("Panel Controls: Tipología Offline = " + _TipologiaOffline);
                        _TipologiaOffline = value;
                        base.SetValue(TipologiaProperty, value);
                        if (isInit) LoadDataOffline();
                    }
                }
            }
        }

        internal protected string _Panel = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Instance Panel")]
        public string Panel
        {
            get
            {
                if (_Panel != (string)base.GetValue(PanelProperty)) base.SetValue(PanelProperty, _Panel);
                return _Panel;
            }
            set
            {
                if (null != value && value != _Panel)
                {
                    _Panel = value;
                    base.SetValue(PanelProperty, value);
                    if (isInit) { GetTipology(value); InitControl(); };
                }
            }
        }

        internal protected string _IDTemplate = "";
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Current Panel Template selected")]
        public string IDTemplate
        {
            get
            {
                if (_IDTemplate != (string)base.GetValue(IDTemplateProperty)) base.SetValue(IDTemplateProperty, _IDTemplate);
                return _IDTemplate;
            }
            set
            {
                if (null != value && value != _IDTemplate)
                {
                    _IDTemplate = value;
                    base.SetValue(IDTemplateProperty, value);
                    AplicarPlantilla = false;
                    if (!isInit) PreInitialize(); else InitControl();
                }
            }
        }

        internal protected bool _Editing = false;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Indicates if current Panel Template is in edition")]
        public bool Editing
        {
            get
            {
                if (_Editing != (bool)base.GetValue(EditingProperty)) base.SetValue(EditingProperty, _Editing);
                return _Editing;
            }
            set
            {
                if (value != _Editing)
                {
                    _Editing = value;
                    base.SetValue(EditingProperty, value);
                    RefreshEditing();
                }
            }
        }

        internal protected bool _AplicarPlantilla = false;
        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Trigger to apply panel template.")]
        public bool AplicarPlantilla
        {
            get
            {
                if (_AplicarPlantilla != (bool)base.GetValue(AplicarPlantillaProperty)) base.SetValue(AplicarPlantillaProperty, _AplicarPlantilla);
                return _AplicarPlantilla;
            }
            set
            {
                if (value != _AplicarPlantilla)
                {
                    _AplicarPlantilla = value;
                    base.SetValue(AplicarPlantillaProperty, value);
                    if (value == true) { SaveHistorical(); }
                }
            }
        }

        private void RefreshEditing()
        {
            this.NamePlantilla.IsEnabled = _Editing;
            this.x0.IsEnabled = _Editing;
            this.x1.IsEnabled = _Editing;
            this.x2.IsEnabled = _Editing;
            this.x3.IsEnabled = _Editing;
            this.x4.IsEnabled = _Editing;
            this.Aspecto1.IsEnabled = _Editing;
            this.Aspecto2.IsEnabled = _Editing;
            this.Aspecto3.IsEnabled = _Editing;
            this.Aspecto4.IsEnabled = _Editing;

            Aspecto1.OfflineMode = _OfflineMode;
            Aspecto2.OfflineMode = _OfflineMode;
            Aspecto3.OfflineMode = _OfflineMode;
            Aspecto4.OfflineMode = _OfflineMode;

            if (!OfflineMode)
            {
                if (_Editing) this.bAceptar.Visibility = Visibility.Visible; else this.bAceptar.Visibility = Visibility.Hidden;
                if (_Editing) this.bCancelar.Visibility = Visibility.Visible; else this.bCancelar.Visibility = Visibility.Hidden;
                if (NumeroConsigna >= 0) this.NamePlantilla.IsEnabled = false;
            }
            else
            {
                ClearData();
                this.bAceptar.Visibility = Visibility.Hidden;
                this.bCancelar.Visibility = Visibility.Hidden;
                this.NamePlantilla.IsEnabled = false;
            }

        }

        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).Panel != str1)
                ((EdicionPaneles)d).Panel = str1;
        }

        private static void OnIDTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).IDTemplate != str1)
                ((EdicionPaneles)d).IDTemplate = str1;
        }

        private static void OnEditingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            bool bit = Convert.ToBoolean(e.NewValue);
            if (((EdicionPaneles)d).Editing != bit)
                ((EdicionPaneles)d).Editing = bit;
        }

        private static void OnAplicarPlantillaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            bool bit = Convert.ToBoolean(e.NewValue);
            if (((EdicionPaneles)d).AplicarPlantilla != bit)
                ((EdicionPaneles)d).AplicarPlantilla = bit;
        }


        private static void OnNumAspectosChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).NumeroAspectos != num)
                ((EdicionPaneles)d).NumeroAspectos = num;
        }

        private static void OnNumConsignaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).NumeroConsigna != num)
                ((EdicionPaneles)d).NumeroConsigna = num;
        }


        private static void OnOfflineModeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (sender.ReadLocalValue(e.Property) == DependencyProperty.UnsetValue) return;
                EdicionPaneles c = sender as EdicionPaneles;
                if (c != null) c.RaisePropertyChanged("OfflineMode");
            }
            catch (Exception ex)
            {
                LKLog.wwLogger.LogError("OnMarkerClickCallBack : " + ex.Message);
            }
            //if (e.NewValue == null) return;
            //bool bit = Convert.ToBoolean(e.NewValue);
            //if (((EdicionPaneles)d).OfflineMode != bit)
            //    ((EdicionPaneles)d).OfflineMode = bit;
        }


        private static void OnA1TextoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).Aspecto1Texto != str1)
                ((EdicionPaneles)d).Aspecto1Texto = str1;
        }

        private static void OnA2TextoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).Aspecto2Texto != str1)
                ((EdicionPaneles)d).Aspecto2Texto = str1;
        }

        private static void OnA3TextoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).Aspecto3Texto != str1)
                ((EdicionPaneles)d).Aspecto3Texto = str1;
        }

        private static void OnA4TextoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((EdicionPaneles)d).Aspecto4Texto != str1)
                ((EdicionPaneles)d).Aspecto4Texto = str1;
        }

        private static void OnPanelDataChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (!string.IsNullOrEmpty(str1))
                if (((EdicionPaneles)d).PanelData != str1)
                    ((EdicionPaneles)d).PanelData = str1;
        }

        private static void OnA1Icono1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto1Icono1 != num)
                ((EdicionPaneles)d).Aspecto1Icono1 = num;
        }

        private static void OnA1Icono2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto1Icono2 != num)
                ((EdicionPaneles)d).Aspecto1Icono2 = num;
        }

        private static void OnA2Icono1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto2Icono1 != num)
                ((EdicionPaneles)d).Aspecto2Icono1 = num;
        }

        private static void OnA2Icono2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto2Icono2 != num)
                ((EdicionPaneles)d).Aspecto2Icono2 = num;
        }

        private static void OnA3Icono1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto3Icono1 != num)
                ((EdicionPaneles)d).Aspecto3Icono1 = num;
        }

        private static void OnA3Icono2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto3Icono2 != num)
                ((EdicionPaneles)d).Aspecto3Icono2 = num;
        }

        private static void OnA4Icono1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto4Icono1 != num)
                ((EdicionPaneles)d).Aspecto4Icono1 = num;
        }

        private static void OnA4Icono2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Aspecto4Icono2 != num)
                ((EdicionPaneles)d).Aspecto4Icono2 = num;
        }

        private static void OnTipologiaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((EdicionPaneles)d).Tipologia != num)
                ((EdicionPaneles)d).Tipologia = num;
        }

        protected override void InitControl()
        {
            //tmrUpdate.Interval = new TimeSpan(0, 0, 0, 0, 300);
            //tmrUpdate.Tick += new EventHandler(tmrUpdate_Tick);

            SQL = new SQLOperations(db);
            CheckDBFunction();
            RefreshEditing();
            Aspecto1.db = this.db;
            Aspecto2.db = this.db;
            Aspecto3.db = this.db;
            Aspecto4.db = this.db;
            //TreeNodes = new ObservableCollection<CategoryTreeNode>();
            //treeView.ItemsSource = TreeNodes;
            LoadData();
            UpdateData();

        }

        bool LoadingInfo = false;
        bool SavingInfo = false;

        //int Tipologia = 0;

        private bool ClearData()
        {
            try
            {
                this.NamePlantilla.Text = "";
                this._NumeroAspectos = 0;
                this.x0.IsChecked = true;
                this.x1.IsChecked = false;
                this.x2.IsChecked = false;
                this.x3.IsChecked = false;
                this.x4.IsChecked = false;

                Aspecto1.ID = Guid.Empty;
                Aspecto2.ID = Guid.Empty;
                Aspecto3.ID = Guid.Empty;
                Aspecto4.ID = Guid.Empty;

                Aspecto1Icono1 = DefaultIcon;
                Aspecto1Icono2 = DefaultIcon;
                Aspecto2Icono1 = DefaultIcon;
                Aspecto2Icono2 = DefaultIcon;
                Aspecto3Icono1 = DefaultIcon;
                Aspecto3Icono2 = DefaultIcon;
                Aspecto4Icono1 = DefaultIcon;
                Aspecto4Icono2 = DefaultIcon;

                Aspecto1Texto = DefaultText16 + DefaultText16 + DefaultText16;
                Aspecto2Texto = DefaultText16 + DefaultText16 + DefaultText16;
                Aspecto3Texto = DefaultText16 + DefaultText16 + DefaultText16;
                Aspecto4Texto = DefaultText16 + DefaultText16 + DefaultText16;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }




        int NumChars = 12;
        private bool SetFullData(string PanelData)
        {
            //PanelData = "Tipologia€NumeroDeAspectos€A1Icono1€A1Icono2€A1Texto€A2Icono1€A12cono2€A2Texto€A3Icono1€A3Icono2€A3Texto€A4Icono1€A4Icono2€A4Texto"
            if (OfflineMode) return false;

            if (PanelData.Trim() == "") return false;
            try
            {
                LoadingInfo = true;
                ClearData();

                this._Tipologia = Convert.ToInt32(PanelData.Split(sepchar)[0]);
                this.NumeroAspectos = Convert.ToInt32(PanelData.Split(sepchar)[1]);

                DataTable DataConfig = SQL.SELECT("SELECT [ID],[NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = " + this._Tipologia + "");
                foreach (DataRow crow in DataConfig.Rows)
                {
                    int NumLines = (int)crow["NumLineas"];
                    int NumIcons = (int)crow["NumIconos"];
                    NumChars = (int)crow["NumChar"];
                    this.Aspecto1.NumLines = NumLines;
                    this.Aspecto1.NumIcons = NumIcons;
                    this.Aspecto1.NumChars = NumChars;
                    this.Aspecto2.NumLines = NumLines;
                    this.Aspecto2.NumIcons = NumIcons;
                    this.Aspecto2.NumChars = NumChars;
                    this.Aspecto3.NumLines = NumLines;
                    this.Aspecto3.NumIcons = NumIcons;
                    this.Aspecto3.NumChars = NumChars;
                    this.Aspecto4.NumLines = NumLines;
                    this.Aspecto4.NumIcons = NumIcons;
                    this.Aspecto4.NumChars = NumChars;
                }

                Aspecto1Icono1 = Convert.ToInt32(PanelData.Split(sepchar)[2]);
                Aspecto1Icono2 = Convert.ToInt32(PanelData.Split(sepchar)[3]);
                Aspecto1Texto = PanelData.Split(sepchar)[4];

                Aspecto2Icono1 = Convert.ToInt32(PanelData.Split(sepchar)[5]);
                Aspecto2Icono2 = Convert.ToInt32(PanelData.Split(sepchar)[6]);
                Aspecto2Texto = PanelData.Split(sepchar)[7];

                Aspecto3Icono1 = Convert.ToInt32(PanelData.Split(sepchar)[8]);
                Aspecto3Icono2 = Convert.ToInt32(PanelData.Split(sepchar)[9]);
                Aspecto3Texto = PanelData.Split(sepchar)[10];

                Aspecto4Icono1 = Convert.ToInt32(PanelData.Split(sepchar)[11]);
                Aspecto4Icono2 = Convert.ToInt32(PanelData.Split(sepchar)[12]);
                Aspecto4Texto = PanelData.Split(sepchar)[13];

                Aspecto1.UpdateData();
                Aspecto2.UpdateData();
                Aspecto3.UpdateData();
                Aspecto4.UpdateData();
                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }

        private int GetTipology(string Panel)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return 0;

            if (OfflineMode) return 0;

            try
            {
                DataTable DataConfig = SQL.SELECT("SELECT [ID],[NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Instancia] WHERE Panel = '" + Panel + "')");

                foreach (DataRow crow in DataConfig.Rows)
                {
                    _Tipologia = (int)crow["ID"];
                    NumChars = (int)crow["NumChar"];
                }
                return _Tipologia;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        DataTable DTipologia = new DataTable("Tipologia");
        DataTable DTipoIcono = new DataTable("TipoIcono");
        private void InitOffline()
        {
            DTipologia = new DataTable("Tipologia");
            DTipologia.Columns.Add("ID");
            DTipologia.Columns.Add("Nombre");
            DTipologia.Columns.Add("Descripción");
            DTipologia.Columns.Add("NumLineas");
            DTipologia.Columns.Add("NumIconos");
            DTipologia.Columns.Add("NumChar");
            DTipologia.Rows.Add(1, "I", "1 Icono", 0, 1, 12);
            DTipologia.Rows.Add(10, "1L", "1 Linea", 1, 0, 12);
            DTipologia.Rows.Add(11, "1L+1I", "1 Linea y 1 Icono", 1, 1, 12);
            DTipologia.Rows.Add(12, "1L+2I", "1 Linea y 2 Iconos", 1, 2, 12);
            DTipologia.Rows.Add(20, "2L", "2 Lineas", 2, 0, 12);
            DTipologia.Rows.Add(21, "2L+1I", "2 Lineas y 1 Icono", 2, 1, 12);
            DTipologia.Rows.Add(22, "2L+2I", "2 Lineas y 2 Iconos", 2, 2, 12);
            DTipologia.Rows.Add(30, "3L", "3 Lineas", 3, 0, 12);
            DTipologia.Rows.Add(31, "3L+1I", "3 Lineas y 1 Icono", 3, 1, 12);
            DTipologia.Rows.Add(32, "3L+2I", "3 Lineas y 2 Iconos", 3, 2, 12);
            DTipologia.Rows.Add(1116, "16L1I", "1 Linea de 16 caracteres y 1 Icono", 1, 1, 16);

            DTipoIcono = new DataTable("TipoIcono");
            DTipoIcono.Columns.Add("ID");
            DTipoIcono.Columns.Add("Nombre");
            DTipoIcono.Rows.Add(1, "Advertencia de peligro");
            DTipoIcono.Rows.Add(2, "Prioridad");
            DTipoIcono.Rows.Add(3, "Prioridad de entrada");
            DTipoIcono.Rows.Add(4, "Restricción de paso");
            DTipoIcono.Rows.Add(5, "Prohibición o restricción");
            DTipoIcono.Rows.Add(6, "Obligación");
            DTipoIcono.Rows.Add(7, "Fin prohibición restricción");
            DTipoIcono.Rows.Add(8, "Indicaciones generales");
            DTipoIcono.Rows.Add(9, "Carriles");
            DTipoIcono.Rows.Add(10, "Especiales");
            DTipoIcono.Rows.Add(11, "De servicio");
            DTipoIcono.Rows.Add(12, "Otras");
            DTipoIcono.Rows.Add(13, "Advertencia de peligro lejos");
        }

        private bool LoadTipologyOffline(int Tipologia)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                foreach (DataRow crow in DTipologia.Select("ID = '" + Tipologia + "'"))
                {
                    int NumLines = Convert.ToInt32(crow[3]);
                    int NumIcons = Convert.ToInt32(crow[4]);
                    NumChars = Convert.ToInt32(crow[5]);
                    this.Aspecto1.NumLines = NumLines;
                    this.Aspecto1.NumIcons = NumIcons;
                    this.Aspecto1.NumChars = NumChars;
                    this.Aspecto2.NumLines = NumLines;
                    this.Aspecto2.NumIcons = NumIcons;
                    this.Aspecto2.NumChars = NumChars;
                    this.Aspecto3.NumLines = NumLines;
                    this.Aspecto3.NumIcons = NumIcons;
                    this.Aspecto3.NumChars = NumChars;
                    this.Aspecto4.NumLines = NumLines;
                    this.Aspecto4.NumIcons = NumIcons;
                    this.Aspecto4.NumChars = NumChars;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadDataOffline()
        {
            try
            {
                LoadingInfo = true;

                ClearData();

                LoadTipologyOffline(_TipologiaOffline);

                this.NamePlantilla.Text = "-offline-";
                this.NumeroAspectos = 4;
                this.NumeroConsigna = -1;
                this.NamePlantilla.IsEnabled = false;
                x4.IsChecked = true;
                x4_Checked(null, null);

                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }

        private bool LoadData()
        {
            try
            {
                if (OfflineMode) return LoadDataOffline();

                LoadingInfo = true;

                ClearData();

                DataTable DataConfig = SQL.SELECT("SELECT [ID],[NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Plantilla] WHERE ID = '" + this.IDTemplate + "')");
                foreach (DataRow crow in DataConfig.Rows)
                {
                    _Tipologia = (int)crow["ID"];
                    int NumLines = (int)crow["NumLineas"];
                    int NumIcons = (int)crow["NumIconos"];
                    NumChars = (int)crow["NumChar"];
                    this.Aspecto1.NumLines = NumLines;
                    this.Aspecto1.NumIcons = NumIcons;
                    this.Aspecto1.NumChars = NumChars;
                    this.Aspecto2.NumLines = NumLines;
                    this.Aspecto2.NumIcons = NumIcons;
                    this.Aspecto2.NumChars = NumChars;
                    this.Aspecto3.NumLines = NumLines;
                    this.Aspecto3.NumIcons = NumIcons;
                    this.Aspecto3.NumChars = NumChars;
                    this.Aspecto4.NumLines = NumLines;
                    this.Aspecto4.NumIcons = NumIcons;
                    this.Aspecto4.NumChars = NumChars;
                }

                DataTable Plantilla = SQL.SELECT_Plantilla_byID(Guid.Parse(this.IDTemplate));
                foreach (DataRow prow in Plantilla.Rows)
                {
                    this.NamePlantilla.Text = prow["Nombre"].ToString();
                    this.NumeroAspectos = Convert.ToInt32(prow["NumeroAspectos"]);
                    try
                    {
                        this.NumeroConsigna = Convert.ToInt32(prow["NumeroPorDefecto"]);
                    }
                    catch (Exception)
                    {
                        NumeroConsigna = -1;
                    }

                    if (NumeroConsigna >= 0) this.NamePlantilla.IsEnabled = false;
                }

                switch (NumeroAspectos)
                {
                    case 0:
                        x0.IsChecked = true; x0_Checked(null, null);
                        break;
                    case 1:
                        x1.IsChecked = true; x1_Checked(null, null);
                        break;
                    case 2:
                        x2.IsChecked = true; x2_Checked(null, null);
                        break;
                    case 3:
                        x3.IsChecked = true; x3_Checked(null, null);
                        break;
                    case 4:
                        x4.IsChecked = true; x4_Checked(null, null);
                        break;
                }

                DataTable Aspectos = SQL.SELECT_Aspecto_byPlantilla(Guid.Parse(this.IDTemplate));
                foreach (DataRow arow in Aspectos.Rows)
                {
                    int NumAspecto = Convert.ToInt32(arow["NumeroAspecto"]);

                    switch (NumAspecto)
                    {
                        case 1:
                            Aspecto1.ID = Guid.Parse(arow["ID"].ToString());
                            if (arow["Icono1"] == DBNull.Value) Aspecto1Icono1 = 0; else Aspecto1Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto1Icono2 = 0; else Aspecto1Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto1Texto = Convert.ToString(arow["Linea1"]).PadRight(NumChars) + Convert.ToString(arow["Linea2"]).PadRight(NumChars) + Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            break;
                        case 2:
                            Aspecto2.ID = Guid.Parse(arow["ID"].ToString());
                            if (arow["Icono1"] == DBNull.Value) Aspecto2Icono1 = 0; else Aspecto2Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto2Icono2 = 0; else Aspecto2Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto2Texto = Convert.ToString(arow["Linea1"]).PadRight(NumChars) + Convert.ToString(arow["Linea2"]).PadRight(NumChars) + Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            break;
                        case 3:
                            Aspecto3.ID = Guid.Parse(arow["ID"].ToString());
                            if (arow["Icono1"] == DBNull.Value) Aspecto3Icono1 = 0; else Aspecto3Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto3Icono2 = 0; else Aspecto3Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto3Texto = Convert.ToString(arow["Linea1"]).PadRight(NumChars) + Convert.ToString(arow["Linea2"]).PadRight(NumChars) + Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            break;
                        case 4:
                            Aspecto4.ID = Guid.Parse(arow["ID"].ToString());
                            if (arow["Icono1"] == DBNull.Value) Aspecto4Icono1 = 0; else Aspecto4Icono1 = Convert.ToInt32(arow["Icono1"]);
                            if (arow["Icono2"] == DBNull.Value) Aspecto4Icono2 = 0; else Aspecto4Icono2 = Convert.ToInt32(arow["Icono2"]);
                            Aspecto4Texto = Convert.ToString(arow["Linea1"]).PadRight(NumChars) + Convert.ToString(arow["Linea2"]).PadRight(NumChars) + Convert.ToString(arow["Linea3"]).PadRight(NumChars);
                            break;
                        default:
                            break;
                    }
                }

                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }

        private bool UpdateData()
        {
            if (SavingInfo) return false;
            try
            {
                LoadingInfo = true;

                switch (NumeroAspectos)
                {
                    case 0:
                        Aspecto1.Visibility = Visibility.Hidden;
                        Aspecto2.Visibility = Visibility.Hidden;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Hidden;
                        n2.Visibility = Visibility.Hidden;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 1:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Hidden;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Hidden;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Hidden;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Hidden;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 3:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Visible;
                        Aspecto4.Visibility = Visibility.Hidden;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Visible;
                        n4.Visibility = Visibility.Hidden;
                        break;
                    case 4:
                        Aspecto1.Visibility = Visibility.Visible;
                        Aspecto2.Visibility = Visibility.Visible;
                        Aspecto3.Visibility = Visibility.Visible;
                        Aspecto4.Visibility = Visibility.Visible;
                        n1.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Visible;
                        n4.Visibility = Visibility.Visible;
                        break;
                }

                this.Aspecto1.Icono1 = Aspecto1Icono1;
                this.Aspecto1.Icono2 = Aspecto1Icono2;
                this.Aspecto2.Icono1 = Aspecto2Icono1;
                this.Aspecto2.Icono2 = Aspecto2Icono2;
                this.Aspecto3.Icono1 = Aspecto3Icono1;
                this.Aspecto3.Icono2 = Aspecto3Icono2;
                this.Aspecto4.Icono1 = Aspecto4Icono1;
                this.Aspecto4.Icono2 = Aspecto4Icono2;

                this.Aspecto1.Linea1 = Aspecto1Texto.Substring(NumChars * 0, NumChars);
                this.Aspecto1.Linea2 = Aspecto1Texto.Substring(NumChars * 1, NumChars);
                this.Aspecto1.Linea3 = Aspecto1Texto.Substring(NumChars * 2, NumChars);
                this.Aspecto2.Linea1 = Aspecto2Texto.Substring(NumChars * 0, NumChars);
                this.Aspecto2.Linea2 = Aspecto2Texto.Substring(NumChars * 1, NumChars);
                this.Aspecto2.Linea3 = Aspecto2Texto.Substring(NumChars * 2, NumChars);
                this.Aspecto3.Linea1 = Aspecto3Texto.Substring(NumChars * 0, NumChars);
                this.Aspecto3.Linea2 = Aspecto3Texto.Substring(NumChars * 1, NumChars);
                this.Aspecto3.Linea3 = Aspecto3Texto.Substring(NumChars * 2, NumChars);
                this.Aspecto4.Linea1 = Aspecto4Texto.Substring(NumChars * 0, NumChars);
                this.Aspecto4.Linea2 = Aspecto4Texto.Substring(NumChars * 1, NumChars);
                this.Aspecto4.Linea3 = Aspecto4Texto.Substring(NumChars * 2, NumChars);

                LoadingInfo = false;
                return true;
            }
            catch (Exception)
            {
                LoadingInfo = false;
                return false;
            }
        }


        private void x0_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x0.IsChecked == true)
            {
                NumeroAspectos = 0;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
            }
        }

        private void x1_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x1.IsChecked == true)
            {
                NumeroAspectos = 1;
                x0.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
            }
        }

        private void x2_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x2.IsChecked == true)
            {
                NumeroAspectos = 2;
                x0.IsChecked = false;
                x1.IsChecked = false;
                x3.IsChecked = false;
                x4.IsChecked = false;
            }
        }

        private void x3_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x3.IsChecked == true)
            {
                NumeroAspectos = 3;
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x4.IsChecked = false;
            }
        }

        private void x4_Checked(object sender, RoutedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;
            if (!isInit) return;
            if (LoadingInfo) return;
            if (x4.IsChecked == true)
            {
                NumeroAspectos = 4;
                x0.IsChecked = false;
                x1.IsChecked = false;
                x2.IsChecked = false;
                x3.IsChecked = false;
            }
        }

        private void bAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (!isInit) return;
            if (SaveData())
            {
                Editing = false;
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No se ha podido actualizar la pantilla de panel.");
            }
        }

        private void bCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (!isInit) return;
            Editing = false;
        }

        private bool SaveData()
        {
            try
            {
                SavingInfo = true;

                Aspecto1Icono1 = this.Aspecto1.Icono1;
                Aspecto1Icono2 = this.Aspecto1.Icono2;
                Aspecto2Icono1 = this.Aspecto2.Icono1;
                Aspecto2Icono2 = this.Aspecto2.Icono2;
                Aspecto3Icono1 = this.Aspecto3.Icono1;
                Aspecto3Icono2 = this.Aspecto3.Icono2;
                Aspecto4Icono1 = this.Aspecto4.Icono1;
                Aspecto4Icono2 = this.Aspecto4.Icono2;

                Aspecto1Texto = this.Aspecto1.Linea1.PadRight(NumChars) + this.Aspecto1.Linea2.PadRight(NumChars) + this.Aspecto1.Linea3.PadRight(NumChars);
                Aspecto2Texto = this.Aspecto2.Linea1.PadRight(NumChars) + this.Aspecto2.Linea2.PadRight(NumChars) + this.Aspecto2.Linea3.PadRight(NumChars);
                Aspecto3Texto = this.Aspecto3.Linea1.PadRight(NumChars) + this.Aspecto3.Linea2.PadRight(NumChars) + this.Aspecto3.Linea3.PadRight(NumChars);
                Aspecto4Texto = this.Aspecto4.Linea1.PadRight(NumChars) + this.Aspecto4.Linea2.PadRight(NumChars) + this.Aspecto4.Linea3.PadRight(NumChars);

                if (!OfflineMode)
                {
                    //ACTUALIZAR PLANTILLA
                    SQL.UPDATE_Plantilla(Guid.Parse(this.IDTemplate), this.NamePlantilla.Text.Trim(), this.NumeroAspectos);
                    //ACTUALIZAR ASPECTOS
                    SQL.DELETE_Aspecto_byPlantilla(Guid.Parse(this.IDTemplate));
                    if (NumeroAspectos >= 1) SQL.INSERT_Aspecto(Guid.NewGuid(), Guid.Parse(this.IDTemplate), 1, Aspecto1Icono1, Aspecto1Icono2, this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3, NumChars);
                    if (NumeroAspectos >= 2) SQL.INSERT_Aspecto(Guid.NewGuid(), Guid.Parse(this.IDTemplate), 2, Aspecto2Icono1, Aspecto2Icono2, this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3, NumChars);
                    if (NumeroAspectos >= 3) SQL.INSERT_Aspecto(Guid.NewGuid(), Guid.Parse(this.IDTemplate), 3, Aspecto3Icono1, Aspecto3Icono2, this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3, NumChars);
                    if (NumeroAspectos >= 4) SQL.INSERT_Aspecto(Guid.NewGuid(), Guid.Parse(this.IDTemplate), 4, Aspecto4Icono1, Aspecto4Icono2, this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3, NumChars);
                }

                SavingInfo = false;
                return true;
            }
            catch (Exception)
            {
                SavingInfo = false;
                return false;
            }

        }

        private bool SaveHistorical()
        {
            //Store = false;
            try
            {
                SavingInfo = true;

                Aspecto1Icono1 = this.Aspecto1.Icono1;
                Aspecto1Icono2 = this.Aspecto1.Icono2;
                Aspecto2Icono1 = this.Aspecto2.Icono1;
                Aspecto2Icono2 = this.Aspecto2.Icono2;
                Aspecto3Icono1 = this.Aspecto3.Icono1;
                Aspecto3Icono2 = this.Aspecto3.Icono2;
                Aspecto4Icono1 = this.Aspecto4.Icono1;
                Aspecto4Icono2 = this.Aspecto4.Icono2;

                Aspecto1Texto = this.Aspecto1.Linea1.PadRight(NumChars) + this.Aspecto1.Linea2.PadRight(NumChars) + this.Aspecto1.Linea3.PadRight(NumChars);
                Aspecto2Texto = this.Aspecto2.Linea1.PadRight(NumChars) + this.Aspecto2.Linea2.PadRight(NumChars) + this.Aspecto2.Linea3.PadRight(NumChars);
                Aspecto3Texto = this.Aspecto3.Linea1.PadRight(NumChars) + this.Aspecto3.Linea2.PadRight(NumChars) + this.Aspecto3.Linea3.PadRight(NumChars);
                Aspecto4Texto = this.Aspecto4.Linea1.PadRight(NumChars) + this.Aspecto4.Linea2.PadRight(NumChars) + this.Aspecto4.Linea3.PadRight(NumChars);

                //ACTUALIZAR HISTORICO

                if (!OfflineMode)
                {
                    DateTime Fecha = DateTime.Now;

                    if (NumeroAspectos >= 1) SQL.INSERT_Historico(this.Panel, this._Tipologia, 1, Fecha, Aspecto1Icono1, Aspecto1Icono2, this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3, NumChars);
                    if (NumeroAspectos >= 2) SQL.INSERT_Historico(this.Panel, this._Tipologia, 2, Fecha, Aspecto2Icono1, Aspecto2Icono2, this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3, NumChars);
                    if (NumeroAspectos >= 3) SQL.INSERT_Historico(this.Panel, this._Tipologia, 3, Fecha, Aspecto3Icono1, Aspecto3Icono2, this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3, NumChars);
                    if (NumeroAspectos >= 4) SQL.INSERT_Historico(this.Panel, this._Tipologia, 4, Fecha, Aspecto4Icono1, Aspecto4Icono2, this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3, NumChars);
                }
                SavingInfo = false;
                return true;
            }
            catch (Exception)
            {
                SavingInfo = false;
                return false;
            }

        }


        private void CheckAspectos()
        {
            switch (NumeroAspectos)
            {
                case 0:
                    x0.IsChecked = true; x0_Checked(null, null);
                    break;
                case 1:
                    x1.IsChecked = true; x1_Checked(null, null);
                    break;
                case 2:
                    x2.IsChecked = true; x2_Checked(null, null);
                    break;
                case 3:
                    x3.IsChecked = true; x3_Checked(null, null);
                    break;
                case 4:
                    x4.IsChecked = true; x4_Checked(null, null);
                    break;
            }
        }


        private void bAplicar_Click(object sender, RoutedEventArgs e)
        {
            if (this.AplicarPlantilla) { AplicarPlantilla = false; Wait(1); }

            this.AplicarPlantilla = true;
        }

        public static void Wait(double seconds)
        {
            var frame = new DispatcherFrame();
            new System.Threading.Thread((System.Threading.ThreadStart)(() =>
            {
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(seconds));
                frame.Continue = false;
            })).Start();
            Dispatcher.PushFrame(frame);
        }

        System.Windows.Threading.DispatcherTimer tmrCheckDB = new System.Windows.Threading.DispatcherTimer();
        bool CheckingDB = false;

        void tmrCheckDB_Tick(object sender, EventArgs e)
        {
            tmrCheckDB.Stop();

            CheckDBFunction();

            tmrCheckDB.Start();
        }

        private void CheckDBFunction()
        {
            if (!CheckingDB)
            {
                System.Threading.Thread t = new System.Threading.Thread(CheckDBConection);
                t.Start();
            }
        }

        private void CheckDBConection()
        {
            CheckingDB = true;
            try
            {
                OfflineMode = !SQL.CheckConnection();
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            CheckingDB = false;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool _OfflineMode = false;

        [Category("Config Properties")]
        [Browsable(true)]
        [Description("Indica si esta en modo online(false) o offline(true)")]
        public bool OfflineMode
        {
            get
            {
                if (_OfflineMode != (bool)base.GetValue(OfflineModeProperty)) base.SetValue(OfflineModeProperty, _OfflineMode);
                return _OfflineMode;
            }
            set
            {
                if (_OfflineMode != value)
                {
                    _OfflineMode = value;
                    this.Dispatcher.Invoke((Action)delegate { base.SetValue(OfflineModeProperty, value); });
                    if (_OfflineMode == false)
                    {
                        LKLog.wwLogger.LogWarning("Panel Controls: Running in Offline mode.");
                        this.Dispatcher.Invoke((Action)delegate
                        {
                            this.ImageGrid.Visibility = Visibility.Hidden;
                            LoadData();
                            Editing = false;
                            //this.BottomFill.Fill = new SolidColorBrush(Colors.Black);
                        });
                    }
                    else
                    {
                        //LKLog.wwLogger.LogInfo("Panel Controls: Running in Online mode.");
                        this.Dispatcher.Invoke((Action)delegate
                        {
                            this.ImageGrid.Visibility = Visibility.Visible;
                            LoadDataOffline();
                            Editing = true;
                            //this.BottomFill.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
                        });
                    }
                }
            }
        }
    }
}
