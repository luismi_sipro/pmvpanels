﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PanelsControls
{
    public partial class EditFolder : Form
    {

        public Guid ID = Guid.Empty;
        public string FolderName = "";
        public SQLServer db;

        public EditFolder()
        {
            InitializeComponent();
        }

        private void NewFolder_Load(object sender, EventArgs e)
        {
            this.textBox1.Text = FolderName;
            this.textBox1.Focus();
            this.textBox1.Select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (EditExistingFolder(this.ID, this.textBox1.Text))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("No se ha podido editar la carpeta.");
            }
        }

        private bool EditExistingFolder(Guid ID, string Name)
        {
            try
            {                
                FolderName = Name;
                using (SQLOperations SQL = new SQLOperations(db))
                {
                    return SQL.UPDATE_Categoria(ID, FolderName);
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
