﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

//My namespaces.
using Sipro.Database;
using Sipro.Utilities.Error;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for BibliotecaPanelesv2.xaml
    /// </summary>
    public partial class BibliotecaPanelesv2 : UserControl
    {
        #region CONST

        public const String DEFAULT_CATEGORY = "00000000-0000-0000-0000-000000000001";
        public const String ROOT_CATEGORY = "00000000-0000-0000-0000-000000000000";
        public const Int32 MAXTREELEVEL = 2;

        #endregion


        #region MEMBERS

        //SQL Server manager.
        private CDBSqlServer m_sqlConnection;

        //Selected node (folder or template)
        private CTreeNodeItem m_selectedNode;
        #endregion


        #region PROPERTIES

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Server name")]
        public String Server
        {
            get { return (string)GetValue(ServerProperty); }
            set
            {
                if (value == (string)GetValue(ServerProperty))
                    return;
                SetValue(ServerProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Database name")]
        public String DataBase
        {
            get { return (string)GetValue(DataBaseProperty); }
            set
            {
                if (value == (string)GetValue(DataBaseProperty))
                    return;
                SetValue(DataBaseProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User name")]
        public String User
        {
            get { return (string)GetValue(UserProperty); }
            set
            {
                if (value == (string)GetValue(UserProperty))
                    return;
                SetValue(UserProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User password")]
        public String Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                if (value == (string)GetValue(PasswordProperty))
                    return;
                SetValue(PasswordProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(false)]
        public String ConnectionStr
        {
            get { return String.Format("Server={0};Database={1};User Id={2};Password={3}", Server, DataBase, User, Password); }
        }

        [Category("Configuration")]
        [Browsable(true)]
        public String AppLang
        {
            get { return (string)GetValue(AppLangProperty); }
            set
            {
                if (value == (string)GetValue(AppLangProperty))
                    return;
                SetValue(AppLangProperty, value);
            }
        }

        //private String m_Panel = "";
        //[Category("Panel Properties")]
        //[Browsable(true)]
        //public String Panel
        //{
        //    get { return m_Panel; }
        //    set
        //    {
        //        if (value == m_Panel)
        //            return;
        //        m_Panel = value;
        //        Refresh();
        //    }
        //}


        [Category("Panel Properties")]
        [Browsable(true)]
        public String Panel
        {
            get { return (string)GetValue(PanelProperty); }
            set
            {
                if (value == (string)GetValue(PanelProperty))
                    return;
                SetValue(PanelProperty, value);
                //Refresh();
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String IDTemplate
        {
            get { return (string)GetValue(IDTemplateProperty); }
            set
            {
                if (value == (string)GetValue(IDTemplateProperty))
                    return;
                SetValue(IDTemplateProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(false)]
        public Int32 Typology
        {
            get;
            set;
        }

        #endregion


        #region DEPENDENCY PROPERTIES

        public static readonly DependencyProperty ServerProperty = DependencyProperty.Register("Server", typeof(String), typeof(BibliotecaPanelesv2));
        public static readonly DependencyProperty DataBaseProperty = DependencyProperty.Register("DataBase", typeof(String), typeof(BibliotecaPanelesv2));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register("User", typeof(String), typeof(BibliotecaPanelesv2));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(BibliotecaPanelesv2));

        //Configuration
        public static readonly DependencyProperty AppLangProperty = DependencyProperty.Register("AppLang", typeof(String), typeof(BibliotecaPanelesv2));

        //Specific I/O public control properties.
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(BibliotecaPanelesv2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        //public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(BibliotecaPanelesv2));
        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            BibliotecaPanelesv2 _libPanels = d as BibliotecaPanelesv2;
            if (_libPanels == null)
                return;

            //Refresh panels.
            _libPanels.Refresh();
        }
        public static readonly DependencyProperty IDTemplateProperty = DependencyProperty.Register("IDTemplate", typeof(String), typeof(BibliotecaPanelesv2));
        #endregion


        #region CONSTRUCTORS

        public BibliotecaPanelesv2()
        {
            InitializeComponent();
            Typology = 0;
        }
        #endregion


        #region FUNCTIONS

        //Translate applications texts.
        private void fTranslateLabels(String mLang)
        {
            //Load texts
            CLangCode.GetLanguageTexts(m_sqlConnection, mLang);
        }

        //Refresh tree view graphic object.
        private CErrorInfo Refresh()
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            this.lbPanelSel.Text = Panel;

            //Get panel tipology.
            fGetTypologyOfPanel(Panel);

            //Execute category query
            String _query = String.Format("SELECT * FROM Categoria");
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return new CErrorInfo(CErrorCode.ERR_DB_QUERY, CLangCode.GetErrorText(CErrorCode.ERR_DB_QUERY));

            //if Yes, retreive it.
            ObservableCollection<CTreeNodeItem> m_listItems = new ObservableCollection<CTreeNodeItem>();

            CCategoryTreeNodeItem _rootNode = new CCategoryTreeNodeItem();
            _rootNode.Id = Guid.Parse(ROOT_CATEGORY);
            _rootNode.Name = CLangCode.GetText(CLangCode.BP2_NAME_LABEL_LIBRARY);
            _rootNode.ParentId = _rootNode.Id;
            _rootNode.ParentNode = null;
            _rootNode.IsExpanded = true;
            m_listItems.Add(_rootNode);

            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CCategoryTreeNodeItem _catInfo = new CCategoryTreeNodeItem();
                _catInfo.Id = (Guid)_row.GetValue("ID");
                _catInfo.Name = _row.GetValue("Nombre").ToString();
                _catInfo.ParentId = (Guid)_row.GetValue("ParentID");
                //_catInfo.IsExpanded = true;

                //Populate all categories.
                PopulateItemsView(m_listItems.ToArray(), _catInfo);
            }

            //Execute template query
            _query = String.Format("SELECT * FROM Plantilla WHERE Tipologia={0}", this.Typology);
            _dbResult = m_sqlConnection.ExecuteQuery(_query);
            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CTemplateTreeNodeItem _tpInfo = new CTemplateTreeNodeItem();
                _tpInfo.Id = (Guid)_row.GetValue("ID");
                _tpInfo.Name = _row.GetValue("Nombre").ToString();
                _tpInfo.ParentId = (Guid)_row.GetValue("Categoria");
                _tpInfo.Tipology = Convert.ToInt32(_row.GetValue("Tipologia"));
                _tpInfo.NumberOfAspects = Convert.ToInt32(_row.GetValue("NumeroAspectos"));

                //Populate all templates.
                PopulateItemsView(m_listItems.ToArray(), _tpInfo);
            }
            
            //Set source nodes.
            this.treeTemplateManager.ItemsSource = m_listItems;

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Use to populate unlimit level of categories.
        private void PopulateItemsView(CTreeNodeItem[] mListCat, CTreeNodeItem mItemInfo)
        {
            foreach (CTreeNodeItem _tCat in mListCat)
            {
                CCategoryTreeNodeItem _tCatInfo = _tCat as CCategoryTreeNodeItem;
                if (_tCatInfo == null)
                    break;

                //Category is equal to template category.
                if (_tCatInfo.Id == mItemInfo.ParentId)
                {
                    _tCatInfo.Nodes.Add(mItemInfo);
                    mItemInfo.ParentNode = _tCatInfo;
                    break;
                }

                //Category don't have more subcategories.
                if (_tCatInfo.Nodes.Count <= 0)
                    continue;

                //Create list array of subcategories.
                CTreeNodeItem[] _subItemsArr = _tCatInfo.Nodes.ToArray();
                PopulateItemsView(_subItemsArr, mItemInfo);
            }
        }

        //Show errors to user.
        private void OnShowError(CErrorInfo mInfo)
        {
            MessageBox.Show(mInfo.AdditionalInfo, "Panel Manager", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        //Insert a new category folder.
        private CErrorInfo fInsertCategory(CCategoryTreeNodeItem mSelectedItem, String mCategoryName)
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            CCategoryTreeNodeItem _catItemTree = new CCategoryTreeNodeItem();
            _catItemTree.Id = Guid.NewGuid();
            _catItemTree.Name = mCategoryName;
            _catItemTree.ParentId = mSelectedItem.ParentId;
            _catItemTree.ParentNode = mSelectedItem;
            _catItemTree.IsExpanded = true;

            //Execute category query
            String _query = String.Format("INSERT INTO Categoria VALUES('{0}','{1}','{2}')", _catItemTree.Id, _catItemTree.Name, mSelectedItem.Id);
            m_sqlConnection.ExecuteQuery(_query);

            //Add template to category.
            mSelectedItem.Nodes.Add(_catItemTree);
            mSelectedItem.IsExpanded = true;

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Insert a new template.
        private CErrorInfo fInsertTemplate(CCategoryTreeNodeItem mSelectedItem, String mTemplateName)
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //Create default template.
            CTemplateTreeNodeItem _tempItemTree = new CTemplateTreeNodeItem();
            _tempItemTree.Id = Guid.NewGuid();
            _tempItemTree.Name = mTemplateName;
            _tempItemTree.NumberOfAspects = 0;
            _tempItemTree.Tipology = this.Typology;
            _tempItemTree.ParentId = mSelectedItem.ParentId;
            _tempItemTree.ParentNode = mSelectedItem;
            _tempItemTree.IsExpanded = true;

            //Execute category query
            String _query = String.Format("INSERT INTO Plantilla VALUES('{0}','{1}',{2},'{3}',{4},0)", _tempItemTree.Id, _tempItemTree.Name, _tempItemTree.NumberOfAspects, mSelectedItem.Id, Typology);
            m_sqlConnection.ExecuteQuery(_query);

            //Add template to category.
            mSelectedItem.Nodes.Add(_tempItemTree);
            mSelectedItem.IsExpanded = true;

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Delete category. Only it can delete if it's empty.
        private CErrorInfo fDeleteCategory(CCategoryTreeNodeItem mSelectedItem)
        {
            //Check if category is empty.
            if (mSelectedItem.Nodes.Count > 0)
                return new CErrorInfo(CErrorCode.ERR_LIB_DELCAT, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DELCAT));

            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //Create query.
            String _query = String.Format("DELETE FROM Categoria WHERE ID='{0}'", mSelectedItem.Id);
            m_sqlConnection.ExecuteQuery(_query);

            //Remove from parent node.
            CCategoryTreeNodeItem _parentNode = mSelectedItem.ParentNode as CCategoryTreeNodeItem;
            if (_parentNode == null)
                return new CErrorInfo(CErrorCode.ERR_LIB_DELCAT, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DELCAT));

            Boolean _res = _parentNode.Nodes.Remove(mSelectedItem);
            if (_res == false)
                return new CErrorInfo(CErrorCode.ERR_LIB_DELCAT, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DELCAT));

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Delete template. This deletes in cascade with aspects. Make sure that 'ID' column in 'Plantilla' table it's configured in delete in cascade.
        private CErrorInfo fDeleteTemplate(CTemplateTreeNodeItem mSelectedItem)
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //Create query.
            String _query = String.Format("DELETE FROM Plantilla WHERE ID='{0}'", mSelectedItem.Id);
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);
            if(_dbResult == null)
                return new CErrorInfo(CErrorCode.ERR_LIB_DEL_TEMPLATE, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DEL_TEMPLATE));

            //Remove from parent node.
            CCategoryTreeNodeItem _parentNode = mSelectedItem.ParentNode as CCategoryTreeNodeItem;
            if (_parentNode == null)
                return new CErrorInfo(CErrorCode.ERR_LIB_DEL_TEMPLATE, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DEL_TEMPLATE));

            Boolean _res = _parentNode.Nodes.Remove(mSelectedItem);
            if (_res == false)
                return new CErrorInfo(CErrorCode.ERR_LIB_DEL_TEMPLATE, CLangCode.GetErrorText(CErrorCode.ERR_LIB_DEL_TEMPLATE));


            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        private CErrorInfo fGetTypologyOfPanel(String mNamePanel)
        {
            //Execute query.
            String _query = String.Format("SELECT Tipologia FROM Instancia WHERE Panel='{0}'", mNamePanel);
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return new CErrorInfo(CErrorCode.ERR_PNL_TYPE, CLangCode.GetErrorText(CErrorCode.ERR_PNL_TYPE));

            //Gets results.
            this.Typology = Convert.ToInt32(_dbResult.Rows[0].GetValue("Tipologia"));

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        #endregion


        #region EVENT HANDLERS

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection = new CDBSqlServer(ConnectionStr);
            Boolean _r = m_sqlConnection.Connect();
            if (_r == false)
                return;

            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
            {
                OnShowError(new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT)));
                return;
            }

            //Get typology of Panel.
            fTranslateLabels(AppLang);

            //Load first time treeview.
            Refresh();

        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection.Close();
        }

        private void btnAddFolder_Click(object sender, RoutedEventArgs e)
        {
            CCategoryTreeNodeItem _catNode = this.m_selectedNode as CCategoryTreeNodeItem;
            if (_catNode == null)
                return;

            //Show editor to enter the name.
            DlgInputText _dlgInputName = new DlgInputText();
            _dlgInputName.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _dlgInputName.MaxCharacters = 50;
            if (_dlgInputName.ShowDialog() == false)
                return;

            CErrorInfo _err = fInsertCategory(_catNode, _dlgInputName.Text);
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            CCategoryTreeNodeItem _catNode = this.m_selectedNode as CCategoryTreeNodeItem;
            if (_catNode == null)
            {
                OnShowError(new CErrorInfo(CErrorCode.ERR_LIB_ADD_TEMPLATE, CLangCode.GetErrorText(CErrorCode.ERR_LIB_ADD_TEMPLATE)));
                return;
            }

            //Show editor to enter the name.
            DlgInputText _dlgInputName = new DlgInputText();
            _dlgInputName.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _dlgInputName.MaxCharacters = 50;
            if (_dlgInputName.ShowDialog() == false)
                return;

            CErrorInfo _err = fInsertTemplate(_catNode, _dlgInputName.Text);
        }

        private void btnDelItem_Click(object sender, RoutedEventArgs e)
        {
            //Creates a new panel template.
            CTemplateTreeNodeItem _tempNode = this.m_selectedNode as CTemplateTreeNodeItem;
            if (_tempNode != null)
            {
                CErrorInfo _err = fDeleteTemplate(_tempNode);
                if (_err.ErrorCode != CErrorCode.ERR_ALL_OK)
                    OnShowError(_err);
                return;
            }

            //Create a new category of panels.
            CCategoryTreeNodeItem _catNode = this.m_selectedNode as CCategoryTreeNodeItem;
            if (_catNode != null)
            {
                CErrorInfo _err = fDeleteCategory(_catNode);
                if (_err.ErrorCode != CErrorCode.ERR_ALL_OK)
                    OnShowError(_err);
                return;
            }
        }
                
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void treeTemplateManager_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //Clear template.
            this.IDTemplate = "";

            //Save selected node.
            this.m_selectedNode = this.treeTemplateManager.SelectedItem as CTreeNodeItem;

            //Change IDTemplate.
            if (m_selectedNode is CTemplateTreeNodeItem)
                this.IDTemplate = this.m_selectedNode.Id.ToString();
        }

        #endregion
    }

    public abstract class CTreeNodeItem
    {
        public String Name
        {
            get;
            set;
        }

        public Guid Id
        {
            get;
            set;
        }

        public Boolean IsExpanded
        {
            get;
            set;
        }

        public Guid ParentId
        {
            get;
            set;
        }

        public CTreeNodeItem ParentNode
        {
            get;
            set;
        }
    }

    public class CCategoryTreeNodeItem : CTreeNodeItem
    {
        public ObservableCollection<CTreeNodeItem> Nodes
        {
            get;
            set;
        }
        
        public CCategoryTreeNodeItem()
        {
            this.Nodes = new ObservableCollection<CTreeNodeItem>();
        }
    }

    public class CTemplateTreeNodeItem : CTreeNodeItem
    {
        public Int32 Tipology
        {
            get;
            set;
        }

        public Int32 NumberOfAspects
        {
            get;
            set;
        }
    }

}
