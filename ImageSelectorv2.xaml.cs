﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//My namespaces
using Sipro.Database;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for ImageSelectorv2.xaml
    /// </summary>
    public partial class ImageSelectorv2 : Window
    {
        #region MEMBERS
        #endregion


        #region PROPERTIES

        /// <summary>
        /// Gets or sets current database connection.
        /// </summary>
        public CDBSqlServer DBConnection
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets icon selected.
        /// </summary>
        public Int32 IconSelected
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets app language.
        /// </summary>
        public String AppLang
        {
            get;
            set;
        }

        #endregion


        #region CONSTRUCTORS

        public ImageSelectorv2()
        {
            InitializeComponent();
        }


        #endregion

        #region METHODS
        #endregion

        #region FUNCTIONS
        
        //Translate applications texts.
        private void fTranslateLabels(String mLang)
        {
            //Load texts
            CLangCode.GetLanguageTexts(DBConnection, mLang);

            this.Title = CLangCode.GetText(CLangCode.IS2_NAME_LABEL_SEL);
            this.label_Copy.Content = CLangCode.GetText(CLangCode.IS2_NAME_LABEL_PREV);
            this.label.Content = CLangCode.GetText(CLangCode.IS2_NAME_LABEL_TYPESIG);
            this.bAceptar.Content = CLangCode.GetText(CLangCode.IS2_NAME_BTN_ACCEPT);
            this.bCancel.Content = CLangCode.GetText(CLangCode.IS2_NAME_BTN_CANCEL);
            this.bDelete.Content = CLangCode.GetText(CLangCode.IS2_NAME_BTN_WITHOUT);
        }

        //Get types of icons from database.
        private Boolean fLoadTypesOfIcons()
        {
            //Check database connection.
            if (DBConnection == null)
                return false;
            if (DBConnection.State != System.Data.ConnectionState.Open)
                return false;

            //Execute query.
            String _query = String.Format("SELECT * FROM TipoIcono");
            CDataBaseResult _r = DBConnection.ExecuteQuery(_query);
            if (_r.HasRows == false)
                return false;

            //List of icons.
            List<TipoIcono> _listIcons = new List<TipoIcono>();

            //Get results.
            foreach (CDataBaseRow _row in _r.Rows)
            {
                TipoIcono _tIcon = new TipoIcono();
                _tIcon.ID = Convert.ToInt32(_row.GetValue("ID"));
                _tIcon.Nombre = _row.GetValue("Nombre").ToString();

                _listIcons.Add(_tIcon);
            }

            //Set source of combo.
            this.comboBox.ItemsSource = _listIcons;
            this.comboBox.SelectedIndex = 0;

            return true;
        }

        private Boolean fLoadIconImages(Int32 mTypeIcon)
        {
            //Check database connection.
            if (DBConnection == null)
                return false;
            if (DBConnection.State != System.Data.ConnectionState.Open)
                return false;

            //Execute query.
            String _query = String.Format("SELECT * FROM Icono WHERE TipoIcono={0}", mTypeIcon);
            if (mTypeIcon == -1)
                _query = String.Format("SELECT * FROM Icono");
            CDataBaseResult _r = DBConnection.ExecuteQuery(_query);
            if (_r == null)
                return false;

            //List of icons.
            List<ImageInfo> _listImages = new List<ImageInfo>();

            //Get results.
            foreach (CDataBaseRow _row in _r.Rows)
            {
                ImageInfo _tImag = new ImageInfo();
                _tImag.ID = Convert.ToInt32(_row.GetValue("ID"));
                _tImag.TipoIcono = Convert.ToInt32(_row.GetValue("TipoIcono"));
                _tImag.Image = GlobalStatic.GetImageFromStream((byte[])_row.GetValue("ImagenIcono"));

                _listImages.Add(_tImag);
            }

            //Fill control
            this.ListOfImages.ItemsSource = _listImages;

            return true;
        }
        #endregion


        #region EVENT HANDLERS
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Get types of icons from database.
            fLoadTypesOfIcons();

            //Translate
            fTranslateLabels(this.AppLang);
        }

        private void ListOfImages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Get image information
            ImageInfo _iInfo = this.ListOfImages.SelectedItem as ImageInfo;
            if (_iInfo == null)
                return;

            //enabled button and set preview.
            this.bAceptar.IsEnabled = true;
            this.image.Source = ((ImageInfo)_iInfo).Image;
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Default value
            Int32 _tIconValue = -1;
            
            //Get types of icons to show.
            TipoIcono _tIcon = this.comboBox.SelectedValue as TipoIcono;
            if (_tIcon != null)
                _tIconValue = _tIcon.ID;

            //Search icons.
            Boolean _r = fLoadIconImages(_tIconValue);
            if (_r == false)
                MessageBox.Show("Existe algún error con la conexión de base de datos", "Cargando iconos...", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void bAceptar_Click(object sender, RoutedEventArgs e)
        {
            //Get image information
            ImageInfo _iInfo = this.ListOfImages.SelectedItem as ImageInfo;
            if (_iInfo == null)
                return;

            //Set selected icon number.
            this.IconSelected = _iInfo.ID;
            this.DialogResult = true;
        }

        private void bDelete_Click(object sender, RoutedEventArgs e)
        {
            //Set empty icon.
            this.IconSelected = 0;
            this.DialogResult = true;
            this.Close();
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion
    }

    public class ImageInfo
    {
        public BitmapImage Image { get; set; }
        public int ID { get; set; }
        public int TipoIcono { get; set; }
    }

    public class TipoIcono
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
    }
}
