﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//My namespaces.
using Sipro.Database;
using Sipro.Utilities.Error;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for EdicionPanelesv2.xaml
    /// </summary>
    public partial class EdicionPanelesv2 : UserControl
    {
        #region CONST

        public const Char CHAR_SEPARATOR = ';';
        #endregion


        #region MEMBERS

        //SQL Server manager.
        private CDBSqlServer m_sqlConnection;

        //Number of selected aspects.
        private Int32 m_numberAspects;

        //Panel typology.
        private CTypologyDescr m_typeDescr;

        #endregion


        #region PROPERTIES

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Server name")]
        public String Server
        {
            get { return (string)GetValue(ServerProperty); }
            set
            {
                if (value == (string)GetValue(ServerProperty))
                    return;
                SetValue(ServerProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Database name")]
        public String DataBase
        {
            get { return (string)GetValue(DataBaseProperty); }
            set
            {
                if (value == (string)GetValue(DataBaseProperty))
                    return;
                SetValue(DataBaseProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User name")]
        public String User
        {
            get { return (string)GetValue(UserProperty); }
            set
            {
                if (value == (string)GetValue(UserProperty))
                    return;
                SetValue(UserProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User password")]
        public String Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                if (value == (string)GetValue(PasswordProperty))
                    return;
                SetValue(PasswordProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(false)]
        public String ConnectionStr
        {
            get { return String.Format("Server={0};Database={1};User Id={2};Password={3}", Server, DataBase, User, Password); }
        }

        [Category("Configuration")]
        [Browsable(true)]
        public String AppLang
        {
            get { return (string)GetValue(AppLangProperty); }
            set
            {
                if (value == (string)GetValue(AppLangProperty))
                    return;
                SetValue(AppLangProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String Panel
        {
            get { return (string)GetValue(PanelProperty); }
            set
            {
                if (value == (string)GetValue(PanelProperty))
                    return;
                SetValue(PanelProperty, value);

                //LMRO: Configurar los paneles para la nueva tipologia.
                //fTypologyConfigPanels(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String PanelData
        {
            get { return (string)GetValue(PanelDataProperty); }
            set
            {
                if (value == (string)GetValue(PanelDataProperty))
                    return;
                SetValue(PanelDataProperty, value);

                //Show info from historical panels.
                //fCopyHistoricalPanel(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public Boolean AplicarCalle
        {
            get { return (Boolean)GetValue(AplicarCalleProperty); }
            set
            {
                if (value == (Boolean)GetValue(AplicarCalleProperty))
                    return;
                SetValue(AplicarCalleProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(false)]
        public Boolean EditPanel
        {
            get { return (Boolean)GetValue(EditPanelProperty); }
            set
            {
                if (value == (Boolean)GetValue(EditPanelProperty))
                    return;
                SetValue(EditPanelProperty, value);

                //Set panel in edit mode.
                fToogleEditPanels(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String IDTemplate
        {
            get { return (string)GetValue(IDTemplateProperty); }
            set
            {
                if (value == (string)GetValue(IDTemplateProperty))
                    return;
                SetValue(IDTemplateProperty, value);

                //Show DB saved information.
                //fGetPanelAspectsInfo(value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public Int32 AspectsNumber
        {
            get { return (Int32)GetValue(AspectsNumberProperty); }
            set
            {
                if (value == (Int32)GetValue(AspectsNumberProperty))
                    return;
                SetValue(AspectsNumberProperty, value);
            }
        }


        [Category("Panel Properties")]
        [Browsable(false)]
        public GraphicShowMode EditMode
        {
            get;
            private set;
        }

        [Category("Aspect 1 Configuration")]
        [Browsable(true)]
        public String Aspecto1Texto
        {
            get { return (String)GetValue(Aspecto1TextoProperty); }
            set
            {
                if (value == (String)GetValue(Aspecto1TextoProperty))
                    return;
                SetValue(Aspecto1TextoProperty, value);
            }
        }

        [Category("Aspect 1 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto1Icono1
        {
            get { return (Int32)GetValue(Aspecto1Icono1Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto1Icono1Property))
                    return;
                SetValue(Aspecto1Icono1Property, value);
            }
        }

        [Category("Aspect 1 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto1Icono2
        {
            get { return (Int32)GetValue(Aspecto1Icono2Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto1Icono2Property))
                    return;
                SetValue(Aspecto1Icono2Property, value);
            }
        }

        [Category("Aspect 2 Configuration")]
        [Browsable(true)]
        public String Aspecto2Texto
        {
            get { return (String)GetValue(Aspecto2TextoProperty); }
            set
            {
                if (value == (String)GetValue(Aspecto2TextoProperty))
                    return;
                SetValue(Aspecto2TextoProperty, value);
            }
        }

        [Category("Aspect 2 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto2Icono1
        {
            get { return (Int32)GetValue(Aspecto2Icono1Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto2Icono1Property))
                    return;
                SetValue(Aspecto2Icono1Property, value);
            }
        }

        [Category("Aspect 2 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto2Icono2
        {
            get { return (Int32)GetValue(Aspecto2Icono2Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto2Icono2Property))
                    return;
                SetValue(Aspecto2Icono2Property, value);
            }
        }

        [Category("Aspect 3 Configuration")]
        [Browsable(true)]
        public String Aspecto3Texto
        {
            get { return (String)GetValue(Aspecto3TextoProperty); }
            set
            {
                if (value == (String)GetValue(Aspecto3TextoProperty))
                    return;
                SetValue(Aspecto3TextoProperty, value);
            }
        }

        [Category("Aspect 3 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto3Icono1
        {
            get { return (Int32)GetValue(Aspecto3Icono1Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto3Icono1Property))
                    return;
                SetValue(Aspecto3Icono1Property, value);
            }
        }

        [Category("Aspect 3 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto3Icono2
        {
            get { return (Int32)GetValue(Aspecto3Icono2Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto3Icono2Property))
                    return;
                SetValue(Aspecto3Icono2Property, value);
            }
        }

        [Category("Aspect 4 Configuration")]
        [Browsable(true)]
        public String Aspecto4Texto
        {
            get { return (String)GetValue(Aspecto4TextoProperty); }
            set
            {
                if (value == (String)GetValue(Aspecto4TextoProperty))
                    return;
                SetValue(Aspecto4TextoProperty, value);
            }
        }

        [Category("Aspect 4 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto4Icono1
        {
            get { return (Int32)GetValue(Aspecto4Icono1Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto4Icono1Property))
                    return;
                SetValue(Aspecto4Icono1Property, value);
            }
        }

        [Category("Aspect 4 Configuration")]
        [Browsable(true)]
        public Int32 Aspecto4Icono2
        {
            get { return (Int32)GetValue(Aspecto4Icono2Property); }
            set
            {
                if (value == (Int32)GetValue(Aspecto4Icono2Property))
                    return;
                SetValue(Aspecto4Icono2Property, value);
            }
        }
        #endregion


        #region DEPENDENCY PROPERTIES

        // SQL connection information.
        public static readonly DependencyProperty ServerProperty = DependencyProperty.Register("Server", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty DataBaseProperty = DependencyProperty.Register("DataBase", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register("User", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(EdicionPanelesv2));

        //Configuration
        public static readonly DependencyProperty AppLangProperty = DependencyProperty.Register("AppLang", typeof(String), typeof(EdicionPanelesv2));

        //Specific I/O public control properties.
        public static readonly DependencyProperty AspectsNumberProperty = DependencyProperty.Register("AspectsNumber", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(EdicionPanelesv2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EdicionPanelesv2 _editPanels = d as EdicionPanelesv2;
            if (_editPanels == null || e.NewValue == null)
                return;

            _editPanels.fTypologyConfigPanels(e.NewValue.ToString());
        }
        public static readonly DependencyProperty PanelDataProperty = DependencyProperty.Register("PanelData", typeof(String), typeof(EdicionPanelesv2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelDataChanged)));
        private static void OnPanelDataChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EdicionPanelesv2 _editPanels = d as EdicionPanelesv2;
            if (_editPanels == null ||e.NewValue == null)
                return;

            _editPanels.fCopyHistoricalPanel(e.NewValue.ToString());
        }
        public static readonly DependencyProperty AplicarCalleProperty = DependencyProperty.Register("AplicarCalle", typeof(Boolean), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty EditPanelProperty = DependencyProperty.Register("EditPanel", typeof(Boolean), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty IDTemplateProperty = DependencyProperty.Register("IDTemplate", typeof(String), typeof(EdicionPanelesv2), new PropertyMetadata("", new PropertyChangedCallback(OnIDTemplateChanged)));
        private static void OnIDTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EdicionPanelesv2 _editPanels = d as EdicionPanelesv2;

            if (_editPanels == null || e.NewValue == null)
                return;

            _editPanels.fGetPanelAspectsInfo(e.NewValue.ToString());
        }

        

        //Config property.
        public static readonly DependencyProperty Aspecto1TextoProperty = DependencyProperty.Register("Aspecto1Texto", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto2TextoProperty = DependencyProperty.Register("Aspecto2Texto", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto3TextoProperty = DependencyProperty.Register("Aspecto3Texto", typeof(String), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto4TextoProperty = DependencyProperty.Register("Aspecto4Texto", typeof(String), typeof(EdicionPanelesv2));

        public static readonly DependencyProperty Aspecto1Icono1Property = DependencyProperty.Register("Aspecto1Icono1", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto2Icono1Property = DependencyProperty.Register("Aspecto2Icono1", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto3Icono1Property = DependencyProperty.Register("Aspecto3Icono1", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto4Icono1Property = DependencyProperty.Register("Aspecto4Icono1", typeof(Int32), typeof(EdicionPanelesv2));

        public static readonly DependencyProperty Aspecto1Icono2Property = DependencyProperty.Register("Aspecto1Icono2", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto2Icono2Property = DependencyProperty.Register("Aspecto2Icono2", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto3Icono2Property = DependencyProperty.Register("Aspecto3Icono2", typeof(Int32), typeof(EdicionPanelesv2));
        public static readonly DependencyProperty Aspecto4Icono2Property = DependencyProperty.Register("Aspecto4Icono2", typeof(Int32), typeof(EdicionPanelesv2));
        #endregion


        #region CONSTRUCTORS

        public EdicionPanelesv2()
        {
            InitializeComponent();

        }
        #endregion


        #region METHODS

        #endregion


        #region FUNCTIONS

        //Apply panel.
        private void fApplyToStreetPanel()
        {
            //Aspect 1
            this.Aspecto1Icono1 = this.Aspecto1.Icono1;
            this.Aspecto1Icono2 = this.Aspecto1.Icono2;
            this.Aspecto1Texto = String.Format("{0}{1}{2}", this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3);

            //Aspect 2
            this.Aspecto2Icono1 = this.Aspecto2.Icono1;
            this.Aspecto2Icono2 = this.Aspecto2.Icono2;
            this.Aspecto2Texto = String.Format("{0}{1}{2}", this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3);

            //Aspect 3
            this.Aspecto3Icono1 = this.Aspecto3.Icono1;
            this.Aspecto3Icono2 = this.Aspecto3.Icono2;
            this.Aspecto3Texto = String.Format("{0}{1}{2}", this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3);

            //Aspect 4
            this.Aspecto4Icono1 = this.Aspecto4.Icono1;
            this.Aspecto4Icono2 = this.Aspecto4.Icono2;
            this.Aspecto4Texto = String.Format("{0}{1}{2}", this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3);
        }

        //Config graphical panel interface according to the typology.
        private CErrorInfo fTypologyConfigPanels(String mPanelName)
        {
            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //Execute query.
            String _query = String.Format("SELECT DISTINCT * FROM Tipologia WHERE ID = (SELECT Tipologia FROM Instancia WHERE Panel='{0}')", mPanelName);
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return new CErrorInfo(CErrorCode.ERR_PNL_TYPE, CLangCode.GetErrorText(CErrorCode.ERR_PNL_TYPE));

            //Get results.
            m_typeDescr = new CTypologyDescr();
            m_typeDescr.ID = Convert.ToInt32(_dbResult.Rows[0].GetValue("ID"));
            m_typeDescr.Name = _dbResult.Rows[0].GetValue("Nombre").ToString();
            m_typeDescr.Description = _dbResult.Rows[0].GetValue("Descripcion").ToString();
            m_typeDescr.NumLines = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumLineas"));
            m_typeDescr.NumIcons = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumIconos"));
            m_typeDescr.NumChars = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumChar"));

            //Set panel typology description.
            this.Aspecto1.TypeDescr = m_typeDescr;
            this.Aspecto2.TypeDescr = m_typeDescr;
            this.Aspecto3.TypeDescr = m_typeDescr;
            this.Aspecto4.TypeDescr = m_typeDescr;

            //Set default view number of panels.
            this.x0.IsChecked = true;
            this.txtTemplate.Text = "";

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Show number of aspect for this panel.
        private void fShowNumberOfGraphicalPanels(Int32 mNumber)
        {
            //Set default.
            this.Aspecto1.Visibility = Visibility.Hidden;
            this.Aspecto2.Visibility = Visibility.Hidden;
            this.Aspecto3.Visibility = Visibility.Hidden;
            this.Aspecto4.Visibility = Visibility.Hidden;
            this.n1.Visibility = Visibility.Hidden;
            this.n2.Visibility = Visibility.Hidden;
            this.n3.Visibility = Visibility.Hidden;
            this.n4.Visibility = Visibility.Hidden;

            //Show all number of paneles selected.
            if (mNumber >= 1)
            {
                this.Aspecto1.Visibility = Visibility.Visible;
                this.n1.Visibility = Visibility.Visible;
            }

            if (mNumber >= 2)
            {
                this.Aspecto2.Visibility = Visibility.Visible;
                this.n2.Visibility = Visibility.Visible;
            }

            if (mNumber >= 3)
            {
                this.Aspecto3.Visibility = Visibility.Visible;
                this.n3.Visibility = Visibility.Visible;
            }

            if (mNumber >= 4)
            {
                this.Aspecto4.Visibility = Visibility.Visible;
                this.n4.Visibility = Visibility.Visible;
            }

            //Set number of aspects.
            this.m_numberAspects = mNumber;

            //Set radio button.
            RadioButton _rdBtn = (RadioButton)this.FindName(String.Format("x{0}", mNumber));
            if (_rdBtn == null)
                return;
            _rdBtn.IsChecked = true;
        }

        //Set to true/false panels to edit
        private void fToogleEditPanels(Boolean mEdit)
        {
            //Enabled panels to edit.
            this.Aspecto1.IsEnabled = mEdit;
            this.Aspecto2.IsEnabled = mEdit;
            this.Aspecto3.IsEnabled = mEdit;
            this.Aspecto4.IsEnabled = mEdit;

            //Enabled selector controls.
            this.txtTemplate.IsEnabled = mEdit;
            this.x0.IsEnabled = mEdit;
            this.x1.IsEnabled = mEdit;
            this.x2.IsEnabled = mEdit;
            this.x3.IsEnabled = mEdit;
            this.x4.IsEnabled = mEdit;

            //Show/Hide saves operationals buttons.
            this.btnSave.Visibility = (mEdit == true && EditMode == GraphicShowMode.TemplateMode) ? Visibility.Visible : Visibility.Hidden;
            this.btnCancel.Visibility = (mEdit == true && EditMode == GraphicShowMode.TemplateMode) ? Visibility.Visible : Visibility.Hidden;
        }

        //Get information from templates id.
        private CErrorInfo fGetPanelAspectsInfo(String mID)
        {
            //Set apply button to false.
            this.AplicarCalle = false;
            this.EditPanel = false;

            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT));

            //If IDTemplates is empty return with no actions. Preserve actual panels.
            if (String.IsNullOrEmpty(this.IDTemplate) == true)
                return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");

            //Create a new list of aspects panels.
            List<CAspectPanel> _aspectsList = new List<CAspectPanel>();

            //Execute query.
            String _query = String.Format("SELECT Nombre FROM Plantilla WHERE ID='{0}'", this.IDTemplate);
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return new CErrorInfo(CErrorCode.ERR_DB_QUERY, CLangCode.GetErrorText(CErrorCode.ERR_DB_QUERY));

            //Set template name into.
            this.txtTemplate.Text = _dbResult.Rows[0].GetValue("Nombre").ToString();

            //Query aspects of template. 
            _query = String.Format("SELECT a.*, p.Nombre FROM Aspecto a, Plantilla p WHERE a.Plantilla = '{0}' AND a.Plantilla = p.ID", mID);
            _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //if Yes, retreive it.
            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CAspectPanel _aspect = new CAspectPanel();
                _aspect.ID = _row.GetValue("ID").ToString();
                //_aspect.Panel = _row.GetValue("Panel").ToString();
                //_aspect.Tipology = Convert.ToInt32(_row.GetValue("Tipologia"));
                _aspect.NumAspect = Convert.ToInt32(_row.GetValue("NumeroAspecto"));
                //_aspect.Timestamp = (DateTime)_row.GetValue("FechaHora");
                _aspect.Icon1 = (_row.GetValue("Icono1") is DBNull) ? 0 : Convert.ToInt32(_row.GetValue("Icono1"));
                _aspect.Icon2 = (_row.GetValue("Icono2") is DBNull) ? 0 : Convert.ToInt32(_row.GetValue("Icono2"));
                _aspect.Line1 = _row.GetValue("Linea1").ToString();
                _aspect.Line2 = _row.GetValue("Linea2").ToString();
                _aspect.Line3 = _row.GetValue("Linea3").ToString();

                //Add to list of aspects.
                _aspectsList.Add(_aspect);
            }

            //Enabled edition panel.
            this.btnEdit.IsEnabled = true;
            this.EditMode = GraphicShowMode.TemplateMode;

            //Show panel aspects.
            fSetGraphicalObjects(_aspectsList.ToArray());

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Get information from historic panel.
        private CErrorInfo fCopyHistoricalPanel(String mPanelData)
        {
            //Set edition to false.
            this.EditPanel = false;

            //If panel has no data, just skip.
            if (String.IsNullOrEmpty(mPanelData))
                return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");

            //Split data serialization panels.
            String[] _splittedData = mPanelData.Split(CHAR_SEPARATOR);

            //Check if there are all amount of data. (5 data x panel)
            Int32 _nData = 5;
            if ((_splittedData.Length % _nData) != 0)
                return new CErrorInfo(CErrorCode.ERR_PNL_DATA_INVALID, CLangCode.GetErrorText(CErrorCode.ERR_PNL_DATA_INVALID));

            List<CAspectPanel> _aspectsList = new List<CAspectPanel>();

            for(int i= 0; i < _splittedData.Length; i+= _nData)
            {
                CAspectPanel _rAspect = new CAspectPanel();
                _rAspect.Icon1 = Convert.ToInt32(_splittedData[i + 0]);
                _rAspect.Icon2 = Convert.ToInt32(_splittedData[i + 1]);
                _rAspect.Line1 = _splittedData[i + 2];
                _rAspect.Line2 = _splittedData[i + 3];
                _rAspect.Line3 = _splittedData[i + 4];
                _rAspect.NumAspect = i / 5 + 1;

                _aspectsList.Add(_rAspect);
            }

            //Unabled edition panel.
            this.EditMode = GraphicShowMode.HistoricMode;

            //Show panel.
            fSetGraphicalObjects(_aspectsList.ToArray());

            //Delete name of panel.
            this.txtTemplate.Text = "";
            this.PanelData = "";

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Show new aspect data into each panel.
        private void fSetGraphicalObjects(CAspectPanel[] mAspectArr)
        {
            //Set panels graphical info.
            foreach(CAspectPanel _rAspect in mAspectArr)
            {
                switch(_rAspect.NumAspect)
                {
                    case 1:
                        Aspecto1.Icono1 = _rAspect.Icon1;
                        Aspecto1.Icono2 = _rAspect.Icon2;
                        Aspecto1.Linea1 = _rAspect.Line1;
                        Aspecto1.Linea2 = _rAspect.Line2;
                        Aspecto1.Linea3 = _rAspect.Line3;
                        Aspecto1.UpdateData();
                        break;
                    case 2:
                        Aspecto2.Icono1 = _rAspect.Icon1;
                        Aspecto2.Icono2 = _rAspect.Icon2;
                        Aspecto2.Linea1 = _rAspect.Line1;
                        Aspecto2.Linea2 = _rAspect.Line2;
                        Aspecto2.Linea3 = _rAspect.Line3;
                        Aspecto2.UpdateData();
                        break;
                    case 3:
                        Aspecto3.Icono1 = _rAspect.Icon1;
                        Aspecto3.Icono2 = _rAspect.Icon2;
                        Aspecto3.Linea1 = _rAspect.Line1;
                        Aspecto3.Linea2 = _rAspect.Line2;
                        Aspecto3.Linea3 = _rAspect.Line3;
                        Aspecto3.UpdateData();
                        break;
                    case 4:
                        Aspecto4.Icono1 = _rAspect.Icon1;
                        Aspecto4.Icono2 = _rAspect.Icon2;
                        Aspecto4.Linea1 = _rAspect.Line1;
                        Aspecto4.Linea2 = _rAspect.Line2;
                        Aspecto4.Linea3 = _rAspect.Line3;
                        Aspecto4.UpdateData();
                        break;
                    default:
                        break;
                }
            }

            //Show all defined panels.
            fShowNumberOfGraphicalPanels(mAspectArr.Length);
        }

        //Save template into db.
        private CErrorInfo fSaveTemplate()
        {
            //Check number of aspects.
            if (this.m_numberAspects == 0)
                return new CErrorInfo(CErrorCode.ERR_ASPECTS_NOSEL, CLangCode.GetErrorText(CErrorCode.ERR_ASPECTS_NOSEL));

            //Update template
            String _query = String.Format("UPDATE Plantilla SET Nombre='{0}', NumeroAspectos={1} WHERE ID='{2}'", this.txtTemplate.Text.Trim(), this.m_numberAspects, Guid.Parse(this.IDTemplate));
            m_sqlConnection.ExecuteQuery(_query);

            //Delete all aspects before update.
            _query = String.Format("DELETE FROM Aspecto WHERE Plantilla='{0}'", this.IDTemplate);
            m_sqlConnection.ExecuteQuery(_query);

            //Insert aspects.
            _query = String.Format("INSERT INTO Aspecto VALUES ");
            if (this.m_numberAspects >= 1)
            {
                _query += String.Format("('{0}','{1}',{2},{3},{4},'{5}','{6}','{7}')", 
                    Guid.NewGuid(), Guid.Parse(this.IDTemplate), 1, this.Aspecto1.Icono1, this.Aspecto1.Icono2, this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3);
            }

            if (this.m_numberAspects >= 2)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},{4},'{5}','{6}','{7}')", 
                    Guid.NewGuid(), Guid.Parse(this.IDTemplate), 2, this.Aspecto2.Icono1, this.Aspecto2.Icono2, this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3);
            }

            if (this.m_numberAspects >= 3)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},{4},'{5}','{6}','{7}')", 
                    Guid.NewGuid(), Guid.Parse(this.IDTemplate), 3, this.Aspecto3.Icono1, this.Aspecto3.Icono2, this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3);
            }

            if (this.m_numberAspects >= 4)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},{4},'{5}','{6}','{7}')", 
                    Guid.NewGuid(), Guid.Parse(this.IDTemplate), 4, this.Aspecto4.Icono1, this.Aspecto4.Icono2, this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3);
            }

            //Insert new data.
            m_sqlConnection.ExecuteQuery(_query);

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Save historic into db.
        private CErrorInfo fSaveHistoricalPanel()
        {
            //Check number of aspects.
            if (this.m_numberAspects == 0)
                return new CErrorInfo(CErrorCode.ERR_ASPECTS_NOSEL, CLangCode.GetErrorText(CErrorCode.ERR_ASPECTS_NOSEL));

            //Get timestamp. ('2012-06-18T10:34:09')
            String _dtNow = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

            //string executequery = "INSERT INTO [Historico] VALUES('" + Guid.NewGuid() + "','" + Panel + "'," + Tipologia + "," + NumeroAspecto + ",'" + GlobalStatic.DateToSQLString(Fecha) + "'," + i1 + "," + i2 + ",'" + Linea1 + "','" + Linea2 + "','" + Linea3 + "')";
            String _query = String.Format("INSERT INTO Historico VALUES ");
            if (this.m_numberAspects >= 1)
            {
                _query += String.Format("('{0}','{1}',{2},{3},'{4}',{5},{6},'{7}','{8}','{9}')",
                    Guid.NewGuid(), this.Panel, m_typeDescr.ID, 1, _dtNow, this.Aspecto1.Icono1, this.Aspecto1.Icono2, this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3);
            }

            if (this.m_numberAspects >= 2)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},'{4}',{5},{6},'{7}','{8}','{9}')",
                    Guid.NewGuid(), this.Panel, m_typeDescr.ID, 2, _dtNow, this.Aspecto2.Icono1, this.Aspecto2.Icono2, this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3);
            }

            if (this.m_numberAspects >= 3)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},'{4}',{5},{6},'{7}','{8}','{9}')",
                    Guid.NewGuid(), this.Panel, m_typeDescr.ID, 3, _dtNow, this.Aspecto3.Icono1, this.Aspecto3.Icono2, this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3);
            }

            if (this.m_numberAspects >= 4)
            {
                _query += String.Format(",('{0}','{1}',{2},{3},'{4}',{5},{6},'{7}','{8}','{9}')",
                    Guid.NewGuid(), this.Panel, m_typeDescr.ID, 4, _dtNow, this.Aspecto4.Icono1, this.Aspecto4.Icono2, this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3);
            }

            //Insert
            m_sqlConnection.ExecuteQuery(_query);

            return new CErrorInfo(CErrorCode.ERR_ALL_OK, "");
        }

        //Show errors to user.
        private void OnShowError(CErrorInfo mInfo)
        {
            MessageBox.Show(mInfo.AdditionalInfo, "Panel Manager", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        //Translate applications texts.
        private void fTranslateLabels(String mLang)
        {
            //Load texts
            CLangCode.GetLanguageTexts(m_sqlConnection, mLang);

            this.lbTemplate.Text = CLangCode.GetText(CLangCode.PE2_NAME_LABEL_TEMPLATE);
            this.lbNumber.Text = CLangCode.GetText(CLangCode.PE2_NAME_LABEL_ASPECTS);
            this.btnApply.Content = CLangCode.GetText(CLangCode.PE2_NAME_BTN_APPLY);
            this.btnCancel.Content = CLangCode.GetText(CLangCode.PE2_NAME_BTN_CANCEL);
            this.btnSave.Content = CLangCode.GetText(CLangCode.PE2_NAME_BTN_SAVE);
        }

        #endregion


        #region EVENTS HANDLERS

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Create sql server connection.
            //String ConnectionStr = String.Format("Server={0};Database={1};User Id={2};Password={3}", "WIN-LQSNP9H3JH8", "Panels", "sa", "Sipro2019");
            m_sqlConnection = new CDBSqlServer(ConnectionStr);
            Boolean _r = m_sqlConnection.Connect();
            if (_r == false)
                return;

            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
            {
                OnShowError(new CErrorInfo(CErrorCode.ERR_DB_CONNECT, CLangCode.GetErrorText(CErrorCode.ERR_DB_CONNECT)));
                return;
            }

            //Set database connection to controls.
            this.Aspecto1.DBConnection = m_sqlConnection;
            this.Aspecto2.DBConnection = m_sqlConnection;
            this.Aspecto3.DBConnection = m_sqlConnection;
            this.Aspecto4.DBConnection = m_sqlConnection;

            //Set the default mode.
            this.EditMode = GraphicShowMode.TemplateMode;

            //Set default view number of panels.
            this.x0.IsChecked = true;
            this.txtTemplate.Text = "";

            //Set config of panels
            fTypologyConfigPanels(this.Panel);

            //Translations
            fTranslateLabels(AppLang);
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection.Close();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton _rdBtn = e.Source as RadioButton;
            if (_rdBtn == null)
                return;

            //Get number of panels and set visibility.
            Int32 _num = Convert.ToInt32(_rdBtn.Content);
            fShowNumberOfGraphicalPanels(_num);
            this.AspectsNumber = _num;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            CErrorInfo _error = fSaveTemplate();
            if (_error.ErrorCode != CErrorCode.ERR_ALL_OK)
            {
                OnShowError(_error);
                return;
            }

            this.EditPanel = false;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.EditPanel = false;
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            //Apply aspects to panels and save on historical db.
            CErrorInfo _error = fSaveHistoricalPanel();
            if (_error.ErrorCode != CErrorCode.ERR_ALL_OK)
            {
                OnShowError(_error);
                return;
            }

            //Copy aspect information to output vars.
            fApplyToStreetPanel();

            //Set apply var.
            this.AplicarCalle = true;
            this.EditPanel = false;
        }
        
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            this.EditPanel = true;
        }

        #endregion
    }


    public enum GraphicShowMode
    {
        TemplateMode = 0,
        HistoricMode = 1
    }
}
