﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelsControls
{
    public class CTypologyDescr
    {
        #region PROPERTIES

        public Int32 ID
        {
            get;
            set;
        }

        public String Name
        {
            get;
            set;
        }

        public String Description
        {
            get;
            set;
        }

        public Int32 NumLines
        {
            get;
            set;
        }

        public Int32 NumIcons
        {
            get;
            set;
        }

        public Int32 NumChars
        {
            get;
            set;
        }

        #endregion

    }
}
