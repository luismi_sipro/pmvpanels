﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace PanelsControls
{
    public static class GlobalStatic
    {
        public static BitmapImage GetImageFromStream(byte[] data)
        {
            if (data == null) return null;
            using (var stream = new MemoryStream(data))
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = stream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();
                return bitmap;
            }
        }

        public static BitmapImage GetImageFromStream(Stream stream)
        {
            if (stream == null) return null;
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.StreamSource = stream;
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            bitmap.Freeze();
            return bitmap;

        }

        public static Stream ToStream(Image image, ImageFormat format)
        {
            var stream = new System.IO.MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }

        public static string DateToSQLString(DateTime DATE)
        {
            try
            {
                string aux = DATE.Year.ToString().PadLeft(4, '0') + "-" + DATE.Month.ToString().PadLeft(2, '0') + "-" + DATE.Day.ToString().PadLeft(2, '0') + "T" + DATE.Hour.ToString().PadLeft(2, '0') + ":" + DATE.Minute.ToString().PadLeft(2, '0') + ":" + DATE.Second.ToString().PadLeft(2, '0');  //'2012-06-18T10:34:09'
                return aux;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
