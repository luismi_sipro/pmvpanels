﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PanelsControls
{
    public partial class NewTemplate : Form
    {

        public Guid ID = Guid.Empty;
        public string TemplateName = "";
        public Guid Category = Guid.Empty;
        public int Tipologia = 32;

        public Guid IDEditingTemplate = Guid.Empty;

        public SQLServer db;

        public NewTemplate()
        {
            InitializeComponent();
        }

        private void NewFolder_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
            this.textBox1.Select();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (AddNewTemplate(this.textBox1.Text))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("No se ha podido añadir la nueva plantilla.");
            }
        }

        private bool AddNewTemplate(string Name)
        {
            try
            {
                ID = Guid.NewGuid();
                TemplateName = Name;

                if (IDEditingTemplate == Guid.Empty)
                {
                    using (SQLOperations SQL = new SQLOperations(db)) return SQL.INSERT_Plantilla(ID, TemplateName, 0, Category, Tipologia);
                }
                else
                {
                    return DuplicateTemplate(ID, TemplateName, IDEditingTemplate);
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool DuplicateTemplate(Guid ID, string Name, Guid SourceID)
        {
            try
            {
                using (SQLOperations SQL = new SQLOperations(db))
                {
                    foreach (DataRow prow in SQL.SELECT_Plantilla_byID(SourceID).Rows)
                    {
                        if (SQL.INSERT_Plantilla(ID, Name, (int)prow["NumeroAspectos"], Category, (int)prow["Tipologia"]))
                        {
                            foreach (DataRow arow in SQL.SELECT_Aspecto_byPlantilla(SourceID).Rows)
                            {
                                if (!SQL.INSERT_Aspecto(Guid.NewGuid(), ID, (int)arow["NumeroAspecto"], (int)arow["Icono1"], (int)arow["Icono2"], (string)arow["Linea1"], (string)arow["Linea2"], (string)arow["Linea3"])) return false;
                            }
                        }
                        else return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
