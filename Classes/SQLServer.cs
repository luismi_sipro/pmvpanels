﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelsControls
{
    public class SQLServer : IDisposable
    {
        SqlDataAdapter daAdapter = null;
        DataTable dtTable = null;
        SqlCommandBuilder cbUpdate = null;

        public string LastError = string.Empty;
        string connString = string.Empty;
        public static string DefaultConnectionString = string.Empty;

        public string ConnectionString
        {
            get
            {
                if (connString.Length <= 0) connString = DefaultConnectionString;
                return connString;
            }
            set
            {
                connString = value;
            }
        }

        public SQLServer()
        {
            ConnectionString = DefaultConnectionString;
        }

        public SQLServer(string connectionString)
        {
            ConnectionString = connectionString;          
        }

        public string ConnectionName = "default";

        volatile static bool FirstInstance = true;

        public static bool KeepConnectionOpen = false;

        string FormatSql(string sql)
        {
            return sql
                .Replace(Environment.NewLine, " ")
                .Replace("\t", " ");
        }

        SqlConnection Connection = null;
        int Connect(ref SqlConnection con)
        {
            DateTime start = DateTime.Now;
            con.Open();

            if (FirstInstance)
            {
                FirstInstance = false;
            }

            int ms = (int)(DateTime.Now.Subtract(start).TotalMilliseconds);
            return ms;
        }

        public bool CheckConnection()
        {
            if (ConnectionString == "") return false;
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString);
                con.Open();
                if (con.State != ConnectionState.Open)
                {
                    con.Dispose();
                    return false;
                }
                else
                {
                    con.Close();
                    con.Dispose();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        SqlConnection GetConnection(string ConnectionString)
        {
            try
            {
                if (FirstInstance)
                {
                    FirstInstance = false;
                }

                if (null == Connection)
                    Connection = new SqlConnection(ConnectionString);

                if (Connection.State == ConnectionState.Open)
                {
                    Connection.Close();
                    Connection.Dispose();
                    Connection = new SqlConnection(ConnectionString);
                }


                if (Connection.State != ConnectionState.Open)
                {
                    Connection.Dispose();
                    Connection = new SqlConnection(ConnectionString);
                    DateTime start = DateTime.Now;
                    Connection.Open();
                }

                if (Connection.State != ConnectionState.Open)
                {
                    DateTime start = DateTime.Now;
                    Connection = new SqlConnection(ConnectionString);
                    Connection.Open();
                }
            }
            catch (Exception)
            {
            }
            return Connection;
        }

        public void CloseConnection()
        {
            try
            {
                if (null != Connection)
                {
                    Connection.Close();
                    Connection.Dispose();
                    Connection = null;
                }
                CloseEditConnection();
            }
            catch { }
        }

        public int Execute(string sql)
        {
            DateTime start = DateTime.Now;

            try
            {
                if (KeepConnectionOpen)
                {
                    GetConnection(ConnectionString);
                    return Execute(sql, Connection, start);
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(ConnectionString))
                    {
                        return Execute(sql, con, start);
                    }
                }
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }
            return 0;
        }

        public int ExecuteWithBlob(string sql, string blobFieldName, byte[] blob)
        {
            DateTime start = DateTime.Now;

            try
            {

                if (KeepConnectionOpen)
                {
                    GetConnection(ConnectionString);
                    return ExecuteWithBlob(sql, Connection, start, blobFieldName, blob);
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(ConnectionString))
                    {
                        return ExecuteWithBlob(sql, con, start, blobFieldName, blob);
                    }
                }
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }
            return 0;
        }

        int Execute(string sql, SqlConnection con, DateTime start)
        {
            int ConTime = 0;

            if (false == KeepConnectionOpen)
                ConTime = Connect(ref con);

            if (con.State.Equals(ConnectionState.Open))
            {
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandTimeout = 0;
                int r = cmd.ExecuteNonQuery();
                cmd.Dispose();
                int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;
                if (false == KeepConnectionOpen)
                    con.Close();
                return r;
            }
            return 0;
        }

        public int ExecuteWithBlob(string sql, SqlConnection litecon, DateTime start, string blobFieldName, byte[] blob)
        {
            int ConTime = 0;

            if (false == KeepConnectionOpen)
                ConTime = Connect(ref litecon);

            if (litecon.State.Equals(ConnectionState.Open))
            {
                SqlCommand cmd = new SqlCommand(sql, litecon);
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@" + blobFieldName, blob);
                int r = cmd.ExecuteNonQuery();
                cmd.Dispose();
                int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;
                if (false == KeepConnectionOpen)
                    litecon.Close();
                return r;
            }
            return 0;
        }

        public DataTable GetDataTableInMemory(DataTable DataInMemory, string sql, string sort = "")
        {
            DataRow[] dr = null;
            sql = sql.Substring(sql.ToUpper().IndexOf("WHERE") + 5);
            if (sort == "")
                dr = DataInMemory.Select(sql);
            else
                dr = DataInMemory.Select(sql, sort);
            if (null == dr) return null;
            if (dr.Length == 0) return null;

            return dr.CopyToDataTable();
        }

        public DataRow[] GetDataRowsInMemory(DataTable DataInMemory, string sql, string sort = "")
        {
            DataRow[] dr = null;
            sql = sql.Substring(sql.ToUpper().IndexOf("WHERE") + 5);
            if (sort == "")
                dr = DataInMemory.Select(sql);
            else
                dr = DataInMemory.Select(sql, sort);
            return dr;
        }

        public DataTable GetDataTable(string sql)
        {
            if (null == ConnectionString && ConnectionString.Length <= 0)
                return new DataTable();

            try
            {
                LastError = string.Empty;
                DateTime start = DateTime.Now;

                if (KeepConnectionOpen)
                {
                    GetConnection(ConnectionString);
                    return GetDataTable(sql, Connection, start);
                }
                else
                {
                    using (SqlConnection con = new SqlConnection(ConnectionString))
                    {
                        return GetDataTable(sql, con, start);
                    }
                }
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }

            return new DataTable();
        }

        DataTable GetDataTable(string sql, SqlConnection con, DateTime start)
        {
            dtTable = new DataTable();
            lock (DataLock)
            {
                int ConTime = 0;

                if (false == KeepConnectionOpen)
                    ConTime = Connect(ref con);

                if (con.State.Equals(ConnectionState.Open))
                {
                    daAdapter = new SqlDataAdapter(sql, con);
                    dtTable.Constraints.Clear();
                    int r = daAdapter.Fill(dtTable);

                    int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;

                    if (false == KeepConnectionOpen)
                        con.Close();

                    daAdapter.Dispose();
                }
            }
            return dtTable;
        }


        private Object DataLock = new Object();

        SqlConnection conEdit = null;

        public SqlDataAdapter GetDataAdapter()
        {
            return daAdapter;
        }

        public DataTable GetDataTableForEdit(string sql)
        {
            dtTable = new DataTable();
            lock (DataLock)
            {


                bool retry = true;
                for (int i = 0; i <= 1 && true == retry; i++)
                {
                    retry = false;
                    try
                    {
                        LastError = string.Empty;
                        DateTime start = DateTime.Now;

                        CloseEditConnection();


                        conEdit = new SqlConnection(ConnectionString);

                        int ConTime = Connect(ref conEdit);
                        if (conEdit.State.Equals(ConnectionState.Open))
                        {
                            daAdapter = new SqlDataAdapter(sql, conEdit);
                            daAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                            daAdapter.MissingMappingAction = MissingMappingAction.Passthrough;
                            daAdapter.RowUpdating += new SqlRowUpdatingEventHandler(daAdapter_RowUpdating);
                            cbUpdate = new SqlCommandBuilder(daAdapter);
                            cbUpdate.ConflictOption = ConflictOption.OverwriteChanges;
                            daAdapter.DeleteCommand = cbUpdate.GetDeleteCommand();
                            daAdapter.UpdateCommand = cbUpdate.GetUpdateCommand();
                            dtTable = new DataTable();
                            int r = daAdapter.Fill(dtTable);
                            daAdapter.AcceptChangesDuringUpdate = true;

                            int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;

                            break; //return dtTable;
                        }
                        else
                        {
                            retry = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        LastError = ex.Message;
                    }
                }
            }
            return dtTable;
        }

        void daAdapter_RowUpdating(object sender, SqlRowUpdatingEventArgs e)
        {

        }

        public void CloseEditConnection()
        {
            try
            {
                if (null != conEdit)
                {
                    conEdit.Close();
                    conEdit.Dispose();
                }
            }
            catch { }
        }

        public virtual bool Save()
        {
            if (null == conEdit)
            {
                return false;
            }
            try
            {
                DateTime start = DateTime.Now;

                if (null == dtTable)
                    return false;

                int r = 0;

                r = daAdapter.Update(dtTable);

                int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;

                return true;
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }
            return false;
        }

        public virtual bool Save(SqlDataAdapter Adapter, DataTable Table)
        {
            if (null == conEdit)
            {
                return false;
            }
            try
            {
                DateTime start = DateTime.Now;

                if (null == Table)
                    return false;
                string command = Adapter.SelectCommand.CommandText;

                GetConnection(ConnectionString);
                Adapter = new SqlDataAdapter(command, ConnectionString);
                SqlCommandBuilder ComandUpdate;
                ComandUpdate = new SqlCommandBuilder(Adapter);
                ComandUpdate.ConflictOption = ConflictOption.OverwriteChanges;
                Adapter.UpdateCommand = ComandUpdate.GetUpdateCommand();
                Adapter.DeleteCommand = ComandUpdate.GetDeleteCommand();
                Adapter.InsertCommand = ComandUpdate.GetInsertCommand();

                int r = Adapter.Update(Table);

                int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;

                return true;
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
            }
            return false;
        }

        public static string GetStringValue(object value)
        {
            try { return Convert.ToString(value); }
            catch { }
            return string.Empty;
        }

        public object GetValue(string sql)
        {
            return GetValue(sql, true);
        }

        public object GetValue(string sql, bool LogResult)
        {
            try
            {
                LastError = string.Empty;
                DateTime start = DateTime.Now;


                if (KeepConnectionOpen)
                {
                    GetConnection(ConnectionString);
                    return GetValue(sql, Connection, start, LogResult);
                }

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    return GetValue(sql, con, start, LogResult);
                }

            }
            catch (Exception ex)
            {

                LastError = ex.Message;
            }
            return new object();
        }

        object GetValue(string sql, SqlConnection con, DateTime start, bool LogResult)
        {
            int ConTime = 0;

            if (false == KeepConnectionOpen)
                ConTime = Connect(ref con);

            if (con.State.Equals(ConnectionState.Open))
            {
                SqlCommand cmd = new SqlCommand(sql, con);
                object r = cmd.ExecuteScalar();
                if (false == KeepConnectionOpen)
                    con.Close();

                int time = (int)DateTime.Now.Subtract(start).TotalMilliseconds;
                return r;
            }
            return new object();
        }
        public virtual string GetStringValue(string sql)
        {
            try
            {
                return Convert.ToString(GetValue(sql));
            }
            catch (Exception ex)
            {                            
                LastError = ex.Message;
            }
            return string.Empty;
        }

     
        public virtual int GetIntValue(string sql)
        {
            try
            {
                return Convert.ToInt32(GetValue(sql));
            }
            catch (Exception ex)
            {               
                LastError = ex.Message;
            }
            return 0;
        }

       
        public virtual double GetDoubleValue(string sql)
        {
            try
            {
                return Convert.ToDouble(GetValue(sql));
            }
            catch (Exception ex)
            {             
                LastError = ex.Message;
            }
            return 0;
        }


    
        public string[] GetStringArray(string sql)
        {
            try
            {
                return GetDataTable(sql).AsEnumerable()
                               .Select(r => (r.Field<object>(0)).ToString())
                               .ToArray();
            }
            catch (Exception ex)
            {               
                LastError = ex.Message;
            }
            return new string[] { };
        }


        public static string CellValue(DataRow row, int FieldIndex)
        {
            try
            {
                if (null != row[FieldIndex] && false == (row[FieldIndex].GetType().Equals(typeof(System.DBNull))))
                    return Convert.ToString(row[FieldIndex]);
            }
            catch
            {
            }
            return string.Empty;
        }

        public static string CellValue(DataRow row, string FieldName)
        {
            try
            {
                if (null != row[FieldName] && false == (row[FieldName].GetType().Equals(typeof(System.DBNull))))
                    return Convert.ToString(row[FieldName]);
            }
            catch
            {
            }
            return string.Empty;
        }

        public static string CellValue(DataTable Table, int RowIndex, string FieldName)
        {
            try
            {
                if (null != Table && null != Table.Rows && Table.Rows.Count > RowIndex)
                    return CellValue(Table.Rows[RowIndex], FieldName);
            }
            catch
            {
            }
            return string.Empty;
        }

        public static string CellValue(DataSet ds, string TableName, int RowIndex, string FieldName)
        {
            if (null != ds && ds.Tables.Count > 0)
            {
                int ix = ds.Tables.IndexOf(TableName);
                if (ix > -1)
                {
                    return CellValue(ds.Tables[ix], RowIndex, FieldName);
                }
            }
            return string.Empty;
        }

        public static DateTime CellValueDateTime(DataRow Row, string FieldName)
        {
            return GetDateTime(CellValue(Row, FieldName));
        }


        public static DateTime CellValueDateTime(DataSet ds, string TableName, int RowIndex, string FieldName)
        {
            return GetDateTime(CellValue(ds, TableName, RowIndex, FieldName));
        }

        public static DateTime CellValueDateTime(DataTable dt, int RowIndex, string FieldName)
        {
            return GetDateTime(CellValue(dt, RowIndex, FieldName));
        }


        public static DateTime CellValueTime(DataRow Row, string FieldName)
        {
            return GetTime(CellValue(Row, FieldName));
        }

        public static DateTime CellValueTime(DataSet ds, string TableName, int RowIndex, string FieldName)
        {
            return GetTime(CellValue(ds, TableName, RowIndex, FieldName));
        }

        public static DateTime CellValueTime(DataTable dt, int RowIndex, string FieldName)
        {
            return GetTime(CellValue(dt, RowIndex, FieldName));
        }


        public static string ToDate(DateTime value)
        {
            return "CONVERT(datetime,'" + value.ToString("yyyy/MM/dd HH:mm:ss") + "',120)";
        }


        public static DateTime GetTime(string datetime)
        {
            return GetDateTime(DateTime.MinValue.ToString("yyyy-MM-dd ") + datetime);
        }


        public static DateTime GetDateTime(string datetime)
        {
            try
            {
                string fmt = "yyyy-MM-dd HH:mm:ss";

                //System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(theCultureString);
                System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                //System.Threading.Thread.CurrentThread.CurrentCulture = ci;

                // dd/mm/yyyy HH:mm:ss ???
                string s = datetime.Replace("/", "-");
                s += ":00"; // enforce seconds if not present

                // ensure fix format
                string[] f = s.Split(new char[] { '-', ' ' });
                string[] h = s.Split(new char[] { ' ' })[1].Split(new char[] { ':', ' ' });

                // bad format?
                if (f.Length < 3)
                    return DateTime.MinValue;

                // yyyy-mm-dd
                if (f[0].Length == 4 && f[1].Length > 0 && f[1].Length <= 2 && f[2].Length > 0 && f[2].Length <= 2)
                {
                    datetime = f[0].PadLeft(4, '0') + "-" + f[1].PadLeft(2, '0') + "-" + f[2].PadLeft(2, '0') + " " + h[0].PadLeft(2, '0') + ":" + h[1].PadLeft(2, '0') + ":" + h[2].PadLeft(2, '0');
                }
                // dd-mm-yyyy
                else if (f[2].Length == 4 && f[1].Length > 0 && f[1].Length <= 2 && f[0].Length > 0 && f[0].Length <= 2)
                {
                    datetime = f[0].PadLeft(2, '0') + "-" + f[1].PadLeft(2, '0') + "-" + f[2].PadLeft(4, '0') + " " + h[0].PadLeft(2, '0') + ":" + h[1].PadLeft(2, '0') + ":" + h[2].PadLeft(2, '0');
                    fmt = "dd-MM-yyyy HH:mm:ss";
                    if (ci.Name.EndsWith("-ES")) fmt = "dd-MM-yyyy HH:mm:ss";
                    if (ci.Name.EndsWith("-US")) fmt = "MM-dd-yyyy HH:mm:ss";
                }
                else
                {
                    return DateTime.MinValue;
                }

                DateTime ret = DateTime.ParseExact(datetime.Substring(0, fmt.Length), fmt, System.Threading.Thread.CurrentThread.CurrentCulture);

                try
                {
                    int ms = 0;
                    if (datetime.IndexOf('.') > -1)
                    {
                        ms = Convert.ToInt32(datetime.Split(new char[] { '.' })[1]);
                        ret.AddMilliseconds(ms);
                    }
                }
                catch { }
                return ret;
            }
            catch { }
            return DateTime.MinValue;
        }

        public static string EscapeString(string text)
        {
            return text.Replace("'", "''");
        }

        public static string DefaultValue0(string text)
        {
            if (text.Trim().Length > 0)
                return text;
            return "0";
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    cbUpdate.Dispose();
                    conEdit.Close();
                    Connection.Close();
                    conEdit.Dispose();
                    Connection.Dispose();
                    daAdapter.Dispose();
                    dtTable.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LKSQLServer() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
