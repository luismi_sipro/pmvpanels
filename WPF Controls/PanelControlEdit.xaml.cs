﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for PanelControl.xaml
    /// </summary>
    public partial class PanelControlEdit : SQLUserControl
    {
        public PanelControlEdit()
        {
            InitializeComponent();

            if (DesignerProperties.GetIsInDesignMode(this)) return;
            InitOffline();
            tmrResize.Interval = new TimeSpan(0, 0, 0, 0, 100);
            tmrResize.Tick += new EventHandler(tmrResize_Tick);
            //tmrRetryDB.Interval = new TimeSpan(0, 0, 0, 0, 500);
            //tmrRetryDB.Tick += new EventHandler(tmrRetryDB_Tick);
            //tmrCheckDB.Interval = new TimeSpan(0, 0, 0, 10, 0);
            //tmrCheckDB.Tick += new EventHandler(tmrCheckDB_Tick);
            tmrRefreshImages.Interval = new TimeSpan(0, 0, 0, 0, 1000);
            tmrRefreshImages.Tick += new EventHandler(tmrRefreshImages_Tick);
            //tmrCheckDB.Start();
            RedrawControl();
        }

        //public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(string), typeof(PanelControlEdit), new UIPropertyMetadata("", new PropertyChangedCallback(OnIDChanged)));
        public static readonly DependencyProperty Icono1Property = DependencyProperty.Register("Icono1", typeof(int), typeof(PanelControlEdit), new UIPropertyMetadata(-1, new PropertyChangedCallback(OnIcono1Changed)));
        public static readonly DependencyProperty Icono2Property = DependencyProperty.Register("Icono2", typeof(int), typeof(PanelControlEdit), new UIPropertyMetadata(-1, new PropertyChangedCallback(OnIcono2Changed)));
        public static readonly DependencyProperty Linea1Property = DependencyProperty.Register("Linea1", typeof(string), typeof(PanelControlEdit), new UIPropertyMetadata("", new PropertyChangedCallback(OnLinea1Changed)));
        public static readonly DependencyProperty Linea2Property = DependencyProperty.Register("Linea2", typeof(string), typeof(PanelControlEdit), new UIPropertyMetadata("", new PropertyChangedCallback(OnLinea2Changed)));
        public static readonly DependencyProperty Linea3Property = DependencyProperty.Register("Linea3", typeof(string), typeof(PanelControlEdit), new UIPropertyMetadata("", new PropertyChangedCallback(OnLinea3Changed)));
        public static readonly DependencyProperty TextoProperty = DependencyProperty.Register("Texto", typeof(string), typeof(PanelControlEdit), new UIPropertyMetadata("", new PropertyChangedCallback(OnTextoChanged)));
        public static readonly DependencyProperty TipologiaProperty = DependencyProperty.Register("Tipologia", typeof(int), typeof(PanelControlEdit), new UIPropertyMetadata(1, new PropertyChangedCallback(OnTipologiaChanged)));


        public bool _OfflineMode = true;

        //public bool OfflineMode
        //{
        //    get
        //    {
        //        return _OfflineMode;
        //    }
        //    set
        //    {
        //        if (_OfflineMode != value)
        //        {
        //            _OfflineMode = value;
        //            if (_OfflineMode == false)
        //                this.Dispatcher.Invoke((Action)delegate
        //                {
        //                    this.ImageGrid.Visibility = Visibility.Hidden;

        //                    //this.BottomFill.Fill = new SolidColorBrush(Colors.Black);
        //                });
        //            else
        //                this.Dispatcher.Invoke((Action)delegate
        //                {
        //                    this.ImageGrid.Visibility = Visibility.Visible;
        //                    //this.BottomFill.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
        //                });
        //        }
        //    }
        //}

        DataTable DTipologia = new DataTable("Tipologia");
        DataTable DTipoIcono = new DataTable("TipoIcono");
        private void InitOffline()
        {
            DTipologia = new DataTable("Tipologia");
            DTipologia.Columns.Add("ID");
            DTipologia.Columns.Add("Nombre");
            DTipologia.Columns.Add("Descripción");
            DTipologia.Columns.Add("NumLineas");
            DTipologia.Columns.Add("NumIconos");
            DTipologia.Columns.Add("NumChar");
            DTipologia.Rows.Add(1, "I", "1 Icono", 0, 1, 12);
            DTipologia.Rows.Add(10, "1L", "1 Linea", 1, 0, 12);
            DTipologia.Rows.Add(11, "1L+1I", "1 Linea y 1 Icono", 1, 1, 12);
            DTipologia.Rows.Add(12, "1L+2I", "1 Linea y 2 Iconos", 1, 2, 12);
            DTipologia.Rows.Add(20, "2L", "2 Lineas", 2, 0, 12);
            DTipologia.Rows.Add(21, "2L+1I", "2 Lineas y 1 Icono", 2, 1, 12);
            DTipologia.Rows.Add(22, "2L+2I", "2 Lineas y 2 Iconos", 2, 2, 12);
            DTipologia.Rows.Add(30, "3L", "3 Lineas", 3, 0, 12);
            DTipologia.Rows.Add(31, "3L+1I", "3 Lineas y 1 Icono", 3, 1, 12);
            DTipologia.Rows.Add(32, "3L+2I", "3 Lineas y 2 Iconos", 3, 2, 12);
            DTipologia.Rows.Add(1116, "16L1I", "1 Linea de 16 caracteres y 1 Icono", 1, 1, 16);

            DTipoIcono = new DataTable("TipoIcono");
            DTipoIcono.Columns.Add("ID");
            DTipoIcono.Columns.Add("Nombre");
            DTipoIcono.Rows.Add(1, "Advertencia de peligro");
            DTipoIcono.Rows.Add(2, "Prioridad");
            DTipoIcono.Rows.Add(3, "Prioridad de entrada");
            DTipoIcono.Rows.Add(4, "Restricción de paso");
            DTipoIcono.Rows.Add(5, "Prohibición o restricción");
            DTipoIcono.Rows.Add(6, "Obligación");
            DTipoIcono.Rows.Add(7, "Fin prohibición restricción");
            DTipoIcono.Rows.Add(8, "Indicaciones generales");
            DTipoIcono.Rows.Add(9, "Carriles");
            DTipoIcono.Rows.Add(10, "Especiales");
            DTipoIcono.Rows.Add(11, "De servicio");
            DTipoIcono.Rows.Add(12, "Otras");
            DTipoIcono.Rows.Add(13, "Advertencia de peligro lejos");
        }

        //private static void OnIDChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    try
        //    {
        //        if (DesignerProperties.GetIsInDesignMode(d)) return;
        //        if (e.NewValue == null) return;
        //        string str1 = e.NewValue.ToString();
        //        if (!string.IsNullOrEmpty(str1))
        //            if (((PanelControlEdit)d).Panel != str1)
        //                ((PanelControlEdit)d).Panel = str1;
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        System.Windows.Threading.DispatcherTimer tmrResize = new System.Windows.Threading.DispatcherTimer();
        //System.Windows.Threading.DispatcherTimer tmrRetryDB = new System.Windows.Threading.DispatcherTimer();


        void tmrResize_Tick(object sender, EventArgs e)
        {
            tmrResize.Stop();

            if (DesignerProperties.GetIsInDesignMode(this)) return;


            if (DinamicW > 0) PanelGrid.Width = PanelGrid.ActualHeight * DinamicW;
            if (DinamicH > 0) PanelGrid.Height = PanelGrid.ActualWidth * DinamicH;

            if (PanelGrid.Height != Double.NaN)
            {
                try
                {
                    if (Col1.ActualWidth > 0) Col1.Width = new GridLength(PanelGrid.Height, GridUnitType.Pixel);
                    //if (Col2.ActualWidth > 0) Col2.Width = new GridLength(PanelGrid.Height * 3, GridUnitType.Pixel);
                    if (Col3.ActualWidth > 0) Col3.Width = new GridLength(PanelGrid.Height, GridUnitType.Pixel);
                }
                catch (Exception)
                {

                }
            }

            DinamicW = 0;
            DinamicH = 0;
            LoadingGrid.Visibility = Visibility.Hidden;
        }

        double DinamicW = 0;
        double DinamicH = 0;

        int _numlines = 3;
        int _numicons = 2;
        int _numchars = 12;
        public int NumLines
        {
            get
            {
                return _numlines;
            }
            set
            {
                _numlines = value;
                RedrawControl();
            }
        }

        public int NumIcons
        {
            get
            {
                return _numicons;
            }
            set
            {
                _numicons = value;
                RedrawControl();
            }
        }


        public int NumChars
        {
            get
            {
                return _numchars;
            }
            set
            {
                _numchars = value;

                if (_numchars == 12)
                {
                    this.PanelLinea16.Visibility = Visibility.Hidden;
                    this.PanelLinea26.Visibility = Visibility.Hidden;
                    this.PanelLinea36.Visibility = Visibility.Hidden;
                    this.PanelLinea1.Visibility = Visibility.Visible;
                    this.PanelLinea2.Visibility = Visibility.Visible;
                    this.PanelLinea3.Visibility = Visibility.Visible;
                }

                if (_numchars == 16)
                {
                    this.PanelLinea1.Visibility = Visibility.Hidden;
                    this.PanelLinea2.Visibility = Visibility.Hidden;
                    this.PanelLinea3.Visibility = Visibility.Hidden;
                    this.PanelLinea16.Visibility = Visibility.Visible;
                    this.PanelLinea26.Visibility = Visibility.Visible;
                    this.PanelLinea36.Visibility = Visibility.Visible;
                }

                RedrawControl();
            }
        }

        //SQLOperations SQL = null;


        protected override void InitControl()
        {
            //SQL = new SQLOperations(db);
            //CheckDBFunction();
        }


        //internal protected string _Panel = "";
        //[Category("Custom Properties")]
        //[Browsable(true)]
        //[Description("Panel Name")]
        //public string Panel
        //{
        //    get
        //    {
        //        if (_Panel != (string)base.GetValue(PanelProperty)) base.SetValue(PanelProperty, _Panel);
        //        return _Panel;
        //    }
        //    set
        //    {
        //        if (null != value)
        //        {
        //            _Panel = value;
        //            base.SetValue(PanelProperty, value);
        //            LoadTipology();
        //        }
        //    }
        //}

        int _Icono1 = -1;
        int _Icono2 = -1;
        string _Linea1 = "";
        string _Linea2 = "";
        string _Linea3 = "";
        string _Texto = "";
        int _Tipologia = 1;
        //public int Icono1 { get { return _Icono1; } set { _Icono1 = value; if (NumIcons > 0) LoadIcon(Icono1, this.Icon1); } }
        //public int Icono2 { get { return _Icono2; } set { _Icono2 = value; if (NumIcons > 1) LoadIcon(Icono2, this.Icon2); } }
        //public string Linea1 { get { return _Linea1; } set { _Linea1 = value; if (NumLines > 0) LoadLine(Linea1, this.PanelLinea1); } }
        //public string Linea2 { get { return _Linea2; } set { _Linea2 = value; if (NumLines > 1) LoadLine(Linea2, this.PanelLinea2); } }
        //public string Linea3 { get { return _Linea3; } set { _Linea3 = value; if (NumLines > 2) LoadLine(Linea3, this.PanelLinea3); } }


        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Icono 1")]
        public int Icono1
        {
            get
            {
                if (_Icono1 != (int)base.GetValue(Icono1Property))
                {
                    base.SetValue(Icono1Property, _Icono1);                  
                }

                return _Icono1;
            }
            set
            {
                if (value != _Icono1)
                {
                    _Icono1 = value;
                    base.SetValue(Icono1Property, value);
                    if (NumIcons > 0)
                        {
                            if (LoadIcon(_Icono1, this.Icon1))
                                this.Icon1.Visibility = Visibility.Visible;
                            else
                                this.Icon1.Visibility = Visibility.Hidden;
                        }
                }
            }
        }

        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Icono 2")]
        public int Icono2
        {
            get
            {
                if (_Icono2 != (int)base.GetValue(Icono2Property))
                {
                    base.SetValue(Icono2Property, _Icono2);                   
                }

                return _Icono2;
            }
            set
            {
                if (value != _Icono2)
                {
                    _Icono2 = value;
                    base.SetValue(Icono2Property, value);
                    if (NumIcons > 1)
                        {
                            if (LoadIcon(_Icono2, this.Icon2))
                                this.Icon2.Visibility = Visibility.Visible;
                            else
                                this.Icon2.Visibility = Visibility.Hidden;
                        }
                }
            }
        }


        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Linea 1")]
        public string Linea1
        {
            get
            {
                if (_Linea1 != (string)base.GetValue(Linea1Property))
                {
                    base.SetValue(Linea1Property, _Linea1);                    
                }

                return _Linea1;
            }
            set
            {
                if (value != _Linea1)
                {
                    _Linea1 = value;
                    base.SetValue(Linea1Property, value);
                    if (NumLines > 0) LoadLine(_Linea1, this.PanelLinea1, this.PanelLinea16);
                }
            }
        }

        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Linea 2")]
        public string Linea2
        {
            get
            {
                if (_Linea2 != (string)base.GetValue(Linea2Property))
                {
                    base.SetValue(Linea2Property, _Linea2);                    
                }
                return _Linea2;
            }
            set
            {
                if (value != _Linea2)
                {
                    _Linea2 = value;
                    base.SetValue(Linea2Property, value);
                    if (NumLines > 1) LoadLine(_Linea2, this.PanelLinea2, this.PanelLinea26);
                }
            }
        }

        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Linea 3")]
        public string Linea3
        {
            get
            {
                if (_Linea3 != (string)base.GetValue(Linea3Property))
                {
                    base.SetValue(Linea3Property, _Linea3);                   
                }
                return _Linea3;
            }
            set
            {
                if (value != _Linea3)
                {
                    _Linea3 = value;
                    base.SetValue(Linea3Property, value);
                    if (NumLines > 2) LoadLine(_Linea3, this.PanelLinea3, this.PanelLinea36);
                }
            }
        }

        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Texto")]
        public string Texto
        {
            get
            {
                if (_Texto != (string)base.GetValue(TextoProperty))
                {
                    base.SetValue(TextoProperty, _Texto);             
                }
                return _Texto;
            }
            set
            {
                if (value != _Texto)
                {
                    _Texto = value;
                    base.SetValue(TextoProperty, value);
                    LoadTexto(_Texto);
                }
            }
        }

        [Category("Custom Properties")]
        [Browsable(true)]
        [Description("Tipología")]
        public int Tipologia
        {
            get
            {
                if (_Tipologia != (int)base.GetValue(TipologiaProperty))
                {
                    base.SetValue(TipologiaProperty, _Tipologia);                   
                }
                return _Tipologia;
            }
            set
            {
                if (value != _Tipologia)
                {
                    //if (OfflineMode)
                    {
                        _Tipologia = value;
                        base.SetValue(TipologiaProperty, value);
                        LoadTipologyOffline(_Tipologia);
                    }
                }
            }
        }

        private static void OnIcono1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);

            ((PanelControlEdit)d).Icono1 = num;
            //LKLog.wwLogger.LogInfo("Icono1:" + num);
        }

        private static void OnIcono2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);

            ((PanelControlEdit)d).Icono2 = num;
            //LKLog.wwLogger.LogInfo("Icono2:" + num);
        }

        private static void OnTipologiaChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            int num = Convert.ToInt32(e.NewValue);
            if (((PanelControlEdit)d).Tipologia != num)
                ((PanelControlEdit)d).Tipologia = num;
        }

        private static void OnLinea1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((PanelControlEdit)d).Linea1 != str1)
                ((PanelControlEdit)d).Linea1 = str1;
        }

        private static void OnLinea2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((PanelControlEdit)d).Linea2 != str1)
                ((PanelControlEdit)d).Linea2 = str1;
        }

        private static void OnLinea3Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((PanelControlEdit)d).Linea3 != str1)
                ((PanelControlEdit)d).Linea3 = str1;
        }

        private static void OnTextoChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(d)) return;
            if (e.NewValue == null) return;
            string str1 = e.NewValue.ToString();
            if (((PanelControlEdit)d).Texto != str1)
                ((PanelControlEdit)d).Texto = str1;
        }

        private bool LoadTipology()
        {
            /* if (OfflineMode) */
            return LoadTipologyOffline(_Tipologia);

            //if (DesignerProperties.GetIsInDesignMode(this)) return false;


            //try
            //{
            //    if (db == null) { tmrRetryDB.Start(); return false; }
            //    if (SQL == null) { tmrRetryDB.Start(); return false; }

            //    DataTable DataConfig = SQL.SELECT("SELECT [NumLineas],[NumIconos],[NumChar] FROM [Tipologia] WHERE ID = (SELECT [Tipologia] FROM [Instancia] WHERE Panel = '" + this.Panel + "')");
            //    foreach (DataRow crow in DataConfig.Rows)
            //    {
            //        NumLines = (int)crow["NumLineas"];
            //        NumIcons = (int)crow["NumIconos"];
            //        NumChars = (int)crow["NumChar"];
            //    }
            //    RetryDBCount = 0;
            //    return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}
        }

        private bool LoadTipologyOffline(int Tipologia)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                foreach (DataRow crow in DTipologia.Select("ID = '" + Tipologia + "'"))
                {
                    NumLines = Convert.ToInt32(crow[3]);
                    NumIcons = Convert.ToInt32(crow[4]);
                    NumChars = Convert.ToInt32(crow[5]);
                }
                //RetryDBCount = 0;
                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }


        //int RetryDBCount = 0;
        //void tmrRetryDB_Tick(object sender, EventArgs e)
        //{
        //    tmrRetryDB.Stop();

        //    RetryDBCount++;

        //    if (RetryDBCount <= 10)
        //    {
        //        LoadTipology();
        //    }
        //    else
        //    {
        //        RetryDBCount = 0;
        //    }
        //}



        private bool LoadTexto(string Text)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                Text = Text.PadRight(_numchars * 3).Substring(0, _numchars * 3);
                Linea1 = Text.Substring(_numchars * 0, _numchars);
                Linea2 = Text.Substring(_numchars * 1, _numchars);
                Linea3 = Text.Substring(_numchars * 2, _numchars);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        string tempLinea1, tempLinea2, tempLinea3;
        private bool UpdateTexto()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                tempLinea1 = Linea1.PadRight(_numchars).Substring(0, _numchars);
                tempLinea2 = Linea2.PadRight(_numchars).Substring(0, _numchars);
                tempLinea3 = Linea3.PadRight(_numchars).Substring(0, _numchars);

                Texto = (tempLinea1 + tempLinea2 + tempLinea3).Substring(0, _numchars * 3);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private bool LoadLine(string Text, PanelText12 PanelText_12, PanelText16 PanelText_16)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            try
            {
                PanelText_12.Text12 = Text;
                PanelText_16.Text16 = Text;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool LoadIcon(int IconNum, Button Button)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;

            /* if (OfflineMode)*/
            return LoadIconOffline(IconNum, Button);
            //LKLog.wwLogger.LogInfo("LoadIcon(" + IconNum + ")");
            //try
            //{
            //    Button.Content = new Image
            //    {
            //        Source = GlobalStatic.GetImageFromStream(GetIconData(IconNum)),
            //        VerticalAlignment = VerticalAlignment.Stretch,
            //        HorizontalAlignment = HorizontalAlignment.Stretch
            //    };
            //    if (((Image)Button.Content).Source == null) Button.Content = null;
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    LKLog.wwLogger.LogError("LoadIcon(" + IconNum + ") : " + ex.Message);
            //    return false;
            //}
        }

        System.Resources.ResourceManager rm = Properties.Resources.ResourceManager;

        private bool LoadIconOffline(int IconNum, Button Button)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return false;
            //LKLog.wwLogger.LogInfo("LoadIconOffline(" + IconNum + ")");
            try
            {
                object img = rm.GetObject("pmv" + IconNum);
                if (img == null) { Button.Content = null; return true; }
                Stream img2 = GlobalStatic.ToStream((System.Drawing.Image)img, System.Drawing.Imaging.ImageFormat.Png);

                Button.Content = new Image
                {
                    Source = GlobalStatic.GetImageFromStream(img2),
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalAlignment = HorizontalAlignment.Stretch
                };
                if (((Image)Button.Content).Source == null) Button.Content = null;
                return true;
            }
            catch (Exception ex)
            {
                LKLog.wwLogger.LogError("LoadIconOffline(" + IconNum + ") : " + ex.Message);
                return false;
            }

        }


        //private byte[] GetIconData(int ID)
        //{
        //    if (DesignerProperties.GetIsInDesignMode(this)) return null;

        //    if (ID <= 0) return null;
        //    try
        //    {
        //        if (db == null) return null;
        //        if (SQL == null) return null;

        //        DataTable ImageData = SQL.SELECT("SELECT [ImagenIcono] FROM [Icono] WHERE ID = " + ID);
        //        foreach (System.Data.DataRow row in ImageData.Rows)
        //        {
        //            return (byte[])row["ImagenIcono"];
        //        }
        //        return null;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        private void RedrawControl()
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;


            LoadingGrid.Visibility = Visibility.Visible;
            switch (_numlines)
            {
                case 0:
                    PanelGrid.Height = double.NaN;
                    DinamicW = 1;

                    Row1.Height = new GridLength(30, GridUnitType.Star);
                    Row2.Height = new GridLength(0, GridUnitType.Star);
                    Row3.Height = new GridLength(0, GridUnitType.Star);

                    Col1.Width = new GridLength(30, GridUnitType.Star);
                    Col2.Width = new GridLength(0, GridUnitType.Star);
                    Col3.Width = new GridLength(0, GridUnitType.Star);

                    break;
                //case 1:
                //    //if (this.Width > this.Height * 5)
                //    //{
                //    //    PanelGrid.Height = double.NaN;
                //    //    DinamicW = 300f / 20f; //PanelGrid.Height = 20;
                //    //}
                //    //else
                //    //{
                //    PanelGrid.Width = double.NaN;
                //    DinamicH = 20f / 300f; //PanelGrid.Height = 20;
                //    //}
                //    Row1.Height = new GridLength(30, GridUnitType.Star);
                //    Row2.Height = new GridLength(0, GridUnitType.Star);
                //    Row3.Height = new GridLength(0, GridUnitType.Star);

                //    Col1.Width = new GridLength(60, GridUnitType.Star);
                //    Col2.Width = new GridLength(180, GridUnitType.Star);
                //    Col3.Width = new GridLength(60, GridUnitType.Star);

                //    switch (_numicons)
                //    {
                //        case 0:
                //            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180; 
                //            Col1.Width = new GridLength(0, GridUnitType.Star);
                //            Col2.Width = new GridLength(100, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 1:
                //            DinamicH = DinamicH * (300f / 200f);//PanelGrid.Width = 200;
                //            Col1.Width = new GridLength(20, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 2:
                //            DinamicH = DinamicH * (300f / 220f);//PanelGrid.Width = 220;
                //            Col1.Width = new GridLength(20, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(20, GridUnitType.Star);
                //            break;
                //        default:
                //            break;
                //    }
                //    break;
                //case 2:
                //    //if (this.Width > this.Height * 5)
                //    //{
                //    //    PanelGrid.Height = double.NaN;
                //    //    DinamicW = 300f / 40f; //PanelGrid.Height = 20;
                //    //}
                //    //else
                //    //{
                //    PanelGrid.Width = double.NaN;
                //    DinamicH = 40f / 300f; //PanelGrid.Height = 40;
                //    //}
                //    Row1.Height = new GridLength(30, GridUnitType.Star);
                //    Row2.Height = new GridLength(30, GridUnitType.Star);
                //    Row3.Height = new GridLength(0, GridUnitType.Star);

                //    Col1.Width = new GridLength(60, GridUnitType.Star);
                //    Col2.Width = new GridLength(180, GridUnitType.Star);
                //    Col3.Width = new GridLength(60, GridUnitType.Star);

                //    switch (_numicons)
                //    {
                //        case 0:
                //            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180;
                //            Col1.Width = new GridLength(0, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 1:
                //            DinamicH = DinamicH * (300f / 220f);//PanelGrid.Width = 220;
                //            Col1.Width = new GridLength(40, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(0, GridUnitType.Star);
                //            break;
                //        case 2:
                //            DinamicH = DinamicH * (300f / 260f);//PanelGrid.Width = 260;
                //            Col1.Width = new GridLength(40, GridUnitType.Star);
                //            Col2.Width = new GridLength(180, GridUnitType.Star);
                //            Col3.Width = new GridLength(40, GridUnitType.Star);
                //            break;
                //        default:
                //            break;
                //    }
                //    break;
                default:
                    //if (this.Width > this.Height * 5)
                    //{
                    //    PanelGrid.Height = double.NaN;
                    //    DinamicW = 300f / 60f; //PanelGrid.Height = 20;
                    //}
                    //else
                    //{
                    PanelGrid.Width = double.NaN;
                    DinamicH = 60f / 300f; //PanelGrid.Height = 60;
                    //}
                    //Row1.Height = new GridLength(30, GridUnitType.Star);
                    //Row2.Height = new GridLength(30, GridUnitType.Star);
                    //Row3.Height = new GridLength(30, GridUnitType.Star);
                    switch (NumLines)
                    {
                        case 1:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(0, GridUnitType.Star);
                            Row3.Height = new GridLength(0, GridUnitType.Star);
                            break;
                        case 2:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(30, GridUnitType.Star);
                            Row3.Height = new GridLength(0, GridUnitType.Star);
                            break;
                        case 3:
                            Row1.Height = new GridLength(30, GridUnitType.Star);
                            Row2.Height = new GridLength(30, GridUnitType.Star);
                            Row3.Height = new GridLength(30, GridUnitType.Star);
                            break;
                    }

                    Col1.Width = new GridLength(60, GridUnitType.Star);
                    Col2.Width = new GridLength(180, GridUnitType.Star);
                    Col3.Width = new GridLength(60, GridUnitType.Star);

                    switch (_numicons)
                    {
                        case 0:
                            DinamicH = DinamicH * (300f / 180f);//PanelGrid.Width = 180;
                            Col1.Width = new GridLength(0, GridUnitType.Star);
                            Col2.Width = new GridLength(150, GridUnitType.Star);
                            Col3.Width = new GridLength(0, GridUnitType.Star);
                            break;
                        case 1:
                            DinamicH = DinamicH * (300f / 240f);//PanelGrid.Width = 240;
                            Col1.Width = new GridLength(60, GridUnitType.Star);
                            Col2.Width = new GridLength(180, GridUnitType.Star);
                            Col3.Width = new GridLength(0, GridUnitType.Star);
                            break;
                        case 2:
                            DinamicH = DinamicH * (300f / 300f);//PanelGrid.Width = 300;
                            Col1.Width = new GridLength(60, GridUnitType.Star);
                            Col2.Width = new GridLength(180, GridUnitType.Star);
                            Col3.Width = new GridLength(60, GridUnitType.Star);
                            break;
                        default:
                            break;
                    }
                    break;

            }
            tmrResize.Start();
        }

        System.Windows.Threading.DispatcherTimer tmrRefreshImages = new System.Windows.Threading.DispatcherTimer();
        //System.Windows.Threading.DispatcherTimer tmrCheckDB = new System.Windows.Threading.DispatcherTimer();
        //bool CheckingDB = false;

        //void tmrCheckDB_Tick(object sender, EventArgs e)
        //{
        //    tmrCheckDB.Stop();

        //    CheckDBFunction();

        //    tmrCheckDB.Start();
        //}

        void tmrRefreshImages_Tick(object sender, EventArgs e)
        {
            tmrRefreshImages.Stop();

            CheckImages();

            tmrRefreshImages.Start();
        }

        //private void CheckDBFunction()
        //{
        //    if (!CheckingDB)
        //    {
        //        System.Threading.Thread t = new System.Threading.Thread(CheckDBConection);
        //        t.Start();
        //    }
        //}

        //private void CheckDBConection()
        //{
        //    CheckingDB = true;
        //    try
        //    {
        //        OfflineMode = !SQL.CheckConnection();
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    CheckingDB = false;
        //}

        private void CheckImages()
        {
            try
            {
                if (Icono1 > 0)
                {
                    if (this.Icon1.Content == null) LoadIcon(Icono1, this.Icon1);
                    if (((Image)Icon1.Content).Source == null) LoadIcon(Icono1, this.Icon1);
                }
            }
            catch (Exception)
            {
            }
            try
            {
                if (Icono2 > 0)
                {
                    if (this.Icon2.Content == null) LoadIcon(Icono2, this.Icon2);
                    if (((Image)Icon2.Content).Source == null) LoadIcon(Icono2, this.Icon2);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void Icon1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                int auxIcon = Icono1;
                EditIcon(ref Icon1, ref auxIcon);
                Icono1 = auxIcon;
            }
        }

        private void Icon2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                int auxIcon = Icono2;
                EditIcon(ref Icon2, ref auxIcon);
                Icono2 = auxIcon;
            }
        }

        private void PanelLinea2_OnTextChanged(string Text)
        {
            if (this.PanelLinea2.Visibility == Visibility.Visible)
            {
                this.Linea2 = Text;
                UpdateTexto();
            }
        }

        private void PanelLinea3_OnTextChanged(string Text)
        {
            if (this.PanelLinea3.Visibility == Visibility.Visible)
            {
                this.Linea3 = Text;
                UpdateTexto();
            }
        }

        private void PanelLinea16_OnTextChanged(string Text)
        {
            if (this.PanelLinea16.Visibility == Visibility.Visible)
            {
                this.Linea1 = Text;
                UpdateTexto();
            }
        }

        private void PanelLinea26_OnTextChanged(string Text)
        {
            if (this.PanelLinea26.Visibility == Visibility.Visible)
            {
                this.Linea2 = Text;
                UpdateTexto();
            }
        }

        private void PanelLinea36_OnTextChanged(string Text)
        {
            if (this.PanelLinea36.Visibility == Visibility.Visible)
            {
                this.Linea3 = Text;
                UpdateTexto();
            }
        }

        private bool EditIcon(ref Button Icon, ref int Icono)
        {
            try
            {
                PanelsControls.ImageSelector IS = new PanelsControls.ImageSelector();
                IS.OfflineMode = _OfflineMode;
                IS.db = this.db;
                IS.SelectedImageID = Icono;
                if (IS.ShowDialog() == true) { Icono = IS.SelectedImageID; LoadIcon(Icono, Icon); }
                IS = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void PanelLinea1_OnTextChanged(string Text)
        {
            if (this.PanelLinea1.Visibility == Visibility.Visible)
            {
                this.Linea1 = Text;
                UpdateTexto();
            }
        }
    }


}
