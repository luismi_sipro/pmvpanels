﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for DlgInputText.xaml
    /// </summary>
    public partial class DlgInputText : Window
    {
        public Int32 MaxCharacters
        {
            get;
            set;
        }


        public String Text
        {
            get;
            set;
        }


        public DlgInputText()
        {
            InitializeComponent();
            this.MaxCharacters = 16;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.DialogResult == false)
                return;

            if(this.txtLine.Text.Length > this.MaxCharacters)
            {
                MessageBox.Show(String.Format("No puede introducir más de {0} carácteres.", MaxCharacters), "Panel", MessageBoxButton.OK, MessageBoxImage.Warning);
                e.Cancel = true;
                return;
            }

            this.Text = this.txtLine.Text.PadRight(this.MaxCharacters);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtLine.Text = this.Text;
            this.txtLine.MaxLength = MaxCharacters;
        }

        private void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            //align left: use oposite pad.
            String _txt = this.txtLine.Text.Trim();
            this.txtLine.Text = _txt.PadRight(MaxCharacters);
        }

        private void btnCenter_Click(object sender, RoutedEventArgs e)
        {
            //align center
            String _txt = this.txtLine.Text.Trim();
            Int32 spaces = MaxCharacters - _txt.Length;
            Int32 padleft = spaces / 2 + _txt.Length;
            this.txtLine.Text = _txt.PadLeft(padleft).PadRight(MaxCharacters);
        }

        private void btnRight_Click(object sender, RoutedEventArgs e)
        {
            //align right: use oposite pad.
            String _txt = this.txtLine.Text.Trim();
            this.txtLine.Text = _txt.PadLeft(MaxCharacters);
        }
    }
}
