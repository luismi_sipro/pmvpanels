﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//My namespaces.
using Sipro.Database;

namespace PanelsControls
{
    /// <summary>
    /// Interaction logic for HistoricoPanelesv2.xaml
    /// </summary>
    public partial class HistoricoPanelesv2 : UserControl
    {
        #region CONST

        public const Char CHAR_SEPARATOR = ';';
        #endregion


        #region MEMBERS

        //SQL Server manager.
        private CDBSqlServer m_sqlConnection;

        //List of historical panels.
        private List<CAspectPanel> m_aspectPanels;

        //Number of aspects.
        private Int32 m_numberAspects = 0;

        //Typology description.
        private CTypologyDescr m_typeDescr;
        #endregion


        #region PROPERTIES

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Server name")]
        public String Server
        {
            get { return (string)GetValue(ServerProperty); }
            set
            {
                if (value == (string)GetValue(ServerProperty))
                    return;
                SetValue(ServerProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("Database name")]
        public String DataBase
        {
            get { return (string)GetValue(DataBaseProperty); }
            set
            {
                if (value == (string)GetValue(DataBaseProperty))
                    return;
                SetValue(DataBaseProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User name")]
        public String User
        {
            get { return (string)GetValue(UserProperty); }
            set
            {
                if (value == (string)GetValue(UserProperty))
                    return;
                SetValue(UserProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(true)]
        [Description("User password")]
        public String Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set
            {
                if (value == (string)GetValue(PasswordProperty))
                    return;
                SetValue(PasswordProperty, value);
            }
        }

        [Category("SqlServer Properties")]
        [Browsable(false)]
        public String ConnectionStr
        {
            get { return String.Format("Server={0};Database={1};User Id={2};Password={3}", Server, DataBase, User, Password); }
        }

        [Category("Configuration")]
        [Browsable(true)]
        public String AppLang
        {
            get { return (string)GetValue(AppLangProperty); }
            set
            {
                if (value == (string)GetValue(AppLangProperty))
                    return;
                SetValue(AppLangProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String Panel
        {
            get { return (string)GetValue(PanelProperty); }
            set
            {
                if (value == (string)GetValue(PanelProperty))
                    return;
                SetValue(PanelProperty, value);
                //fGetPanelInformation(false);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public String PanelData
        {
            get { return (string)GetValue(PanelDataProperty); }
            set
            {
                if (value == (string)GetValue(PanelDataProperty))
                    return;
                SetValue(PanelDataProperty, value);
            }
        }

        [Category("Panel Properties")]
        [Browsable(true)]
        public Boolean AplicarCalle
        {
            get { return (Boolean)GetValue(AplicarCalleProperty); }
            set
            {
                if (value == (Boolean)GetValue(AplicarCalleProperty))
                    return;
                SetValue(AplicarCalleProperty, value);
            }
        }

        #endregion


        #region DEPENDENCY PROPERTIES

        // SQL connection information.
        public static readonly DependencyProperty ServerProperty = DependencyProperty.Register("Server", typeof(String), typeof(HistoricoPanelesv2));
        public static readonly DependencyProperty DataBaseProperty = DependencyProperty.Register("DataBase", typeof(String), typeof(HistoricoPanelesv2));
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register("User", typeof(String), typeof(HistoricoPanelesv2));
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(String), typeof(HistoricoPanelesv2));

        //Configuration
        public static readonly DependencyProperty AppLangProperty = DependencyProperty.Register("AppLang", typeof(String), typeof(HistoricoPanelesv2));

        //Specific I/O public control properties.
        public static readonly DependencyProperty AplicarCalleProperty = DependencyProperty.Register("AplicarCalle", typeof(Boolean), typeof(HistoricoPanelesv2), new PropertyMetadata(false, new PropertyChangedCallback(OnPanelChanged)));
        public static readonly DependencyProperty PanelProperty = DependencyProperty.Register("Panel", typeof(String), typeof(HistoricoPanelesv2), new PropertyMetadata("", new PropertyChangedCallback(OnPanelChanged)));
        private static void OnPanelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            HistoricoPanelesv2 _histPanels = d as HistoricoPanelesv2;
            if (_histPanels == null)
                return;

            _histPanels.fGetPanelInformation(false);
            _histPanels.AplicarCalle = false;
        }
        public static readonly DependencyProperty PanelDataProperty = DependencyProperty.Register("PanelData", typeof(String), typeof(HistoricoPanelesv2));
        

        #endregion


        #region CONSTRUCTORS

        public HistoricoPanelesv2()
        {
            InitializeComponent();

        }

        #endregion


        #region METHODS
        #endregion


        #region FUNCTIONS

        //Send panel info to panel control viewer.
        private Int32 fSetGraphicalObjects(Int32 mPanelNumber)
        {
            //Clear aspects.
            this.Aspecto1.ClearData();
            this.Aspecto2.ClearData();
            this.Aspecto3.ClearData();
            this.Aspecto4.ClearData();

            //Set default.
            Aspecto1.Visibility = Visibility.Hidden;
            Aspecto2.Visibility = Visibility.Hidden;
            Aspecto3.Visibility = Visibility.Hidden;
            Aspecto4.Visibility = Visibility.Hidden;
            n1.Visibility = Visibility.Hidden;
            n2.Visibility = Visibility.Hidden;
            n3.Visibility = Visibility.Hidden;
            n4.Visibility = Visibility.Hidden;

            //Disabled copy button.
            this.btnCopy.IsEnabled = false;

            //Get all aspects of selected panel.
            DateTime[] _dates = m_aspectPanels.Select(x => x.Timestamp).Distinct().ToArray();
            if (_dates == null)
                return CErrorCode.ERR_ALL_OK;

            //Check if select panel out of limits.
            if (mPanelNumber == 0 || mPanelNumber > _dates.Length)
                return CErrorCode.ERR_ALL_OK;

            CAspectPanel[] _panels = m_aspectPanels.Where(x => x.Timestamp == _dates[mPanelNumber - 1]).ToArray();
            this.m_numberAspects = _panels.Length;

            foreach (CAspectPanel _rAspect in _panels)
            {
                switch (_rAspect.NumAspect)
                {
                    case 1:
                        Aspecto1.Icono1 = _rAspect.Icon1;
                        Aspecto1.Icono2 = _rAspect.Icon2;
                        Aspecto1.Linea1 = _rAspect.Line1;
                        Aspecto1.Linea2 = _rAspect.Line2;
                        Aspecto1.Linea3 = _rAspect.Line3;
                        Aspecto1.UpdateData();
                        Aspecto1.Visibility = Visibility.Visible;
                        n1.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        Aspecto2.Icono1 = _rAspect.Icon1;
                        Aspecto2.Icono2 = _rAspect.Icon2;
                        Aspecto2.Linea1 = _rAspect.Line1;
                        Aspecto2.Linea2 = _rAspect.Line2;
                        Aspecto2.Linea3 = _rAspect.Line3;
                        Aspecto2.UpdateData();
                        Aspecto2.Visibility = Visibility.Visible;
                        n2.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        Aspecto3.Icono1 = _rAspect.Icon1;
                        Aspecto3.Icono2 = _rAspect.Icon2;
                        Aspecto3.Linea1 = _rAspect.Line1;
                        Aspecto3.Linea2 = _rAspect.Line2;
                        Aspecto3.Linea3 = _rAspect.Line3;
                        Aspecto3.UpdateData();
                        Aspecto3.Visibility = Visibility.Visible;
                        n3.Visibility = Visibility.Visible;
                        break;
                    case 4:
                        Aspecto4.Icono1 = _rAspect.Icon1;
                        Aspecto4.Icono2 = _rAspect.Icon2;
                        Aspecto4.Linea1 = _rAspect.Line1;
                        Aspecto4.Linea2 = _rAspect.Line2;
                        Aspecto4.Linea3 = _rAspect.Line3;
                        Aspecto4.UpdateData();
                        Aspecto4.Visibility = Visibility.Visible;
                        n4.Visibility = Visibility.Visible;
                        break;
                    default:
                        break;
                }
            }

            //Enabled button because theare are more than 0 panels.
            this.btnCopy.IsEnabled = true;

            return 0;
        }

        //Get information from sql server.
        private Int32 fGetPanelInformation(Boolean fFilterByTipology)
        {
            Int32 _typology = 0;

            //Check if object connection exist and connection it's ok.
            if (m_sqlConnection == null || m_sqlConnection.State != System.Data.ConnectionState.Open)
                return -1;

            //Create a new list of aspects panels.
            m_aspectPanels = new List<CAspectPanel>();

            //This query gets the last used panels.
            String _query = String.Format("SELECT * FROM [Historico] WHERE Panel = '{0}' AND FechaHora IN (SELECT DISTINCT TOP(10) FechaHora FROM [Historico] WHERE Panel='{0}' ORDER BY FechaHora DESC) ORDER BY FechaHora DESC", this.Panel);
            if (fFilterByTipology == true)
                _query = String.Format("SELECT * FROM [Historico] WHERE Tipologia = (SELECT Tipologia FROM Instancia WHERE Panel='{0}') AND FechaHora IN (SELECT DISTINCT TOP(10) FechaHora FROM [Historico] WHERE Tipologia=(SELECT Tipologia FROM Instancia WHERE Panel='{0}') ORDER BY FechaHora DESC) ORDER BY FechaHora DESC", this.Panel);

            //Execute query.
            CDataBaseResult _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
            {
                fSetGraphicalObjects(0); //Hide panels. No historic panels are available.
                return 0;
            }

            //if Yes, retreive it.
            foreach (CDataBaseRow _row in _dbResult.Rows)
            {
                CAspectPanel _aspect = new CAspectPanel();
                _aspect.ID = _row.GetValue("ID").ToString();
                _aspect.Panel = _row.GetValue("Panel").ToString();
                _aspect.Tipology = Convert.ToInt32(_row.GetValue("Tipologia"));
                _aspect.NumAspect = Convert.ToInt32(_row.GetValue("NumeroAspecto"));
                _aspect.Timestamp = (DateTime)_row.GetValue("FechaHora");
                _aspect.Icon1 = (_row.GetValue("Icono1") is DBNull) ? 0 : Convert.ToInt32(_row.GetValue("Icono1"));
                _aspect.Icon2 = (_row.GetValue("Icono2") is DBNull) ? 0 : Convert.ToInt32(_row.GetValue("Icono2"));
                _aspect.Line1 = _row.GetValue("Linea1").ToString();
                _aspect.Line2 = _row.GetValue("Linea2").ToString();
                _aspect.Line3 = _row.GetValue("Linea3").ToString();

                //Save the last tipology. 
                _typology = _aspect.Tipology;

                //Add to list of aspects.
                m_aspectPanels.Add(_aspect);
            }

            //Get typology description and update control.
            _query = String.Format("SELECT DISTINCT * FROM Tipologia WHERE ID={0}", _typology);
            _dbResult = m_sqlConnection.ExecuteQuery(_query);

            //Check if there are results.
            if (_dbResult.HasRows == false)
                return -1;

            //Get results.
            if (_dbResult.Rows.Length > 1)
                return -1;
            m_typeDescr = new CTypologyDescr();
            m_typeDescr.ID = Convert.ToInt32(_dbResult.Rows[0].GetValue("ID"));
            m_typeDescr.Name = _dbResult.Rows[0].GetValue("Nombre").ToString();
            m_typeDescr.Description = _dbResult.Rows[0].GetValue("Descripcion").ToString();
            m_typeDescr.NumLines = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumLineas"));
            m_typeDescr.NumIcons = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumIconos"));
            m_typeDescr.NumChars = Convert.ToInt32(_dbResult.Rows[0].GetValue("NumChar"));

            //Set panel typology description.
            this.Aspecto1.TypeDescr = this.m_typeDescr;
            this.Aspecto2.TypeDescr = this.m_typeDescr;
            this.Aspecto3.TypeDescr = this.m_typeDescr;
            this.Aspecto4.TypeDescr = this.m_typeDescr;

            //Select first element.
            this.cboSelect.SelectedIndex = 0;
            fSetGraphicalObjects(1);

            return 0;
        }

        //Serialized data panel.
        private String fSetPanelHistoricalData()
        {
            //Check if combo panel number are selected.
            if (this.cboSelect.SelectedIndex < 0)
                return "";

            //Serialized information for all panel. The '{0}' is for separator character. 
            String _p1 = String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", CHAR_SEPARATOR, this.Aspecto1.Icono1, this.Aspecto1.Icono2, this.Aspecto1.Linea1, this.Aspecto1.Linea2, this.Aspecto1.Linea3);
            String _p2 = String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", CHAR_SEPARATOR, this.Aspecto2.Icono1, this.Aspecto2.Icono2, this.Aspecto2.Linea1, this.Aspecto2.Linea2, this.Aspecto2.Linea3);
            String _p3 = String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", CHAR_SEPARATOR, this.Aspecto3.Icono1, this.Aspecto3.Icono2, this.Aspecto3.Linea1, this.Aspecto3.Linea2, this.Aspecto3.Linea3);
            String _p4 = String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}", CHAR_SEPARATOR, this.Aspecto4.Icono1, this.Aspecto4.Icono2, this.Aspecto4.Linea1, this.Aspecto4.Linea2, this.Aspecto4.Linea3);

            String _result = "";
            if (this.m_numberAspects >= 1)
            {
                _result += String.Format("{0}", _p1); 
            }

            if (this.m_numberAspects >= 2)
            {
                _result += String.Format("{1}{0}", _p2, CHAR_SEPARATOR);
            }

            if (this.m_numberAspects >= 3)
            {
                _result += String.Format("{1}{0}", _p3, CHAR_SEPARATOR);
            }

            if (this.m_numberAspects >= 4)
            {
                _result += String.Format("{1}{0}", _p4, CHAR_SEPARATOR);
            }

            return _result;
        }

        //Translate applications texts.
        private void fTranslateLabels(String mLang)
        {
            //Load texts
            CLangCode.GetLanguageTexts(m_sqlConnection, mLang);

            this.btnCopy.Content = CLangCode.GetText(CLangCode.HP2_NAME_BTN_COPY);
            this.rdPanel.Content = CLangCode.GetText(CLangCode.HP2_NAME_BTN_PANEL);
            this.rdTipology.Content = CLangCode.GetText(CLangCode.HP2_NAME_BTN_TYPE);
            this.label1.Content = CLangCode.GetText(CLangCode.HP2_NAME_LABEL_SEL);
        }
        #endregion


        #region EVENT HANDLERS

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Create sql server connection.
            m_sqlConnection = new CDBSqlServer(ConnectionStr);
            Boolean _r = m_sqlConnection.Connect();
            if (_r == false)
            {
                //OnShowError(new CErrorInfo(CErrorCode.ERR_DB_CONNECT, "No se puede conectar con la base de datos."));
                return;
            }

            //Set database connection to controls.
            this.Aspecto1.DBConnection = m_sqlConnection;
            this.Aspecto2.DBConnection = m_sqlConnection;
            this.Aspecto3.DBConnection = m_sqlConnection;
            this.Aspecto4.DBConnection = m_sqlConnection;

            //Translate
            fTranslateLabels(AppLang);

            this.rdPanel.IsChecked = true;
        }
        
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            m_sqlConnection.Close();
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            //Set panel serialized info.
            //this.AplicarPlantilla = true;
            this.PanelData = this.fSetPanelHistoricalData();
        }

        private void rdPanel_Checked(object sender, RoutedEventArgs e)
        {
            if (this.rdPanel.IsChecked == true)
            {
                fGetPanelInformation(false);
            }
        }

        private void rdTipology_Checked(object sender, RoutedEventArgs e)
        {
            if (this.rdTipology.IsChecked == true)
            {
                fGetPanelInformation(true);
            }
        }

        private void cboSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Int32 _posSel = Convert.ToInt32(((System.Windows.Controls.Label)cboSelect.SelectedItem).Content);
            if (_posSel > 0)
                this.btnCopy.IsEnabled = true;

            fSetGraphicalObjects(_posSel);
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            fGetPanelInformation(false);
        }
        #endregion
    }
}
