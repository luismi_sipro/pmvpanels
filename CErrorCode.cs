﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelsControls
{
    public class CErrorCode
    {
        public const Int32 ERR_ALL_OK = 0;
        public const Int32 ERR_DB_CONNECT = 20;
        public const Int32 ERR_DB_QUERY = 21;
        public const Int32 ERR_PNL_TYPE = 30;
        public const Int32 ERR_PNL_DATA_INVALID = 31;
        public const Int32 ERR_ASPECTS_NOSEL = 40;
        public const Int32 ERR_LIB_DELCAT = 50;
        public const Int32 ERR_LIB_DEL_TEMPLATE = 51;
        public const Int32 ERR_LIB_ADD_TEMPLATE = 52;
    }
}
